﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace PrettyEditorWindows
{
    /*
    Pretty Editor Windows - Visual Enhancement Tool

    Adds icons and label colours tot he Project and Hierarchy windows.
    Allows the user to colour and assign an icon to the label based on asset type or what components contained on the scene gameobject.

    See also: http://www.tenebrous.co.uk/?portfolio=unity-editor-enhancements
    */
    [InitializeOnLoad]
    public static class PrettyEditorWindows
    {
        #region /- Variables -------------------------------------------------------------------------------------------
        public static bool DEBUG                    = false;
        public const string MenuRoot                = "Tools/Pretty Editor Windows/";
        public static PrettyEditorWindowPreferences preferences = new PrettyEditorWindowPreferences();
        #endregion

        #region /- Constructor -----------------------------------------------------------------------------------------
        static PrettyEditorWindows()
        {
            string debugOutput = "";

            // Set up an OnInitialize() event to be run on the first frame call
            // This allows the other portions of this system, particularly the MenuItem entries to be registered
            // before the preferences load.
            EditorApplication.update -= OnInitialize;
            EditorApplication.update += OnInitialize;

            if (DEBUG) Debug.LogFormat("[ {0} ] {1}():\n{2}", "PEW", System.Reflection.MethodBase.GetCurrentMethod().Name, debugOutput);
        } // ctor()
        #endregion

        #region /- Initialize ------------------------------------------------------------------------------------------
        /// <summary>
        /// Raises the initialize event.
        /// </summary>
        public static void OnInitialize()
        {
            string debugOutput = "";

            LoadPreferences();
            DeregisterListeners();
            RegisterListeners();

            // Remove this function from being called again.
            EditorApplication.update -= OnInitialize;

            if (DEBUG) Debug.LogFormat("[ {0} ] {1}():\n{2}", "PEW", System.Reflection.MethodBase.GetCurrentMethod().Name, debugOutput);
        } // Initialize()

        public static void RegisterListeners()
        {
            /*
            EditorApplication.projectWindowChanged += ProjectWindowChanged;
            EditorApplication.projectWindowItemOnGUI += ProjectWindowOnGUI;
            EditorApplication.hierarchyWindowChanged += HierarchyWindowChanged;
            EditorApplication.hierarchyWindowItemOnGUI += HierarchyWindowOnGUI;
            */
        } // RegisterListeners()
        public static void DeregisterListeners()
        {
            /*
            EditorApplication.projectWindowChanged -= ProjectWindowChanged;
            EditorApplication.projectWindowItemOnGUI -= ProjectWindowOnGUI;
            EditorApplication.hierarchyWindowChanged -= HierarchyWindowChanged;
            EditorApplication.hierarchyWindowItemOnGUI -= HierarchyWindowOnGUI;
            */
        } // DeregisterListeners()
        #endregion

        #region /- Preferences -----------------------------------------------------------------------------------------
        public static bool LoadPreferences()
        {
            string debugOutput = "";
            string targetFilename = Path.Combine(Application.dataPath, "xml/editor/prettyeditorwindows.xml");
            bool result = XML.LoadFile(ref preferences, targetFilename);
            if (result)
            {
                debugOutput += string.Format("Loaded from: {0}", targetFilename);
                SetMenuChecks();
            }

            //Debug.LogFormat("[ {0} ] {1}(): {2}", "PEW", System.Reflection.MethodBase.GetCurrentMethod().Name, debugOutput);
            return (result);
        } // LoadPreferences()

        public static bool SavePreferences()
        {
            string debugOutput = "";
            string targetFilename = Path.Combine(Application.dataPath, "xml/editor/prettyeditorwindows.xml");
            bool result = XML.SaveFile(preferences, targetFilename);
            if (result)
            {
                debugOutput += string.Format("Saved to: {0}", targetFilename);
            }

            //Debug.LogFormat("[ {0} ] {1}(): {2}", "PEW", System.Reflection.MethodBase.GetCurrentMethod().Name, debugOutput);
            return (result);
        } // SavePreferences()
        #endregion

        #region /- MenuItems -------------------------------------------------------------------------------------------
        [MenuItem(MenuRoot + "Enable", false, 10)]
        public static void TogglePrettyEditorWindows()
        {
            string debugOutput = "";

            preferences.enabled = !preferences.enabled;
            debugOutput += string.Format("TogglePrettyEditorWindows = {0}", preferences.enabled);
            SetMenuChecks();

            if (DEBUG) Debug.LogFormat("[ {0} ] {1}():\n{2}", "PEW", System.Reflection.MethodBase.GetCurrentMethod().Name, debugOutput);
        } // TogglePrettyEditorWindows()

        [MenuItem(MenuRoot + "Enable Project Styles", true)]
        private static bool ToggleProjectWindowValidation()
        {
            return (preferences.enabled);
        } // ToggleProjectWindowValidation()
        [MenuItem(MenuRoot + "Enable Project Styles", false, 25)]
        public static void ToggleProjectWindow()
        {
            string debugOutput = "";

            preferences.enableProjectStyles = !preferences.enableProjectStyles;
            debugOutput += string.Format("enableProjectStyles = {0}", preferences.enableProjectStyles);
            SetMenuChecks();

            Debug.LogFormat("[ {0} ] {1}():\n{2}", "PEW", System.Reflection.MethodBase.GetCurrentMethod().Name, debugOutput);
        } // ToggleProjectWindow()

        [MenuItem(MenuRoot + "Enable Hierarchy Styles", true)]
        private static bool ToggleHierarchyWindowValidation()
        {
            return (preferences.enabled);
        } // ToggleHierarchyWindowValidation()
        [MenuItem(MenuRoot + "Enable Hierarchy Styles", false, 25)]
        public static void ToggleHierarchyWindow()
        {
            string debugOutput = "";

            preferences.enableHierarchyStyles = !preferences.enableHierarchyStyles;
            debugOutput += string.Format("enableHierarchyStyles = {0}", preferences.enableHierarchyStyles);
            SetMenuChecks();

            Debug.LogFormat("[ {0} ] {1}():\n{2}", "PEW", System.Reflection.MethodBase.GetCurrentMethod().Name, debugOutput);
        } // ToggleHierarchyWindow()

        [MenuItem(MenuRoot + "Edit Styles", false, 50)]
        public static void EditStyles()
        {
            string debugOutput = "";

            // Get existing open window or if none, make a new one:
            EditStyles_Editor window = (EditStyles_Editor)EditorWindow.GetWindow(typeof(EditStyles_Editor), false, "Edit Styles");
            window.Show();

            Debug.LogFormat("[ {0} ] {1}():\n{2}", "PEW", System.Reflection.MethodBase.GetCurrentMethod().Name, debugOutput);
        } // EditStyles()

        [MenuItem(MenuRoot + "Load Preferences", false, 100)]
        public static void LoadPreferencesMenuItem()
        {
            LoadPreferences();
        } // LoadPreferencesMenuItem()

        [MenuItem(MenuRoot + "Save Preferences", false, 101)]
        public static void SavePreferencesMenuItem()
        {
            SavePreferences();
        } // SavePreferencesMenuItem()

        public static void SetMenuChecks()
        {
            Menu.SetChecked(MenuRoot + "Enable", preferences.enabled);
            Menu.SetChecked(MenuRoot + "Enable Project Styles", preferences.enableProjectStyles);
            Menu.SetChecked(MenuRoot + "Enable Hierarchy Styles", preferences.enableHierarchyStyles);
        } // SetMenuChecks()
        #endregion


        #region /- Project List Functions ------------------------------------------------------------------------------
        /// <summary>
        /// Adds the item to the list.
        /// </summary>
        /// <param name="item">Item to add.</param>
        public static void AddProjectItem(ProjectWindowStyle item)
        {
            preferences.projectStyles.Add(item);
        } // AddItem()

        /// <summary>
        /// Gets the item by name, if found.
        /// </summary>
        /// <param name="name">Name of the item to find.</param>
        /// <returns>The item if found, otherwise null.</returns>
        public static ProjectWindowStyle GetProjectItem(string name)
        {
            for (int i = 0; i < preferences.projectStyles.Count; i++)
            {
                if (preferences.projectStyles[i].id.ToLower() == name.ToLower()) return (preferences.projectStyles[i]);
            }
            return (null);
        } // GetItem()

        /// <summary>
        /// Gets the index of the item, if found. Searches by item name and returns the first instance encountered.
        /// </summary>
        /// <param name="name">Name of the item to get the index of. </param>
        /// <returns>The index of the found item.</returns>
        public static int GetProjectItemIndex(string name)
        {
            return (GetProjectItemIndex(GetProjectItem(name)));
        } // GetItemIndex()

        /// <summary>
        /// Gets the index of the item, if found.
        /// </summary>
        /// <param name="item">The item to get the index of in the main list.</param>
        /// <returns>The index of the found item.</returns>
        public static int GetProjectItemIndex(ProjectWindowStyle item)
        {
            for (int i = 0; i < preferences.projectStyles.Count; i++)
            {
                if (preferences.projectStyles[i] == item) return (i);
            }
            return (-1);
        } // GetItemIndex()

        /// <summary>
        /// Deletes the specified Item from the main list.
        /// </summary>
        /// <param name="item">Item to delete.</param>
        public static void DeleteProjectItem(ProjectWindowStyle item)
        {
            if (item == null) return;
            preferences.projectStyles.Remove(item);
        } // DeleteItem()

        /// <summary>
        /// Duplicates the item.
        /// </summary>
        /// <param name="item">Item to duplicate.</param>
        public static void DuplicateProjectItem(ProjectWindowStyle item)
        {
            if (item == null) return;
            ProjectWindowStyle newItem = new ProjectWindowStyle(item);
            newItem.id += " Copy";
            preferences.projectStyles.Add(newItem);
        } // DuplicateItem()

        /// <summary>
        /// Moves the specified item one down in the list.
        /// </summary>
        /// <param name="item">Item to move.</param>
        public static void MoveProjectItemDown(ProjectWindowStyle item)
        {
            if (item == null) return;
            int index = GetProjectItemIndex(item);
            if (GetProjectItemIndex(item) != (preferences.projectStyles.Count - 1))
            {
                preferences.projectStyles.Remove(item);
                preferences.projectStyles.Insert(index + 1, item);
            }
        } // MoveItemDown()

        /// <summary>
        /// Moves the specified item one up in the list.
        /// </summary>
        /// <param name="item">Item to move.</param>
        public static void MoveProjectItemUp(ProjectWindowStyle item)
        {
            if (item == null) return;
            int index = GetProjectItemIndex(item);
            if (index > 0)
            {
                preferences.projectStyles.Remove(item);
                preferences.projectStyles.Insert(index - 1, item);
            }
        } // MoveItemUp()

        #endregion

    } // PrettyEditorWindows()
} // PrettyEditorWindows

#if DISABLED
#region /- Variables -------------------------------------------------------------------------------------------
	public static List<ProjectWindowStyle> project_styles			= new List<ProjectWindowStyle>();
	public static List<HierarchyWindowStyle> hierarchy_styles		= new List<HierarchyWindowStyle>();

	public static Dictionary<string,ProjectWindowItem> project_items= new Dictionary<string, ProjectWindowItem>();
	public static Dictionary<GameObject,HierarchyWindowItem> hierarchy_items = new Dictionary<GameObject, HierarchyWindowItem>();
#endregion
   

#region /- Update ----------------------------------------------------------------------------------------------
	/// <summary>
	/// Update event, runs whenever the contents change.
	/// </summary>
	public static void Update()
	{
		//string debugOutput = "EditorWindowEnhancement.Update():";

		// clear the caches
		project_items.Clear();
		hierarchy_items.Clear();

		//Debug.Log( debugOutput );
	} // Update()
#endregion

#region /- ProjectWindow ---------------------------------------------------------------------------------------
	/// <summary>
	/// Event when the ProjectBrowser Window has changed, i.e. added new asset or reorganized the structure.
	/// Opening/Closing a parent does not trigger this event.
	/// </summary>
	public static void ProjectWindowChanged()
	{
		//Debug.LogWarning("ProjectWindowChanged()");

		// Clear the ProjectWindowItems since we don't know what's chanegd.
		project_items.Clear();
	} // ProjectWindowChanged()

	/// <summary>
	/// Event runs when the ProjectBrowser window updates its GUI.
	/// </summary>
	/// <param name="guid">GUID.</param>
	/// <param name="selectionRect">Selection rect.</param>
	public static void ProjectWindowOnGUI(string guid, Rect selectionRect)
	{
		if (!m_enableProjectStyles) return;	// feature not enabled, skip
		if (Event.current.type != EventType.Repaint) return; // not a repaint event, skip

		// Convert from GUID to Asset Path, and strip out the first Asset part of the path (but maintain the preceding /)
		string asset_path = AssetDatabase.GUIDToAssetPath(guid);
		asset_path = asset_path.Substring( asset_path.IndexOf("/")==-1?0:asset_path.IndexOf("/") );

		// Check if the asset_path matches a particular label
		if ( project_items.ContainsKey(asset_path) ) // Assignment exists in the cache, use it's reference.
		{
			project_items[asset_path].Draw(asset_path, selectionRect);
		}
		else // Not found, check what criteria it matches, if any
		{
			// Find a matching label and store it
			bool found_match = false;
			foreach (ProjectWindowStyle style in project_styles) // Loop all the project styles for a match
			{
				if (style.DoesPathMatch( asset_path ))
				{
					// Store the label assignment to skip calculations for later Repaint() events
					if (!project_items.ContainsKey(asset_path))
					{
						ProjectWindowItem item = new ProjectWindowItem(asset_path, style);
						item.Draw(asset_path, selectionRect);
						project_items.Add(asset_path, item);
						found_match = true;
						break;
					}
				} // Matches the pattern
			} // Loop styles

			// If no match was found, default to...
			if (!found_match && (asset_path != "Assets") && (asset_path != ""))
			{
				ProjectWindowItem item = new ProjectWindowItem(asset_path, null);
				item.Draw(asset_path, selectionRect);
				project_items.Add(asset_path, item);
			}
		} // not found

	} // ProjectWindowOnGUI()
#endregion

#region /- HierarchyWindow -------------------------------------------------------------------------------------
	/// <summary>
	/// Event when the Hierarchy Window has changed, i.e. added new scene object, reorganized the structure.
	/// </summary>
	public static void HierarchyWindowChanged()
	{
		//Debug.Log("HierarchyWindowChanged()");

		// Clear the hierarchy window cache since we don't know what's changed.
		hierarchy_items.Clear();
	}

	/// <summary>
	/// Event runs when the Hierarchy window updates its GUI.
	/// </summary>
	/// <param name="instanceID">Instance ID of the.</param>
	/// <param name="selectionRect">Selection rect.</param>
	public static void HierarchyWindowOnGUI(int instanceID, Rect selectionRect)
	{
		if (!m_enableHierarchyStyles) return;	// feature not enabled, skip
		if (Event.current.type != EventType.Repaint) return; // not a repaint event, skip

		// Based on: http://feedback.unity3d.com/suggestions/ability-to-change-the-folders-co
		// Convert from instanceID into GameObject
		GameObject obj = (GameObject) EditorUtility.InstanceIDToObject(instanceID);
		if (obj == null) return; // not a scene gameobject

		// Check if the GameObject exists in the cache
		if (hierarchy_items.ContainsKey(obj)) // Exists in the cache
		{
			hierarchy_items[obj].Draw(instanceID, selectionRect);
		}
		else // Not found, check what criteria it matches, if any
		{
			bool found_match = false;
			foreach (HierarchyWindowStyle style in hierarchy_styles) // Loop all the hierarchy styles for a match
			{
				if (style.DoesComponentMatch( obj ))
				{
					// Store the label assignment to skip calculations for later Repaint() events
					if (!hierarchy_items.ContainsKey(obj))
					{
						HierarchyWindowItem item = new HierarchyWindowItem(obj, style);
						item.Draw(instanceID, selectionRect);
						hierarchy_items.Add(obj, item);
						found_match = true;
						break;
					}
				} // Component matches
			} // Loop styles

			// If no match was found, default to...
			if (!found_match)
			{

			}
		} // not found

	} // HierarchyWindowOnGUI()
#endregion

#region /- Save/Load -------------------------------------------------------------------------------------------
	/// <summary>
	/// Save the colour settings to the specified XML file.
	/// </summary>
	public static bool Save()
	{
		// Populate a temp class for saving/loading
		EditorWindowSettings window_settings = new EditorWindowSettings();

		// Duplicate the entries from the target's class
		window_settings.enable_project_styles = m_enableProjectStyles;
		window_settings.enable_hierarchy_styles = m_enableHierarchyStyles;
		window_settings.project_styles = project_styles;
		window_settings.hierarchy_styles = hierarchy_styles;

		// Populate the texture and component names since XML can't store the type, make it a string
		foreach (HierarchyWindowStyle style in window_settings.hierarchy_styles)
		{
			if (style.icon != null) style.icon_name = style.icon.name;

			if (style.component_types.Length > 0)
			{
				List<string> component_names = new List<string>();
				foreach (Component comp in style.component_types)
				{
					if (comp == null) continue;
					if (comp.GetType() == null) continue;

					string comp_name = comp.GetType().ToString();

					// remove prefix
					if (comp_name.Contains(".")) comp_name = comp_name.Substring( comp_name.LastIndexOf(".")+1 );

					//Debug.Log("Save: "+comp_name);
					component_names.Add(comp_name);
				}
				style.component_names = component_names.ToArray();
			} // is a component
		}
		foreach (ProjectWindowStyle style in window_settings.project_styles)
		{
			if (style.icon != null) style.icon_name = style.icon.name;
		}

		// Save the XML file
		if (XML.SaveObject(window_settings, xml_file))
		{
			if (DEBUG) Debug.Log( "Save successfully to: " + xml_file );
		}
		else
		{
			Debug.LogError( "Error saving to: " + xml_file );
		}

		return(true);
	}

	/// <summary>
	/// Load the colour settings from the specified XML file.
	/// </summary>
	public static bool Load()
	{
		if (xml_file == "") return(false);
		EditorWindowSettings window_settings = new EditorWindowSettings();
		string debugOutput = "";

		// Load the XML file
		if (XML.LoadObject(ref window_settings, xml_file))
		{
			debugOutput += "Load successfully from: " + xml_file + "\n"+window_settings.project_styles.Count+" project label styles, "+window_settings.hierarchy_styles.Count+" hierarchy label styles.";

#region /- Hierarchy Labels ----------------------------------------------------------------------------
			// Populate the icon texture and component names
			foreach (HierarchyWindowStyle style in window_settings.hierarchy_styles)
			{
				if (style == null)
				{
					Debug.LogError("Style "+style+" does not exist.");
					continue;
				}
				else if (style.component_names == null)
				{
					Debug.LogError("Style "+style.id+" has no components.");
					continue;
				}

				if (DEBUG) Debug.Log("Loading "+style.id+"...");

				// Icon
				if (style.icon_name != "") 
				{
					style.icon = (Texture2D) GetResourceByName(style.icon_name, typeof(Texture2D));
				}

				// Component
				List<Component> component_list = new List<Component>();

				if (style == null) 
				{
					debugOutput += "Missing Style.\n";
					Debug.LogWarning( debugOutput );
					continue;
				}
				else if (style.component_names == null)
				{
					debugOutput += "Missing style.component_names.\n";
					Debug.LogWarning( debugOutput );
					continue;
				}

				foreach (string component_name in style.component_names)
				{
					if (string.IsNullOrEmpty(component_name)) continue;
					if (component_name != "")
					{
						Component new_component = (Component) GetResourceByName(component_name, typeof(Component));
						if (new_component != null)
						{
							debugOutput += "Load: "+component_name+" -> "+new_component+"\n";
							component_list.Add( new_component );
						}
					}
				}

				style.component_types = component_list.ToArray();
			}
			// Duplicate the entries to the target's class
			hierarchy_styles = window_settings.hierarchy_styles;
			m_enableHierarchyStyles = window_settings.enable_hierarchy_styles;
#endregion

#region /- Project Labels ------------------------------------------------------------------------------
			// Populate the icon texture
			foreach (ProjectWindowStyle label in window_settings.project_styles)
			{
				// Icon
				if (label.icon_name != "") label.icon = (Texture2D) GetResourceByName(label.icon_name, typeof(Texture2D));
			}
			// Duplicate the entries to the target's class
			project_styles = window_settings.project_styles;
			m_enableProjectStyles = window_settings.enable_project_styles;
#endregion

			// Prefs - load from editor prefs, not the XML
			window_settings.enable_project_styles = EditorPrefs.GetBool("editor_window_enable_project_styles");
			window_settings.enable_hierarchy_styles = EditorPrefs.GetBool("editor_window_enable_hierarchy_styles");
			m_enableProjectStyles = window_settings.enable_project_styles;
			m_enableHierarchyStyles = window_settings.enable_hierarchy_styles;

			if (DEBUG) Debug.Log( debugOutput );
			return(true);
		}
		else
		{
			Debug.LogError( "Error loading from: " + xml_file );
			return(false);
		}
	} // Load()
#endregion

#region /- Save/Load XML Object --------------------------------------------------------------------------------
	/// <summary>
	/// Saves the supplied object into an XML format
	/// </summary>
	/// <returns><c>true</c>, if object was saved, <c>false</c> otherwise.</returns>
	/// <param name="obj">Object to be saved.</param>
	/// <param name="fileName">Target File name. If the folder does not exist, it will create it.</param>
	private bool SaveObject <T> (T obj, string fileName)
	{
		// Check if we have a target filename to write to
		if (string.IsNullOrEmpty(fileName))
		{
			Debug.LogError("ERROR: Target file name is empty.");
			return(false);
		}

		// Check if target path exists
		string target_dir = Path.GetDirectoryName(fileName);
		if (!Directory.Exists(target_dir)) // does not exist, make the directory
		{
			Directory.CreateDirectory(target_dir);
		}

		try
		{
			StreamWriter streamWriter = new StreamWriter(fileName, false);
			XmlSerializer serializer = new XmlSerializer(typeof(T));
			serializer.Serialize(streamWriter, obj);
			streamWriter.Close();
			return( true );
		}
		catch (InvalidCastException e)
		{
			Debug.LogError("ERROR: " + e);
			return( false );
		}
	} // SaveObject()

	/// <summary>
	/// Loads the object from an XML file
	/// </summary>
	/// <returns><c>true</c>, if object was loaded, <c>false</c> otherwise.</returns>
	/// <param name="obj">Object to load the XML class to.</param>
	/// <param name="fileName">File name of the XML file to load.</param>
	public bool LoadObject <T> (ref T obj, string fileName)
	{
		// Check if we have a target file to read from
		if (!System.IO.File.Exists(fileName))
		{
			Debug.LogError("ERROR: File not found at: "+fileName);
			return(false);
		}

		try
		{
			XmlSerializer serializer = new XmlSerializer(typeof(T));
			StreamReader streamReader = new StreamReader(fileName);
			obj = (T)serializer.Deserialize(streamReader);
			streamReader.Close();
			return true;
		}
		catch (InvalidCastException e)
		{
			Debug.LogError("ERROR: " + e);
			return false;
		}
	} // LoadObject()
#endregion

#region /- Save/Load Functions ---------------------------------------------------------------------------------
	/// <summary>
	/// Gets the stored xml file.
	/// </summary>
	/// <returns>The xml file.</returns>
	public static string GetXmlFile()
	{
		/*
		if (xml_file == "") // Load string from stored Editor Prefs
		{
			xml_file = EditorPrefs.GetString("nvyve_editorwindow", "");
		}
		*/

		xml_file = Application.dataPath + "/xml/_nvyve_editorwindow.xml"; // Default setting
		return( xml_file );
	}
	/// <summary>
	/// Sets the stored xml file.
	/// </summary>
	/// <returns><c>true</c>, if xml file was set, <c>false</c> otherwise.</returns>
	/// <param name="new_xml_file">New_xml_file.</param>
	public static void SetXmlFile(string new_xml_file)
	{
		xml_file = new_xml_file;
		EditorPrefs.SetString("nvyve_editorwindow", xml_file);
	}

	/// <summary>
	/// Searches in memory for an asset resource by name, and returns it.
	/// </summary>
	/// <returns>The resource object if found, null otherwise.</returns>
	/// <param name="object_name">The name of the resource object.</param>
	/// <param name="object_type">The object type for the resource, to help filter and speed up the search.</param>
	public static UnityEngine.Object GetResourceByName( string object_name, System.Type object_type=null )
	{
		// Checks
		if (object_name == "") return( null ); // Nothing to search for
		if (object_type == null) object_type = typeof(UnityEngine.Object); // No object specified, default to everything

		// Find all objects of that particular type
		UnityEngine.Object[] objs = (UnityEngine.Object[]) Resources.FindObjectsOfTypeAll(object_type);
		string debugOutput = string.Format("GetResourceByName({0}, {1}): {2} in scene.\n", object_name, object_type, objs.Length);

		// Cycle every object that was found in memory for a name match (case insensitive)
		UnityEngine.Object found_obj = null;
		for (int i=0; i<objs.Length; i++)
		{
			// Is a object type, needs to compare by obj.GetType().name instead of by obj.name.
			if (
				(object_type == typeof(Component))
				|| (object_type == typeof(EditorWindow))
				)
			{
				if (objs[i].GetType().Name.ToLower() == object_name.ToLower()) // Match Type
				{
					if (DEBUG)
					{
						debugOutput += string.Format("Component: {0} == {1}", object_name.ToLower(), objs[i].GetType().Name.ToLower());
						debugOutput += " = Found @ "+i+"!\n";
					}
					found_obj = objs[i];
					break;
				}
				debugOutput += "\n";
			}
			// else if (some other special criteria)
			else  if (objs[i].name.ToLower() == object_name.ToLower()) // Match Name
			{
				if (DEBUG) debugOutput += " = Found @ "+i+"\n";
				found_obj = objs[i];
				break;
			}
		} // Loop objects

		if (found_obj == null) // Not found? Try direct load via Resources.Load()
		{
			found_obj = (UnityEngine.Object) Resources.Load(object_name, object_type);
		}

		//if (DEBUG) Debug.Log(debugOutput);
		return(found_obj);
	} // GetResourceByName()
#endregion
#endif

#if DISABLED
[System.Serializable, XmlRoot("editorwindowsettings")]
public class EditorWindowSettings
{
#region /- Variables -------------------------------------------------------------------------------------------
	[XmlElement] public bool enable_project_styles					= true;
	[XmlElement] public bool enable_hierarchy_styles				= true;
	[XmlElement] public List<ProjectWindowStyle> project_styles		= new List<ProjectWindowStyle>();
	[XmlElement] public List<HierarchyWindowStyle> hierarchy_styles = new List<HierarchyWindowStyle>();
#endregion

#region /- Constructor -----------------------------------------------------------------------------------------
	public EditorWindowSettings()
	{
		enable_project_styles = true;
		enable_hierarchy_styles = true;
		project_styles = new List<ProjectWindowStyle>();
		hierarchy_styles = new List<HierarchyWindowStyle>();
	}
#endregion
} // EditorWindowSettings()
#endif

#if DISABLED


public class EditorWindowEnhancement : MonoBehaviour
{
#region /- Variables -------------------------------------------------------------------------------------------
    public string m_preferenceFile                          = "";

    public bool m_enableProjectStyles                       = true;                                 // Enables/Disables the feature to colourize the Project window
    public bool m_enableHierarchyStyles                     = true;                                 // Enables/Disables the feature to colourize the Hierarchy window

    /*
    public List<ProjectWindowStyle> project_styles			= new List<ProjectWindowStyle>();
    [HideInInspector]
    public List<ProjectWindowItem> project_items			= new List<ProjectWindowItem>();		// caches the contents of the Project window

    public List<HierarchyWindowStyle> hierarchy_styles		= new List<HierarchyWindowStyle>();
    [HideInInspector]
    public List<HierarchyWindowItem> hierarchy_items		= new List<HierarchyWindowItem>(); 		// caches the contents of the Hierarchy window
    */
    [HideInInspector]
    public GUIStyle hilabel;
#endregion



#region /- Help ----------------------------------------------------------------------------------------------------
    [ContextMenu("Help")]
    public void Context_Help() { HelpSystem.OpenURL(this.GetType().Name); }
#endregion
}
#endif