﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;

#if DISABLED
[System.Serializable, XmlRoot("hierarchystyle")]
public class HierarchyWindowStyle
{
#region /- Variables -----------------------------------------------------------------------------------------------
	[XmlElement]	public string id			= "";					// Label name for this entry
	[XmlElement]	public Color color			= new Color(1,1,1,1);	// Color to change the text label to.

	[XmlIgnore]		public Component[] component_types = new Component[0]; // Hierarchy Window Only
	[XmlElement]	public string[] component_names = new string[0];

	[XmlIgnore]		public Texture2D icon;								// Icon to prefix the label with.
	[XmlElement]	public string icon_name		= "";
	[XmlElement]	public TextAnchor icon_align= TextAnchor.MiddleRight; // icon or filetype alignment.
#endregion
		
#region /- Constructor ---------------------------------------------------------------------------------------------
	public HierarchyWindowStyle()
	{
		id 				= "";
		color 			= new Color(1,1,1,1);
		icon 			= null;
		icon_name 		= "";

		component_types = new Component[0];
		component_names = new string[0];
	} // HierarchyWindow()

	public HierarchyWindowStyle(string new_id, Color new_color = default(Color))
	{
		id = new_id;
		color = new_color;

		icon 			= null;
		icon_name 		= "";
			
		component_types = new Component[0];
		component_names = new string[0];
	}
#endregion

#region /- Functions -------------------------------------------------------------------------------------------
	public bool DoesComponentMatch(GameObject target_obj)
	{
		bool match = false;

		// Loop each component for this label and see if it matches
		if (target_obj != null)
		{

			foreach (Component comp in component_types)
			{
				if (comp == null) continue;
				string comp_name = comp.GetType().ToString();

				if (comp_name.Contains(".")) comp_name = comp_name.Substring( comp_name.LastIndexOf(".")+1 );

				if (target_obj.GetComponent( comp_name ) != null) match = true;
			} // loop components 
		} // obj exists

		return( match );
	} // DoesComponentMatch()
#endregion

} // HierarchyWindowStyle()

[System.Serializable, XmlRoot("hierarchywindowitem")]
public class HierarchyWindowItem
{
#region /- Variables -------------------------------------------------------------------------------------------
	[XmlIgnore]		public string id					= "";
	[XmlElement]	public int instanceID				= 0;
	[XmlIgnore]		public GameObject obj;

	[XmlIgnore]		public GUIStyle label_style			= null;
	[XmlIgnore]		public GUIStyle icon_style			= null;
		
	[XmlElement]	public HierarchyWindowStyle style	= null;
#endregion
				
#region /- Constructor -----------------------------------------------------------------------------------------
	public HierarchyWindowItem()
	{
		label_style			= null;
		icon_style			= null;

		style 				= null;
	} // HierarchyWindowItem()
		
	public HierarchyWindowItem(int new_instanceID, HierarchyWindowStyle new_style)
	{
		instanceID = new_instanceID;
		style = new_style;
	} // HierarchyWindowItem()

	public HierarchyWindowItem(GameObject new_obj, HierarchyWindowStyle new_style)
	{
		obj = new_obj;
		id = obj.name;
		instanceID = obj.GetInstanceID();
		style = new_style;
	} // HierarchyWindowItem()
#endregion

#region /- Functions - Draw ------------------------------------------------------------------------------------
	public void Draw(int instanceID, Rect selectionRect)
	{
		if (label_style == null) 
		{
			label_style = new GUIStyle((GUIStyle) "Hi Label");
			label_style.padding.left = EditorStyles.label.padding.left;

			if (obj.activeSelf)
			{
				label_style.normal.textColor = style.color; 
			}
			else // Object is disabled, half the color to indicate it in the window
			{
				label_style.normal.textColor = style.color / 2;
			}

			label_style.hover.textColor = style.color;

			// Disabled, half the color
			if (!obj.activeInHierarchy)
			{
				label_style.normal.textColor = style.color/2; 
				label_style.hover.textColor = style.color/2;
			}

			/* Unused
			style.active.textColor = Color.red;
			style.focused.textColor = Color.green;
				
			style.onNormal.textColor = Color.yellow;
			style.onHover.textColor = Color.cyan;
			style.onActive.textColor = Color.magenta;
			style.onFocused.textColor = Color.black;
			*/

			icon_style = new GUIStyle((GUIStyle) "Hi Label");
			icon_style.alignment = style.icon_align;
			icon_style.padding = new RectOffset(0,0,0,0);
			icon_style.margin = new RectOffset(0,0,0,0);
		}

		// Draw the label
		label_style.Draw(selectionRect, new GUIContent(obj.name), instanceID);

		// Draw the icon
		if ((style != null) && (style.icon != null))
		{
			Rect icon_rect = new Rect( selectionRect );

			icon_rect.x -= 14;
			icon_rect.width += 14;
				
			icon_style.Draw(icon_rect, new GUIContent(style.icon), false, false, false, false);
		}
	} // Draw()
#endregion

} // HierarchyWindowItem()
#endif
#endif