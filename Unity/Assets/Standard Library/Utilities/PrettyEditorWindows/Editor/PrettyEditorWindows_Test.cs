﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System;

namespace PrettyEditorWindows
{
    public class PrettyEditorWindows_Test
    {
        #region /- Some Unit Test ------------------------------------------------------------------------------------------
        /// <summary>
        /// 
        /// </summary>
        [Test]
        public void LoadPreferences_Test()
        {
            // Execute
            bool result = PrettyEditorWindows.LoadPreferences();


            // Results
            Assert.AreEqual(result, true);
        } // CreatePrettyEditorWindows_Test()



        #endregion

    } // PrettyEditorWindows_Test()
} // PrettyEditorWindows