﻿using UnityEngine;
using UnityEditor;
using System.Collections;

#if DISABLED
[CustomEditor(typeof(EditorWindow_Modifier))]
public class EditorWindow_Modifier_Editor : Editor
{
	#region /- Variables -------------------------------------------------------------------------------------------
	EditorWindow_Modifier _target;
	static MonoScript _target_script			= null;
	static MonoScript _target_editor			= null;

	bool init_gui								= false;
	#endregion
				
	#region /- Help/Edit -------------------------------------------------------------------------------------------
	[MenuItem ("CONTEXT/EditorWindow_Modifier/Edit Editor Script")]
	static void Context_EditEditor(MenuCommand command) { AssetDatabase.OpenAsset( _target_editor ); }
	[MenuItem ("CONTEXT/EditorWindow_Modifier/Find Script")]
	static void Context_FindScript(MenuCommand command) { EditorGUIUtility.PingObject( _target_script ); }
	[MenuItem ("CONTEXT/EditorWindow_Modifier/Find Editor Script")]
	static void Context_FindEditor(MenuCommand command) { EditorGUIUtility.PingObject( _target_editor ); }
	#endregion
				
	#region /- Events ----------------------------------------------------------------------------------------------
	public void OnEnable()
	{
		_target = (EditorWindow_Modifier) target;
		_target_script = MonoScript.FromMonoBehaviour( _target );
		_target_editor = MonoScript.FromScriptableObject( this );

		PopulateProperties();
	} // OnEnable()

	/// <summary>
	/// Populates the properties from the editor class to ths GameObject for modifying purposes.
	/// </summary>
	public void PopulateProperties()
	{
		// Stored
		_target.xml_file = EditorWindowEnhancement.GetXmlFile();

		// ProjectBrowser Window
		_target.m_enableProjectStyles = EditorWindowEnhancement.m_enableProjectStyles;
		_target.project_styles = EditorWindowEnhancement.project_styles;
		_target.project_items.Clear();
		foreach (string asset_path in EditorWindowEnhancement.project_items.Keys) _target.project_items.Add( EditorWindowEnhancement.project_items[asset_path] );

		// Hierarchy Window
		_target.m_enableHierarchyStyles = EditorWindowEnhancement.m_enableHierarchyStyles;
		_target.hierarchy_styles = EditorWindowEnhancement.hierarchy_styles;
		_target.hierarchy_items.Clear();
		foreach (GameObject obj in EditorWindowEnhancement.hierarchy_items.Keys) _target.hierarchy_items.Add( EditorWindowEnhancement.hierarchy_items[obj] );
	}
	#endregion

	#region /- OnInspectorGUI --------------------------------------------------------------------------------------
	public override void OnInspectorGUI()
	{	
		if (!init_gui)
		{	
			//Debug.Log("Creating Label");
			_target.hilabel = new GUIStyle( (GUIStyle) "Hi Label");
			init_gui = true;
		}

		EditorGUILayout.Space();
		GUILayout.BeginHorizontal();
		{
			if (GUILayout.Button("Save Settings"))
			{
				EditorWindowEnhancement.Save();
			}
			if (GUILayout.Button("Load Settings"))
			{
				EditorWindowEnhancement.project_styles.Clear();
				EditorWindowEnhancement.project_items.Clear();
				EditorWindowEnhancement.hierarchy_styles.Clear();
				EditorWindowEnhancement.hierarchy_items.Clear();

				EditorWindowEnhancement.Load();
				PopulateProperties();
			}
		}
		GUILayout.EndHorizontal();

		EditorGUILayout.Space();
		DrawDefaultInspector();

		if (GUI.changed)
		{
			//Debug.LogWarning("GUI.changed");

			// Populate the changes from the GameObject back to the Editor class.
			EditorWindowEnhancement.SetXmlFile(_target.xml_file);

			// Update Changes from Inspector to editor class
			EditorWindowEnhancement.m_enableProjectStyles = _target.m_enableProjectStyles;
			EditorWindowEnhancement.m_enableHierarchyStyles = _target.m_enableHierarchyStyles;
			EditorWindowEnhancement.project_styles = _target.project_styles;
			EditorWindowEnhancement.hierarchy_styles = _target.hierarchy_styles;

			// Preferences
			EditorWindowEnhancement.m_enableProjectStyles = _target.m_enableProjectStyles;
			EditorWindowEnhancement.m_enableHierarchyStyles = _target.m_enableHierarchyStyles;
			EditorPrefs.SetBool("editor_window_enable_project_styles", EditorWindowEnhancement.m_enableProjectStyles);
			EditorPrefs.SetBool("editor_window_enable_hierarchy_styles", EditorWindowEnhancement.m_enableHierarchyStyles);

			EditorWindowEnhancement.Update();
			PopulateProperties();
		}

		if (GUI.changed)
		{
			serializedObject.ApplyModifiedProperties();
			EditorUtility.SetDirty(_target);
		} // GUI.changed

	} // OnInspectorGUI()
	#endregion

	#region /- Functions -------------------------------------------------------------------------------------------

	#endregion

} // EditorWindow_Modifier_Editor()
#endif