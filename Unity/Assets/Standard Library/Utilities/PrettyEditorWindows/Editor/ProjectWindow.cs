﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

namespace PrettyEditorWindows
{
    [System.Serializable, XmlRoot("projectwindowstyle")]
    public class ProjectWindowStyle
    {
        #region /- Variables -------------------------------------------------------------------------------------------
        [XmlElement] public string id;                  // ID name for this style
        [XmlElement] public Color color;                // Color to change the text label to.
        [XmlElement] public Vector2Int offset;          // Text offset

        [XmlElement] public List<string> patterns;          // Array of patterns to match in the path
        [XmlElement] public bool isExpandedPatterns;
        [XmlElement] public string filetype;            // File extension to show

        [XmlIgnore]  public Texture2D icon;             // Icon label to show
        [XmlElement] public string icon_name;           // Name of icon
        [XmlElement] public TextAnchor icon_align;      // icon or filetype alignment.

        [XmlElement] public bool recursive;             // ProjectWindow Only: Also colour sub-elements

        [XmlElement] public bool isExpanded;            // ProjectWindow Only: Also colour sub-elements
        #endregion

        #region /- Constructor -----------------------------------------------------------------------------------------
        public ProjectWindowStyle()
        {
            this.id = "New Project Style";
            // default white text is RGB 180, 180, 180
            float c = 180f / 255f;
            this.color = new Color(c, c, c, 1.0f);
            this.offset = Vector2Int.zero;

            this.patterns = new List<string>();
            this.isExpandedPatterns = false;

            this.filetype = "";
            this.icon = null;
            this.icon_name = "";
            this.icon_align = TextAnchor.MiddleRight;

            this.recursive = false;

            this.isExpanded = false;
        } // ProjectWindowStyle()

        public ProjectWindowStyle(string name)
        {
            this.id = name;
            // default white text is RGB 180, 180, 180
            float c = 180f / 255f;
            this.color = new Color(c, c, c, 1.0f);
            this.offset = Vector2Int.zero;
            this.patterns = new List<string>();
            this.isExpandedPatterns = false;
            this.filetype = "";
            this.icon = null;
            this.icon_name = "";
            this.icon_align = TextAnchor.MiddleRight;

            this.recursive = false;

            this.isExpanded = false;
        } // ProjectWindowStyle()

        public ProjectWindowStyle(ProjectWindowStyle source)
        {
            this.id = source.id;
            this.color = new Color(source.color.r, source.color.g, source.color.b, source.color.a);
            this.offset = source.offset;

            this.patterns = new List<string>();
            foreach (string pattern in source.patterns) this.patterns.Add(pattern);

            this.isExpandedPatterns = source.isExpandedPatterns;
            this.filetype = source.filetype;
            this.icon = source.icon;
            this.icon_name = source.icon_name;
            this.icon_align = source.icon_align;

            this.recursive = source.recursive;

            this.isExpanded = source.isExpanded;
        } // ProjectWindowStyle()
        #endregion

    } // ProjectWindowStyle()









} // PrettyEditorWindows

#if DISABLED

		

#region /- Functions -------------------------------------------------------------------------------------------
	/// <summary>
	/// Doeses the path match the stored patterns for this label item?
	/// </summary>
	/// <returns><c>true</c>, if path matches one of the patterns, <c>false</c> otherwise.</returns>
	/// <param name="asset_path">Asset_path.</param>
	public bool DoesPathMatch(string asset_path)
	{
		if (asset_path == "") return(false); // nothing to match to
		bool match = false;

		// Loop each pattern for a match
		foreach (string pattern in patterns) 
		{						
			// If "/pattern" starts with /, meaning paths much match and be based off the root
			if (pattern[0] == '/') 
			{
				if (asset_path.ToLower() == pattern.ToLower())
				{
					match = true;
				}
				else if (recursive && asset_path.ToLower().StartsWith(pattern.ToLower())) // Alternatively, is recursive, so does the path start with the pattern?
				{
					match = true;
				}
			}
			else
			{
				if (recursive && (asset_path.ToLower().Contains( "/"+pattern.ToLower()+"/" ))) // path contains "/pattern/" exists in the path
				{
					match = true;
				}
				else if (asset_path.ToLower().EndsWith( "/"+pattern.ToLower() ) ) // path ends with "/pattern" exists in the path
				{
					match = true;
				}
			}

			// Exit the loop if a match is found
			if (match) break;
		} // loop patterns
			
		return( match );
	} // DoesPathMatch()
#endregion

} // ProjectWindowStyle()

[System.Serializable, XmlRoot("projectwindowitem")]
public class ProjectWindowItem
{
#region /- Variables -------------------------------------------------------------------------------------------
	[XmlElement]	public string asset_path			= "";			// Assets/SomeFolder/SomeAsset.cs
	[XmlElement]	public string full_asset_path		= "";			// C:/projects/some_project/Unity/Assets/SomeFolder/SomeAsset.cs

	[XmlElement]	public bool is_folder				= false;
	[XmlElement]	public int folder_content_count		= 0;
	[XmlElement]	public string file_type				= "";
	[XmlElement]	public bool is_library				= false;

	[XmlIgnore]		public GUIStyle label_style			= null;
	[XmlIgnore]		public GUIStyle icon_style			= null;

	[XmlElement]	public ProjectWindowStyle style		= null;
#endregion

#region /- Constructor -----------------------------------------------------------------------------------------
	public ProjectWindowItem()
	{
		asset_path 			= "";
		full_asset_path 	= "";
		
		label_style			= null;

		is_folder 			= false;
		folder_content_count= 0;
	} // ProjectWindowItem()

	public ProjectWindowItem(string new_asset_path, ProjectWindowStyle new_style)
	{
		asset_path = new_asset_path;
		style = new_style;
		UpdateFolder();
	} // ProjectWindowItem()
#endregion
				
#region /- Functions -------------------------------------------------------------------------------------------
	public void UpdateFolder()
	{
		// If a folder, get the contents and display it as part of the label
		full_asset_path = (Application.dataPath + "/" + asset_path).ToPath();
		if (Directory.Exists(full_asset_path)) // Is Directory, display contents
		{
			is_folder = true;

			// Get the file count, but minus .meta files
			folder_content_count = Directory.GetFiles(full_asset_path).Length - Directory.GetFiles(full_asset_path, "*.meta").Length;
			folder_content_count += Directory.GetDirectories(full_asset_path).Length;
		}
		else // is file
		{
			is_folder = false;
			folder_content_count = 0;

			// Get the type of file
			file_type = Path.GetExtension(full_asset_path).ToLower();
			if (file_type.Length > 0)
			{
				file_type = file_type.Substring(1); // remove leading .

				// Certain files contain other assets within, we skip using the custom display for those sub-items
				if (
					(file_type == "dll")            // DLL
					|| (file_type == "ttf")         // Font
					|| (file_type == "fbx")         // Model
					|| (file_type == "psd")         // Texture, if set to sprite
					|| (file_type == "png")         // Texture, if set to sprite
					|| (file_type == "tga")         // Texture, if set to sprite
					|| (file_type == "prefab")      // Prefab
					)
				{
					is_library = true;
				}
			} // has extension
		}
	} // UpdateFolder()
#endregion

#region /- Functions - Draw ------------------------------------------------------------------------------------
	/// <summary>
	/// Draw the GUI label for this entry.
	/// </summary>
	/// <param name="asset_path">Asset_path.</param>
	/// <param name="selectionRect">Selection rect.</param>
	public void Draw(string asset_path, Rect selectionRect)
	{
		//Debug.Log("Draw()");
		string asset_name = asset_path.Substring( asset_path.LastIndexOf("/")+1 );
			
		// Set the colour of the folder text
		if (label_style == null) 
		{
			int xoffset = 2;

			// Space out due to version control icons
			if (EditorSettings.externalVersionControl == "PlasticSCM") xoffset = 16;

			label_style = new GUIStyle((GUIStyle) "Hi Label");
			if (style == null)
			{
				float c = 179.0f / 255.0f; // Default text gray
				label_style.normal.textColor = new Color( c, c, c, 1.0f);
				label_style.contentOffset = new Vector2(label_style.contentOffset.x + xoffset, label_style.contentOffset.y);
			}
			else
			{
				label_style.normal.textColor = style.color;
				/* Unused
				style.hover.textColor = Color.red;
				style.active.textColor = Color.green;
				style.focused.textColor = Color.blue;

				style.onNormal.textColor = Color.yellow;
				style.onHover.textColor = Color.cyan;
				style.onActive.textColor = Color.magenta;
				style.onFocused.textColor = Color.black;
				*/
				label_style.alignment = TextAnchor.MiddleLeft;
				label_style.contentOffset = new Vector2(label_style.contentOffset.x + xoffset, label_style.contentOffset.y);
					
				icon_style = new GUIStyle((GUIStyle) "Hi Label");
				icon_style.alignment = style.icon_align;
				icon_style.padding = new RectOffset(0,0,0,0);
				icon_style.margin = new RectOffset(0,0,0,0);
			}
		}

		if (is_folder) asset_name += " ("+folder_content_count+")"; // add folder count if applicable

		if (selectionRect.height == 16) // ProjectBrowser in list mode
		{
			if (is_library) // is a library that has items inside, skip
			{
			}
			else
			{
				// Draw the label
				label_style.Draw(selectionRect, new GUIContent(asset_name), false, false, false, false);
			}

			// Draw the icon
			if ((style != null) && (style.icon != null))
			{
				// Expand the rect on the left so it covers the icon too
				Rect icon_rect = new Rect( selectionRect );
				//icon_rect.x -= 16; 
				//icon_rect.width += 14;
					
				//icon_rect.DrawRect(Color.red);
				icon_style.Draw(icon_rect, new GUIContent(style.icon), false, false, false, false);
			}
		}
		else // ProjectBrowser in icon mode
		{
			// Draw the label
			//label_style.Draw(selectionRect, new GUIContent(asset_name), false, false, false, false);

			// Draw the icon
			if ((style != null) && (style.icon != null)) // Draw the icon
			{
				Rect icon_rect = new Rect(selectionRect);
				//icon_rect.DrawRect(Color.red);

				if ( (icon_style.alignment == TextAnchor.UpperLeft) || (icon_style.alignment == TextAnchor.UpperCenter) || (icon_style.alignment == TextAnchor.UpperRight))
				{
				}
				else if ( (icon_style.alignment == TextAnchor.MiddleLeft) || (icon_style.alignment == TextAnchor.MiddleCenter) || (icon_style.alignment == TextAnchor.MiddleRight))
				{
					icon_rect.y += icon_rect.height / 2 - 8;
				}
				else if ( (icon_style.alignment == TextAnchor.LowerLeft) || (icon_style.alignment == TextAnchor.LowerCenter) || (icon_style.alignment == TextAnchor.LowerRight))
				{
					icon_rect.y = icon_rect.y + icon_rect.height - 16; 
				}
				//icon_rect.DrawRect(Color.magenta);

				icon_style.Draw(icon_rect, new GUIContent(style.icon), false, false, false, false);
			}
		}
			
	} // Draw()

#endregion
} // EditorWindowItem()
#endif
