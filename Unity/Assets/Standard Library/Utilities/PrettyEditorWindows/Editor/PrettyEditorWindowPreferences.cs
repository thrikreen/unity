﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;

namespace PrettyEditorWindows
{
    /// <summary>
    /// PrettyEditorWindowPreferences 
    /// </summary>
    [System.Serializable, XmlRoot("PrettyEditorWindowPreferences")]
    public class PrettyEditorWindowPreferences
    {
        #region /- Variables -----------------------------------------------------------------------------------------------
        [XmlElement] public bool enabled                        = false;
        [XmlElement] public Rect rect                           = new Rect();

        [XmlElement] public bool enableProjectStyles            = false;
        [XmlElement] public bool expandProjectStyles            = false;
        [XmlElement] public List<ProjectWindowStyle> projectStyles = new List<ProjectWindowStyle>();

        [XmlElement] public bool enableHierarchyStyles          = false;
        [XmlElement] public bool expandHierarchyStyles          = false;

        #endregion

        #region /- Constructor ---------------------------------------------------------------------------------------------
        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        public PrettyEditorWindowPreferences()
        {
        } // PrettyEditorWindowPreferencesItem()

        /// <summary>
        /// Creates a duplicate of the provided PrettyEditorWindowPreferencesItem source.
        /// </summary>
        /// <param name="source"></param>
        public PrettyEditorWindowPreferences(PrettyEditorWindowPreferences source)
        {
            string debugOutput = "";

            // Clone via JSON, make a new object and source from that
            PrettyEditorWindowPreferences newSource = new PrettyEditorWindowPreferences();
            newSource = (PrettyEditorWindowPreferences)JsonUtility.FromJson(JsonUtility.ToJson(source), typeof(PrettyEditorWindowPreferences));

            // Parse every field on this class and duplicate the values over
            FieldInfo[] fields = this.GetType().GetFields(BindingFlags.Public|BindingFlags.NonPublic|BindingFlags.Instance);
            foreach (FieldInfo field in fields)
            {
                // Note: object references will copy the pointer, not make a new object. Need to deep copy it
                field.SetValue(this, field.GetValue(newSource));
                debugOutput += string.Format("- Field: {0} = {1}, {2}\n", field.Name, field.GetValue(newSource), field.GetType().IsByRef);
            } // loop fields

        } // PrettyEditorWindowPreferences()
        #endregion

    } // PrettyEditorWindowPreferences()
} // PrettyEditorWindows
