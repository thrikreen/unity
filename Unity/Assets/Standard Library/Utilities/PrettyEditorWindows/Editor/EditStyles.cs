﻿#pragma warning disable
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using UnityEditorInternal;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace PrettyEditorWindows
{
    /// <summary>
    /// 
    /// </summary>
    public class EditStyles_Editor : EditorWindow
    {
        #region /- Variables -----------------------------------------------------------------------------------------------
        public bool DEBUG                                   = true;
        Vector2 scrollPos;

        #region /- GUI Styles ----------------------------------------------------------------------------------------------
        private bool _initialized                           = false;
        private static float miniButtonWidth                = 30f;
        private static GUIStyle miniButtonStyle;
        private static GUIStyle miniButtonLeftStyle;
        private static GUIStyle miniButtonMidStyle;
        private static GUIStyle miniButtonRightStyle;
        private static GUIContent moveDownButtonContent;
        private static GUIContent moveUpButtonContent;
        private static GUIContent duplicateButtonContent;
        private static GUIContent addButtonContent;
        private static GUIContent deleteButtonContent;
        private static GUIContent expandAllButtonContent;
        private static GUIContent collapseAllButtonContent;
        private static GUIContent playButtonContent;

        private static GUIContent loadButtonContent;
        private static GUIContent saveButtonContent;
        private static GUIContent helpButtonContent;
        #endregion


        #endregion

        #region /- Initialization ------------------------------------------------------------------------------------------
        /// <summary>
        /// Runs whenever this Inspector panel is shown. Note: Will run multiple times if other Inspector panels are also opened.
        /// </summary>
        void OnEnable()
        {
            string debugOutput = "";

            _initialized = false;

            //if (DEBUG) Debug.LogFormat("[ {0} ] {1}(): {2}", "PEW", System.Reflection.MethodBase.GetCurrentMethod().Name, debugOutput);
        } // OnEnable()
        #endregion

        #region /- OnInspectorGUI ------------------------------------------------------------------------------------------
        /// <summary>
        /// Initializes the GUI, as some property types (GUIStyles) can't be assigned on declaration, only in OnGUI() calls.
        /// </summary>
        void InitializeGUI()
        {
            string debugOutput = "";

            // Button GUIStyles for the item listing - can't run GUIStyles outside of an OnGUI call
            miniButtonStyle = new GUIStyle(EditorStyles.miniButton);
            miniButtonLeftStyle = new GUIStyle(EditorStyles.miniButtonLeft);
            miniButtonMidStyle = new GUIStyle(EditorStyles.miniButtonMid);
            miniButtonRightStyle = new GUIStyle(EditorStyles.miniButtonRight);
            moveDownButtonContent = new GUIContent("\u25bC", "Move Item Down");
            moveUpButtonContent = new GUIContent("\u25b2", "Move Item Up");
            duplicateButtonContent = new GUIContent((Texture2D)EditorGUIUtility.Load("GUI/icons/icon_copy.png"), "Duplicate Item");
            addButtonContent = new GUIContent("+", "Add Item");
            deleteButtonContent = new GUIContent("-", "Delete Item");
            expandAllButtonContent = new GUIContent("\u25bC", "Expand All Items");
            collapseAllButtonContent = new GUIContent("\u25b2", "Collapse All Items");
            playButtonContent = new GUIContent(EditorGUIUtility.FindTexture("d_PlayButton"), "Set Item");

            loadButtonContent = new GUIContent("Load", "Load Styles");
            saveButtonContent = new GUIContent("Save", "Save Styles");
            helpButtonContent = new GUIContent("Help", "Open help for this utility.");

            // Other GUI Initializations here
            //PrettyEditorWindows.LoadPreferences();

            // Repopulate icon texture fields


            _initialized = true;

            //if (DEBUG) Debug.LogFormat("[ {0} ] {1}(): {2}", "PEW", System.Reflection.MethodBase.GetCurrentMethod().Name, debugOutput);
        } // InitializeGUI()

        /// <summary>
        /// InspectorGUI
        /// </summary>
        void OnGUI()
        {
            // Checks ------------------------------------------------------------------------------------------------------
            if (!_initialized) InitializeGUI(); // init the GUI

            // Inspector Body - Start --------------------------------------------------------------------------------------
            // Menu
            GUILayout.Space(10);
            EditorGUILayout.BeginHorizontal();
            {
                GUILayout.Space(10);
                if (GUILayout.Button(loadButtonContent)) PrettyEditorWindows.LoadPreferences();
                GUILayout.Space(10);
                if (GUILayout.Button(saveButtonContent)) PrettyEditorWindows.SavePreferences();
                GUILayout.Space(10);
                if (GUILayout.Button(helpButtonContent, GUILayout.Width(60))) HelpSystem.OpenURL("pew");
                GUILayout.Space(10);
            }
            EditorGUILayout.EndHorizontal();

            // Contents
            GUILayout.Space(10);
            EditorGUILayout.BeginHorizontal();
            {
                GUILayout.Space(10);
                PrettyEditorWindows.preferences.enabled = EditorGUILayout.Toggle(PrettyEditorWindows.preferences.enabled, GUILayout.Width(20));
                EditorGUILayout.LabelField("Enable");
            }
            EditorGUILayout.EndHorizontal();
            GUILayout.Space(10);

            scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
            DrawProjectStyles();
            EditorGUILayout.EndScrollView();


        } // OnInspectorGUI()
        #endregion

        #region /- Draw Project Lists ----------------------------------------------------------------------------------
        void DrawProjectStyles()
        {
            EditorGUILayout.BeginHorizontal();
            {
                GUILayout.Space(10);
                PrettyEditorWindows.preferences.enableProjectStyles = EditorGUILayout.Toggle(PrettyEditorWindows.preferences.enableProjectStyles, GUILayout.Width(20));
                PrettyEditorWindows.preferences.expandProjectStyles = EditorGUILayout.Foldout(PrettyEditorWindows.preferences.expandProjectStyles, "Project Styles");
                Rect rect = GUILayoutUtility.GetRect(position.width-100, EditorGUIUtility.singleLineHeight);
                DrawProjectListButtons(rect, PrettyEditorWindows.preferences.projectStyles);
            }
            EditorGUILayout.EndHorizontal();

            // ProjectStyle Items
            if (PrettyEditorWindows.preferences.expandProjectStyles)
            {
                GUILayout.Space(10);
                EditorGUI.indentLevel++;
                for (int i=0; i<PrettyEditorWindows.preferences.projectStyles.Count; i++)
                {
                    DrawProjectStyle(PrettyEditorWindows.preferences.projectStyles[i]);
                }
                EditorGUI.indentLevel--;
            } // Expanded
        } // DrawProjectStyles()

        void DrawProjectListButtons(Rect rect, List<ProjectWindowStyle> items)
        {
            // Add Item Button
            rect.width = miniButtonWidth * 2;
            rect.x = Screen.width - rect.width - 20;
            if (GUI.Button(rect, addButtonContent, miniButtonStyle))
            {
                ProjectWindowStyle newItem = new ProjectWindowStyle();
                newItem.id = "New Project Style " + items.Count;
                items.Add(newItem);
            }

            rect.width = miniButtonWidth;
            rect.x -= rect.width + 10;
            if (GUI.Button(rect, expandAllButtonContent, miniButtonRightStyle))
            {
                for (int i = 0; i < items.Count; i++) items[i].isExpanded = true;
            }
            rect.x -= rect.width;
            if (GUI.Button(rect, collapseAllButtonContent, miniButtonLeftStyle))
            {
                for (int i = 0; i < items.Count; i++) items[i].isExpanded = false;
            }
            EditorGUILayout.Space();
        } // DrawListButtons()

        void DrawProjectStyle(ProjectWindowStyle style)
        {
            Rect rect = GUILayoutUtility.GetRect(position.width - (miniButtonWidth * 4) - 50, EditorGUIUtility.singleLineHeight, GUILayout.ExpandWidth(false));
            rect.x += 30;
            style.isExpanded = EditorGUI.Foldout(rect, style.isExpanded, style.id);
            DrawProjectItemButtons(rect, style); // Draw the list manipulation buttons for this item
            if (style.isExpanded)
            {
                EditorGUI.indentLevel += 3;

                style.id = EditorGUILayout.TextField("Name", style.id, GUILayout.Width(500));
                style.color = EditorGUILayout.ColorField("Color", style.color, GUILayout.Width(500));
                style.offset = EditorGUILayout.Vector2IntField("Label Offset", style.offset, GUILayout.Width(500));

                // Pattern List
                Rect rectPattern = GUILayoutUtility.GetRect(450, (style.patterns.Count + 2) * (EditorGUIUtility.singleLineHeight + 5), GUILayout.ExpandWidth(false));
                rectPattern.x += 65;
                rectPattern.DrawRect(Color.blue);

                ReorderableList patternList = new ReorderableList(style.patterns, style.patterns.GetType(), true, true, true, true);
                patternList.drawHeaderCallback += (Rect rectHeader) => { EditorGUI.LabelField(rectHeader, "Asset Name Patterns"); };
                patternList.drawElementCallback += (Rect rectElement, int index, bool isActive, bool isFocused) =>
                {
                    style.patterns[index] = EditorGUI.TextField(rectElement, "Pattern " + index, style.patterns[index]);
                };
                patternList.onAddCallback += (ReorderableList list) => 
                {
                    Debug.Log("patternList.onAddCallback");
                    style.patterns.Add("");
                };
                patternList.onRemoveCallback += (ReorderableList list) =>
                {
                    Debug.Log("patternList.onRemoveCallback");
                };
                patternList.onReorderCallback += (ReorderableList list) =>
                {
                    Debug.Log("patternList.onReorderCallback");
                };
                patternList.onSelectCallback += (ReorderableList list) =>
                {
                    Debug.Log("patternList.onSelectCallback");
                };
                patternList.onChangedCallback += (ReorderableList list) =>
                {
                    Debug.Log("patternList.onChangedCallback");
                };

                patternList.DoList(rectPattern);
                
                style.filetype = EditorGUILayout.TextField("File Type", style.filetype, GUILayout.Width(500));

                /*
                EditorGUI.BeginChangeCheck();
                style.icon = (Texture2D)EditorGUILayout.ObjectField("Icon Texture", style.icon, typeof(Texture2D), GUILayout.Width(500));
                if (EditorGUI.EndChangeCheck()) style.icon_name = style.icon.name;
                style.icon_name = EditorGUILayout.TextField("Icon Name", style.icon_name, GUILayout.Width(500));
                style.recursive = EditorGUILayout.Toggle("Recursive", style.recursive, GUILayout.Width(500));
                */

                EditorGUI.indentLevel-=3;
            } // isExpanded
        } // DrawProjectStyle()
        
        /// <summary>
        /// Draws the list management button for this Item.
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="item"></param>
        public void DrawProjectItemButtons(Rect rect, ProjectWindowStyle item)
        {
            // Set the default width of the buttons
            rect.y -= 2;
            rect.width = miniButtonWidth;

            // Set the initial X position, right side of window - button width - padding
            // Delete Button
            rect.x = Screen.width - rect.width - 20;
            if (GUI.Button(rect, deleteButtonContent, miniButtonRightStyle))
            {
                PrettyEditorWindows.DeleteProjectItem(item);
            }

            // Duplicate Button
            rect.x -= rect.width;
            if (GUI.Button(rect, duplicateButtonContent, miniButtonMidStyle))
            {
                PrettyEditorWindows.DuplicateProjectItem(item);
            }

            // Move Down button
            rect.x -= rect.width;
            if (GUI.Button(rect, moveDownButtonContent, miniButtonMidStyle))
            {
                PrettyEditorWindows.MoveProjectItemDown(item);
            }

            // Move Up Button
            rect.x -= rect.width;
            if (GUI.Button(rect, moveUpButtonContent, miniButtonLeftStyle))
            {
                PrettyEditorWindows.MoveProjectItemUp(item);
            }
            
        } // DrawItemButtons()
        #endregion

    } // EditStyles_Editor()
} // PrettyEditorWindows

// Draw the list of Items
//DrawList(_target.m_items);

/*
/// <summary>
/// Draws the serialized property as if it was rendered in the default inspector (i.e. handles array/lists as a 
/// list, serialized XML as a tree, etc.)
/// </summary>
/// <param name="property_name">Name of the property (use "parent/child" for sub-properties).</param>
public void DrawProperty(string property_name)
{
    SerializedProperty property = serializedObject.FindProperty( property_name );
    if (property != null)
    {
        EditorGUI.BeginChangeCheck();

        // Draw the property, including children
        EditorGUILayout.PropertyField(property, true);

        if (EditorGUI.EndChangeCheck())
        {
            serializedObject.ApplyModifiedProperties();
        }
    }
} // DrawPropertyInspector()
*/


#if DISABLED
#region /- List ----------------------------------------------------------------------------------------------------
    /// <summary>
    /// Draws the list of Items
    /// </summary>
    public void DrawList(List<_TemplateItem> items)
	{
		// List Header
		SerializedProperty prop = serializedObject.FindProperty("m_items");
		if (prop == null) return;
		Rect rect = GUILayoutUtility.GetRect(EditorGUIUtility.fieldWidth, 18, GUILayout.ExpandWidth(false));
		prop.isExpanded = EditorGUI.Foldout(rect, prop.isExpanded, ObjectNames.NicifyVariableName(prop.name) + " ("+items.Count+")");

		DrawListButtons(rect, items, prop);

		if (prop.isExpanded)
		{
			EditorGUI.indentLevel++;
			//int size = EditorGUILayout.IntField("Size", items.Count);
			for (int i=0; i<items.Count; i++)
			{
				if ((items[i] != null) && (prop.arraySize == items.Count))
				{
					SerializedProperty propitem = prop.GetArrayElementAtIndex(i);
					DrawItem(items[i], propitem);
				}
			} // loop items

			EditorGUI.indentLevel--;
		} // // isExpanded

	} // DrawList()

	/// <summary>
	///
	/// </summary>
	/// <param name="items"></param>
	/// <param name="rect"></param>
	/// <param name="prop"></param>
	void DrawListButtons(Rect rect, List<_TemplateItem> items, SerializedProperty prop)
	{
		// Add Item Button
		rect.width = miniButtonWidth * 2;
		rect.x = Screen.width - rect.width - 20;
		if (GUI.Button(rect, addButtonContent, miniButtonStyle))
		{
            _TemplateItem newItem = new _TemplateItem();
			newItem.name = "New Item " + _target.m_items.Count;
			_target.AddItem(newItem);
		}

		rect.width = miniButtonWidth;
		rect.x -= rect.width + 10;
		if (GUI.Button(rect, expandAllButtonContent, miniButtonRightStyle))
		{
			for (int i = 0; i < items.Count; i++)
			{
				SerializedProperty propitem = prop.GetArrayElementAtIndex(i);
				propitem.isExpanded = true;
			} // loop items
		}
		rect.x -= rect.width;
		if (GUI.Button(rect, collapseAllButtonContent, miniButtonLeftStyle))
		{
			for (int i = 0; i < items.Count; i++)
			{
				SerializedProperty propitem = prop.GetArrayElementAtIndex(i);
				propitem.isExpanded = false;
			} // loop items
		}
		EditorGUILayout.Space();
	} // DrawListButtons()

	/// <summary>
	/// Draws an individual Item.
	/// </summary>
	/// <param name="item"></param>
	/// <param name="prop"></param>
	public void DrawItem(_TemplateItem item, SerializedProperty prop=null)
	{
		if (prop == null) return;

		Rect rect = GUILayoutUtility.GetRect(EditorGUIUtility.fieldWidth, 18, GUILayout.ExpandWidth(false));
		prop.isExpanded = EditorGUI.Foldout(rect, prop.isExpanded, item.name);
		DrawItemButtons(rect, item); // Draw the list manipulation buttons for this item
		if (prop.isExpanded)
		{
			EditorGUI.indentLevel++;

			// Item Properties
			bool childrenAreExpanded = true;
			int propertyStartingDepth = prop.depth;
			while (prop.NextVisible(childrenAreExpanded) && (propertyStartingDepth < prop.depth))
			{
				childrenAreExpanded = EditorGUILayout.PropertyField(prop, true);
			}

			EditorGUI.indentLevel--;
		} // isExpanded

		EditorGUILayout.Space();
	} // DrawItem()


	/// <summary>
	/// Draws the list management button for this Item.
	/// </summary>
	/// <param name="rect"></param>
	/// <param name="item"></param>
	public void DrawItemButtons(Rect rect, _TemplateItem item)
	{
		// Set the default width of the buttons
		rect.width = miniButtonWidth;

		// Set the initial X position, right side of window - button width - padding
		// Delete Button
		rect.x = Screen.width - rect.width - 20;
		if (GUI.Button(rect, deleteButtonContent, miniButtonRightStyle))
		{
			_target.DeleteItem(item);
		}

		// Duplicate Button
		rect.x -= rect.width;
		if (GUI.Button(rect, duplicateButtonContent, miniButtonMidStyle))
		{
			_target.DuplicateItem(item);
		}

		// Move Down button
		rect.x -= rect.width;
		if (GUI.Button(rect, moveDownButtonContent, miniButtonMidStyle))
		{
			_target.MoveItemDown(item);
		}

		// Move Up Button
		rect.x -= rect.width;
		if (GUI.Button(rect, moveUpButtonContent, miniButtonLeftStyle))
		{
			_target.MoveItemUp(item);
		}

		// Play Button
		rect.width = miniButtonWidth * 2f;
		rect.x -= rect.width + 10;
		if (GUI.Button(rect, playButtonContent, miniButtonStyle))
		{
			_target.SetItem(item);
		}

	} // DrawItemButtons()
#endregion
#endif
