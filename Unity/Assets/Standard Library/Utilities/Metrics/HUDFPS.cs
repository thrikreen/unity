﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class HUDFPS : MonoBehaviour 
{
	// Attach this to a GUIText to make a frames/second indicator.
	//
	// It calculates frames/second over each updateInterval,
	// so the display does not keep changing wildly.
	//
	// It is also fairly accurate at very low FPS counts (<10).
	// We do this not by simply counting frames per interval, but
	// by accumulating FPS for each frame. This way we end up with
	// correct overall FPS even if the interval renders something like
	// 5.5 frames.
	#region /- Variables -----------------------------------------------------------------------------------------------
	public float updateInterval		= 0.5F;

	private float accum				= 0;    // FPS accumulated over the interval
	private int   frames			= 0;    // Frames drawn over the interval
	private float timeleft;                 // Left time for current interval

	public Color fps_ok_color		= Color.green;

	public float fps_warning		= 30.0f;
	public Color fps_warning_color 	= Color.yellow;

	public float fps_bad			= 10.0f;
	public Color fps_bad_color		= Color.red;

	public bool calculateAverage    = true;
	public float averageRange		= 10.0f; // Seconds 
	private int maxAverageCount		= 10;
	private List<float> fpshistory  = new List<float>();
	private float avgFontSize       = 0.0f;

	Text text_obj;
	#endregion

	#region /- Initialize ----------------------------------------------------------------------------------------------
	void Start()
	{
		// Check if there's a Text component to access.
		text_obj = GetComponent<Text>();
		if (!text_obj)
		{
			Debug.LogError("[ "+GetType().Name+" ] HUDFPS needs a UI.Text component!", gameObject);
			enabled = false;
			return;
		} 

		avgFontSize = text_obj.fontSize / 2.0f;
		maxAverageCount = (int)(averageRange / updateInterval);

		timeleft = updateInterval;
	} // Start()
	#endregion
	
	#region /- Update --------------------------------------------------------------------------------------------------
	void Update()
	{
		timeleft -= Time.deltaTime;
		accum += Time.timeScale/Time.deltaTime;
		++frames;
		
		// Interval ended - update GUI text and start new interval
		if ( timeleft <= 0.0 )
		{
			// Display two fractional digits (f2 format)
			float fps = accum/frames;

			// Display the FPS
			string format = System.String.Format("{0:0.00} FPS", fps);

			// Calculate the average
			if (calculateAverage)
			{
				fpshistory.Add(fps);
				while (fpshistory.Count > maxAverageCount) fpshistory.RemoveAt(0);
				float sum = 0.0f;
				for (int i = 0; i < fpshistory.Count; i++) sum += fpshistory[i];
				float avg = sum / fpshistory.Count;

				// Display the FPS
				format = System.String.Format("{0:F2} FPS\n<size={1}>Avg: {2:0.00} FPS</size>", fps, avgFontSize, avg);
			}

			// FPS Text 
			text_obj.text = format;

			// FPS Colour
			if (fps < fps_warning)
			{
				text_obj.color = fps_warning_color;
			}
			else if (fps < fps_bad)
			{
				text_obj.color = fps_bad_color;
			}
			else
			{
				text_obj.color = fps_ok_color;
			}

			//	DebugConsole.Log(format,level);
			timeleft = updateInterval;
			accum = 0.0F;
			frames = 0;
		}

	} // Update()
	#endregion

} // HUDFPS()