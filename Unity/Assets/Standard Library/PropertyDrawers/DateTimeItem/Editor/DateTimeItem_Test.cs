﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System;

public class DateTimeItem_Test
{
    string dateFormat = "yyyy-MM-dd HH:mm:ss.fff";

    [Test]
    public void CreateByDateTimeItem()
    {
        // Setup
        DateTime currentTime = DateTime.Now;

        // Execute
        DateTimeItem dateTimeItem1 = new DateTimeItem(currentTime);
        DateTimeItem dateTimeItem2 = new DateTimeItem(dateTimeItem1);

        // Results
        Assert.AreEqual(dateTimeItem1.datetimestring, dateTimeItem2.datetimestring);
    } // CreateByDateTimeItem()

    [Test]
    public void CreateByDateTime()
    {
        // Setup
        DateTime currentTime = DateTime.Now;

        // Execute
        DateTimeItem dateTimeItem = new DateTimeItem(currentTime);

        // Results
        Assert.AreEqual(currentTime.ToString(dateFormat), dateTimeItem.datetime.ToString(dateFormat));
    } // CreateByDateTime()

    [Test]
    public void CreateByString()
    {
        // Setup
        DateTime currentTime = DateTime.Now;

        // Execute
        DateTimeItem dateTimeItem = new DateTimeItem(currentTime.ToString(dateFormat));

        // Results
        Assert.AreEqual(currentTime.ToString(dateFormat), dateTimeItem.datetime.ToString(dateFormat));
    } // CreateByString()

    [Test]
    public void IncrementByYear()
    {
        // Setup
        DateTime currentTime = DateTime.Now;

        // Execute
        DateTimeItem dateTimeItem = new DateTimeItem(currentTime);

        currentTime.AddYears(1);
        dateTimeItem.datetime.AddYears(1);

        // Results
        Assert.AreEqual(currentTime.ToString(dateFormat), dateTimeItem.datetime.ToString(dateFormat));
    } // IncrementByYear()

    [Test]
    public void IncrementByMonth()
    {
        // Setup
        DateTime currentTime = DateTime.Now;

        // Execute
        DateTimeItem dateTimeItem = new DateTimeItem(currentTime);

        currentTime.AddMonths(1);
        dateTimeItem.datetime.AddMonths(1);

        // Results
        Assert.AreEqual(currentTime.ToString(dateFormat), dateTimeItem.datetime.ToString(dateFormat));
    } // IncrementByMonth()

    [Test]
    public void IncrementByDay()
    {
        // Setup
        DateTime currentTime = DateTime.Now;

        // Execute
        DateTimeItem dateTimeItem = new DateTimeItem(currentTime);

        currentTime.AddDays(1);
        dateTimeItem.datetime.AddDays(1);

        // Results
        Assert.AreEqual(currentTime.ToString(dateFormat), dateTimeItem.datetime.ToString(dateFormat));
    } // IncrementByDay()

    [Test]
    public void IncrementByHour()
    {
        // Setup
        DateTime currentTime = DateTime.Now;

        // Execute
        DateTimeItem dateTimeItem = new DateTimeItem(currentTime);

        currentTime.AddHours(1);
        dateTimeItem.datetime.AddHours(1);

        // Results
        Assert.AreEqual(currentTime.ToString(dateFormat), dateTimeItem.datetime.ToString(dateFormat));
    } // IncrementByHour()

    [Test]
    public void IncrementByMinute()
    {
        // Setup
        DateTime currentTime = DateTime.Now;

        // Execute
        DateTimeItem dateTimeItem = new DateTimeItem(currentTime);

        currentTime.AddMinutes(1);
        dateTimeItem.datetime.AddMinutes(1);

        // Results
        Assert.AreEqual(currentTime.ToString(dateFormat), dateTimeItem.datetime.ToString(dateFormat));
    } // IncrementByMinute()

    [Test]
    public void IncrementBySecond()
    {
        // Setup
        DateTime currentTime = DateTime.Now;

        // Execute
        DateTimeItem dateTimeItem = new DateTimeItem(currentTime);

        currentTime.AddSeconds(1);
        dateTimeItem.datetime.AddSeconds(1);

        // Results
        Assert.AreEqual(currentTime.ToString(dateFormat), dateTimeItem.datetime.ToString(dateFormat));
    } // IncrementBySecond()

    [Test]
    public void IncrementByMillisecond()
    {
        // Setup
        DateTime currentTime = DateTime.Now;

        // Execute
        DateTimeItem dateTimeItem = new DateTimeItem(currentTime);

        currentTime.AddMilliseconds(1);
        dateTimeItem.datetime.AddMilliseconds(1);

        // Results
        Assert.AreEqual(currentTime.ToString(dateFormat), dateTimeItem.datetime.ToString(dateFormat));
    } // IncrementByMillisecond()

} // DateTimeItem_Test()
