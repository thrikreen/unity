﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.Linq;
using System.Reflection;

[CustomPropertyDrawer(typeof(DateTimeItemAttribute))]
public class DateTimeItemPropertyDrawer : PropertyDrawer
{
    #region /- Variables -----------------------------------------------------------------------------------------------
    DateTimeItemAttribute drawerAttribute { get { return ((DateTimeItemAttribute)this.attribute); } }
    DateTimeItem _target;
    DateTime _targetDateTime;

    // GUI
    bool _initialized                               = false;

    string currentDateTime                          = "";
    int currentYear                                 = 0;
    int currentMonth                                = 0;
    int currentDay                                  = 0;
    int currentHour                                 = 0;
    int currentMinute                               = 0;
    int currentSecond                               = 0;
    int currentMillisecond                          = 0;

    //string[] year_list                              = new string[100];
    string[] month_list                             = new string[12];
    string[] day_list                               = new string[31];
    string[] hour_list                              = new string[24];
    string[] minute_list                            = new string[60];
    string[] second_list                            = new string[60];

    // GUI Style
    private static GUIStyle miniButtonLeftStyle;
    private static GUIStyle miniButtonRightStyle;
    private static GUIContent increaseYearButtonContent;
    private static GUIContent decreaseYearButtonContent;
    private static GUIContent increaseMonthButtonContent;
    private static GUIContent decreaseMonthButtonContent;
    private static GUIContent increaseDayButtonContent;
    private static GUIContent decreaseDayButtonContent;
    private static GUIContent increaseHourButtonContent;
    private static GUIContent decreaseHourButtonContent;
    private static GUIContent increaseMinuteButtonContent;
    private static GUIContent decreaseMinuteButtonContent;
    private static GUIContent increaseSecondButtonContent;
    private static GUIContent decreaseSecondButtonContent;
    private static GUIContent increaseMilliSecondButtonContent;
    private static GUIContent decreaseMilliSecondButtonContent;
    #endregion

    #region /- GetPropertyHeight ---------------------------------------------------------------------------------------
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        float property_height = EditorGUIUtility.singleLineHeight;

        if (property.isExpanded)
        {
            property_height *= 9; // Header + Y, M, D, H, M, S, MS, buttons
        } // isExpanded

        return (property_height);
    } // GetPropertyHeight()
    #endregion

    #region /- InitializeGUI -------------------------------------------------------------------------------------------
    /// <summary>
    /// Initializes the GUI elements - runs once whenever the InspectorUI panel opens and displays for this PropertyDrawer object.
    /// </summary>
    /// <param name="property">Property.</param>
    void InitializeGUI(Rect position, SerializedProperty property)
    {
        // Get a reference to the class data instance that is utilizing this PropertyDrawer
        //object targetProperty = this.fieldInfo.GetValue(GetParent(property));
        _target = this.fieldInfo.GetValue(EditorPropertyDrawer_Extensions.GetParent(property)) as DateTimeItem;

        // Update the DateTimeItem, set string from internal value if not set.
        if (string.IsNullOrEmpty(_target.datetimestring))
        {
            // Set the format to what was set in the property drawer attribute (defaults to yyyy-MM-dd HH:mm:ss.fff)
            _target.datetimestring = _target.datetime.ToString(drawerAttribute.format);
        }

        // Populate the values for the dropdown lists
        // This should be done every time as the days will vary depending on the month
        UpdateDateTime();

        if (!_initialized) // Stuff that only needs to happen once
        {
            // GUI Style
            miniButtonLeftStyle = new GUIStyle(EditorStyles.miniButtonLeft);
            miniButtonRightStyle = new GUIStyle(EditorStyles.miniButtonRight);

            // Date
            increaseYearButtonContent = new GUIContent("+", "Add Year");
            decreaseYearButtonContent = new GUIContent("-", "Subtract Year");
            increaseMonthButtonContent = new GUIContent("+", "Add Month");
            decreaseMonthButtonContent = new GUIContent("-", "Subtract Month");
            increaseDayButtonContent = new GUIContent("+", "Add Day");
            decreaseDayButtonContent = new GUIContent("-", "Subtract Day");

            // Time
            increaseHourButtonContent = new GUIContent("+", "Add Hour");
            decreaseHourButtonContent = new GUIContent("-", "Subtract Hour");
            increaseMinuteButtonContent = new GUIContent("+", "Add Minute");
            decreaseMinuteButtonContent = new GUIContent("-", "Subtract Minute");
            increaseSecondButtonContent = new GUIContent("+", "Add Second");
            decreaseSecondButtonContent = new GUIContent("-", "Subtract Second");
            increaseMilliSecondButtonContent = new GUIContent("+", "Add MilliSecond");
            decreaseMilliSecondButtonContent = new GUIContent("-", "Subtract MilliSecond");

            // Get the Month names to populate the dropdown list.
            DateTime date = new DateTime(2000, 1, 1, 0, 0, 0);
            for (int m = 0; m < month_list.Length; m++)
            {
                month_list[m] = date.ToString("MMMM");
                date = date.AddMonths(1);
            }

            // Populate the lists for Hours and Min/Secs
            for (int h = 0; h < 24; h++) hour_list[h] = h.ToString();
            for (int m = 0; m < 60; m++) minute_list[m] = m.ToString();
            for (int s = 0; s < 60; s++) second_list[s] = s.ToString();
            UpdateDaysInMonth(currentYear, currentMonth);

            _initialized = true;
        }

        //Debug.LogFormat("InitializeGUI(): {0} ({1})", targetProperty, targetProperty.GetType().Name);
    } // InitializeGUI()
    #endregion

    #region /- OnGUI ---------------------------------------------------------------------------------------------------
    /// <summary>
    /// Draw the custom PropertyDrawer for this property.
    /// </summary>
    /// <param name="position">Rect position for this control.</param>
    /// <param name="property">The serialized property.</param>
    /// <param name="label">GUIContent for displaying the property (style, tooltips, etc.)</param>
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        // Init 
        InitializeGUI(position, property);

        // Main
        DrawDateTimeItemDrawer(position, property, label);

        if (GUI.changed)
        {
            //Debug.LogWarningFormat(property.serializedObject.targetObject, "[ {0} ] {1}():\n{2}", GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, "GUI.changed");
            UpdateDateTime();
            EditorUtility.SetDirty(property.serializedObject.targetObject);
        }
    } // OnGUI()

    /// <summary>
    /// Draw the custom PropertyDrawer for the DateTimeItem.
    /// </summary>
    /// <param name="position">Rect position for this control.</param>
    /// <param name="property">The serialized property.</param>
    /// <param name="label"></param>
    public void DrawDateTimeItemDrawer(Rect position, SerializedProperty property, GUIContent label)
    {
        position.height = EditorGUIUtility.singleLineHeight; // reset the height

        // DateTime text field
        EditorGUI.BeginChangeCheck();
        currentDateTime = EditorGUI.TextField(position, label, currentDateTime);
        if (EditorGUI.EndChangeCheck())
        {
            // Test if the DateTime string is valid, if so update it.
            DateTime newDateTime = new DateTime();
            if (DateTime.TryParse(currentDateTime, out newDateTime))
            {
                _target.datetime = newDateTime;
            }
            // else Failed to parse properly, do nothing - avoids the class spitting out error messages
        } // EndChangeCheck()

        property.isExpanded = EditorGUI.Foldout(position, property.isExpanded, "");
        if (property.isExpanded)
        {
            // Update the dropdown lists
            UpdateDateTime();

            // Draw the dropdown lists
            DrawDateTimeDropDown(position, property);
        } // isExpanded

    } // DrawDateTimeItemDrawer()

    /// <summary>
    /// Draws the dropdown lists for the DateTime, makes it easier to manipulate the values.
    /// </summary>
    /// <param name="position">Rect position for this control.</param>
    /// <param name="property">The serialized property.</param>
    void DrawDateTimeDropDown(Rect position, SerializedProperty property)
    {
        // TO DO: Modularize this to make management easier.
        bool dirty = false;
        EditorGUI.indentLevel++;

        // Adjust width for the +/- buttons
        position.width -= 60;
        Rect decreaseButtonPosition = new Rect(position);
        Rect increaseButtonPosition = new Rect(position);
        decreaseButtonPosition.width = 25;
        increaseButtonPosition.width = 25;
        decreaseButtonPosition.x += position.width + 5;
        increaseButtonPosition.x = decreaseButtonPosition.x + decreaseButtonPosition.width;

        // Year
        position.y += position.height;
        increaseButtonPosition.y = position.y;
        decreaseButtonPosition.y = position.y;
        EditorGUI.BeginChangeCheck();
        currentYear = EditorGUI.IntField(position, "Year", currentYear);
        if (EditorGUI.EndChangeCheck()) dirty = true;
        if (GUI.Button(decreaseButtonPosition, decreaseYearButtonContent, miniButtonLeftStyle)) { currentYear--; dirty = true; }
        if (GUI.Button(increaseButtonPosition, increaseYearButtonContent, miniButtonRightStyle)) { currentYear++; dirty = true; }

        // Month
        position.y += position.height;
        increaseButtonPosition.y = position.y;
        decreaseButtonPosition.y = position.y;
        EditorGUI.BeginChangeCheck();
        currentMonth = EditorGUI.Popup(position, "Month", currentMonth - 1, month_list) + 1;
        if (EditorGUI.EndChangeCheck()) dirty = true;
        if (GUI.Button(decreaseButtonPosition, decreaseMonthButtonContent, miniButtonLeftStyle)) { currentMonth--; dirty = true; }
        if (GUI.Button(increaseButtonPosition, increaseMonthButtonContent, miniButtonRightStyle)) { currentMonth++; dirty = true; }

        // Day
        position.y += position.height;
        increaseButtonPosition.y = position.y;
        decreaseButtonPosition.y = position.y;
        EditorGUI.BeginChangeCheck();
        currentDay = EditorGUI.Popup(position, "Day", currentDay - 1, day_list) + 1;
        if (EditorGUI.EndChangeCheck()) dirty = true;
        if (GUI.Button(decreaseButtonPosition, decreaseDayButtonContent, miniButtonLeftStyle)) { currentDay--; dirty = true; }
        if (GUI.Button(increaseButtonPosition, increaseDayButtonContent, miniButtonRightStyle)) { currentDay++; dirty = true; }

        // Hour
        position.y += position.height;
        increaseButtonPosition.y = position.y;
        decreaseButtonPosition.y = position.y;
        EditorGUI.BeginChangeCheck();
        currentHour = EditorGUI.Popup(position, "Hour", currentHour, hour_list);
        if (EditorGUI.EndChangeCheck()) dirty = true;
        if (GUI.Button(decreaseButtonPosition, decreaseHourButtonContent, miniButtonLeftStyle)) { currentHour--; dirty = true; }
        if (GUI.Button(increaseButtonPosition, increaseHourButtonContent, miniButtonRightStyle)) { currentHour++; dirty = true; }

        // Minute
        position.y += position.height;
        increaseButtonPosition.y = position.y;
        decreaseButtonPosition.y = position.y;
        EditorGUI.BeginChangeCheck();
        currentMinute = EditorGUI.Popup(position, "Minute", currentMinute, minute_list);
        if (EditorGUI.EndChangeCheck()) dirty = true;
        if (GUI.Button(decreaseButtonPosition, decreaseMinuteButtonContent, miniButtonLeftStyle)) { currentMinute--; dirty = true; }
        if (GUI.Button(increaseButtonPosition, increaseMinuteButtonContent, miniButtonRightStyle)) { currentMinute++; dirty = true; }

        // Second
        position.y += position.height;
        increaseButtonPosition.y = position.y;
        decreaseButtonPosition.y = position.y;
        EditorGUI.BeginChangeCheck();
        currentSecond = EditorGUI.Popup(position, "Second", currentSecond, second_list);
        if (EditorGUI.EndChangeCheck()) dirty = true;
        if (GUI.Button(decreaseButtonPosition, decreaseSecondButtonContent, miniButtonLeftStyle)) { currentSecond--; dirty = true; }
        if (GUI.Button(increaseButtonPosition, increaseSecondButtonContent, miniButtonRightStyle)) { currentSecond++; dirty = true; }

        // Millisecond
        position.y += position.height;
        increaseButtonPosition.y = position.y;
        decreaseButtonPosition.y = position.y;
        EditorGUI.BeginChangeCheck();
        currentMillisecond = EditorGUI.IntField(position, "Millisecond", currentMillisecond);
        if (EditorGUI.EndChangeCheck()) dirty = true;
        if (GUI.Button(decreaseButtonPosition, decreaseMilliSecondButtonContent, miniButtonLeftStyle)) { currentMillisecond--; dirty = true; }
        if (GUI.Button(increaseButtonPosition, increaseMilliSecondButtonContent, miniButtonRightStyle)) { currentMillisecond++; dirty = true; }

        // Buttons
        position.y += position.height;
        position.x += EditorGUIUtility.labelWidth;
        position.width = Screen.width - position.x - 20;
        if (GUI.Button(position, "Now"))
        {
            _target.datetime = DateTime.Now;
        }

        EditorGUI.indentLevel--;

        // Update the new DateTime if any values have changed
        if (dirty)
        {
            // Time
            if (currentMillisecond < 0) { currentMillisecond = 999; currentSecond--; }
            else if (currentMillisecond > 999) { currentMillisecond = 0; currentSecond++; }
            if (currentSecond < 0) { currentSecond = 59; currentMinute--; }
            else if (currentSecond > 59) { currentSecond = 0; currentMinute++; }
            if (currentMinute < 0) { currentMinute = 59; currentHour--; }
            else if (currentMinute > 59) { currentMinute = 0; currentHour++; }
            if (currentHour < 0) { currentHour = 23; currentDay--; }
            else if (currentHour > 23) { currentHour = 0; currentDay++; }

            // Date - a bit more tricky due to differences between months
            if (currentDay < 1) { currentMonth--; }
            else if (currentDay > day_list.Length) { currentDay = 1; currentMonth++; }
            if (currentMonth < 1) { currentMonth = 12; currentYear--; }
            else if (currentMonth > 12) { currentMonth = 1; currentYear++; }

            UpdateDaysInMonth(currentYear, currentMonth);

            if (currentDay < 1) currentDay = day_list.Length; // recalc the last day of the month

            _target.datetime = new DateTime(currentYear, currentMonth, currentDay, currentHour, currentMinute, currentSecond, currentMillisecond);
        } // update date

    } // DrawDateTimeDropDown()
    #endregion

    #region /- DateTime -----------------------------------------------------------------------------------------------
    /// <summary>
    /// Populates the PropertyDrawer's values from the target class.
    /// </summary>
    public void UpdateDateTime()
    {
        if ((_target != null) && (drawerAttribute != null))
        {
            currentDateTime = _target.datetime.ToString(drawerAttribute.format); // Use the string format specified in the PropertyDrawer attribute.
            currentYear = _target.datetime.Year;
            currentMonth = _target.datetime.Month;
            currentDay = _target.datetime.Day;
            currentHour = _target.datetime.Hour;
            currentMinute = _target.datetime.Minute;
            currentSecond = _target.datetime.Second;
            currentMillisecond = _target.datetime.Millisecond;
        }
        else
        {
            if (_target == null) Debug.LogError("Target is null");
            if (drawerAttribute == null) Debug.LogError("dateTimeAttribute is null");
        }

    } // UpdateDateTime()

    /// <summary>
    /// Updates the allowed days in the dropdown list, based on the active month.
    /// </summary>
    /// <param name="year">Year of the month to use.</param>
    /// <param name="month">Month to use.</param>
    public void UpdateDaysInMonth(int year, int month)
    {
        // Update Days for the active Month
        int days = DateTime.DaysInMonth(year, month);
        day_list = new string[days];
        for (int d = 0; d < days; d++) day_list[d] = (d + 1).ToString();
        currentDay = Mathf.Clamp(currentDay, 0, days);
    } // UpdateDaysInMonth()
    #endregion

} // DateTimePropertyDrawer()
