﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Class for the DateTimeDisplay Attribute.
/// </summary>
public class DateTimeItemAttribute : PropertyAttribute 
{
	#region /- Variables -----------------------------------------------------------------------------------------------
	public string format;
	#endregion

	#region /- Constructor ---------------------------------------------------------------------------------------------
	/// <summary>
	/// Initializes a new instance of the <see cref="DateTimeAttribute"/> class.
	/// </summary>
	/// <param name="new_label">String to use for the InspectorLabel.</param>
	public DateTimeItemAttribute(string new_format)
	{
		this.format = new_format;
	} // DateTimeAttribute()

    /// <summary>
    /// Initializes a new instance of the <see cref="DateTimeAttribute"/> class.
    /// </summary>
    public DateTimeItemAttribute()
	{
		this.format = "yyyy/MM/dd HH:mm:ss.fff";
	} // DateTimeAttribute()
	#endregion

} // DateTimeAttribute()
