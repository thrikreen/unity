﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

[System.Serializable, XmlRoot("datetimeitem")]
public class DateTimeItem
{
	#region /- Variables -----------------------------------------------------------------------------------------------
    [XmlElement] public DateTime _datetime              = new DateTime(2000, 1, 1, 12, 0, 0, 0);
    [XmlIgnore] public DateTime datetime
    {
        get
        {
            if (!this._initialized)
            {
                this._initialized = true;
                DateTime newDateTime = new DateTime();
                if (DateTime.TryParse(this.datetimestring, out newDateTime))
                {
                    this._datetime = newDateTime;
                }
            }
            return (this._datetime);
        } // get
        set
        {
            this._initialized = true;
            this._datetime = value;
            this.datetimestring = this._datetime.ToString("yyyy-MM-dd HH:mm:ss.fff");
        } // set
    } // datetime
    [XmlElement] public string datetimestring           = "2000-01-01 12:00:00.000";
    [XmlIgnore] public bool _initialized                = false;
    #endregion

    #region /- Constructor ---------------------------------------------------------------------------------------------
    /// <summary>
    /// Initializes a new instance of the <see cref="DateTimeItem"/> class.
    /// </summary>
    public DateTimeItem()
	{
        this._initialized = false;
        this.datetime = new DateTime();
	} // DateTimeItem()

	/// <summary>
	/// Initializes a new instance of the <see cref="DateTimeItem"/> class.
	/// </summary>
	/// <param name="source">Source DateTimeItem to copy from.</param>
	public DateTimeItem(DateTimeItem source)
	{
        this.datetime = new DateTime(source.datetime.Ticks);
    } // DateTimeItem()

    /// <summary>
    /// Initializes a new instance of the <see cref="DateTimeItem"/> class.
    /// </summary>
    /// <param name="source">Source DateTime object to store.</param>
    public DateTimeItem(DateTime source)
	{
        this.datetime = new DateTime(source.Ticks);
    } // DateTimeItem()

    /// <summary>
    /// Initializes a new instance of the <see cref="DateTimeItem"/> class.
    /// </summary>
    /// <param name="source">DateTime string format to parse into a DateTime object.</param>
    public DateTimeItem(string source)
	{
        DateTime newDateTime = new DateTime();
        if (DateTime.TryParse(source, out newDateTime))
        {
            this.datetime = newDateTime;
        }
        else // Failed to parse the time string
        {
            Debug.LogErrorFormat("[ {0} ] {1}(): Failed to parse '{2}'", GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, source);
        }
    } // DateTimeItem()
    #endregion

} // DateTimeItem()
