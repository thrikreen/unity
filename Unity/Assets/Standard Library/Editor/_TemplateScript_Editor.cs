﻿/*
// ---------------------------------------------------------------------------------------------------------------------
//	_TemplateScript_Editor.cs
// ---------------------------------------------------------------------------------------------------------------------

Description



Update History:


To Do:


Author(s):
-

*/
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;

[CustomEditor(typeof(_TemplateScript))]
public class _TemplateScript_Editor : Editor
{
	#region /- Variables -----------------------------------------------------------------------------------------------
	const string CLASSNAME								= "_TemplateScript";
	public static _TemplateScript _target;
	public static MonoScript _target_script;
	public static MonoScript _editor_script;

	#region /- GUI Styles ----------------------------------------------------------------------------------------------
	private bool _initialized							= false;
	private static float miniButtonWidth				= 30f;
	private static GUIStyle miniButtonStyle;
	private static GUIStyle miniButtonLeftStyle;
	private static GUIStyle miniButtonMidStyle;
	private static GUIStyle miniButtonRightStyle;
	private static GUIContent moveDownButtonContent;
	private static GUIContent moveUpButtonContent;
	private static GUIContent duplicateButtonContent;
	private static GUIContent addButtonContent;
	private static GUIContent deleteButtonContent;
	private static GUIContent expandAllButtonContent;
	private static GUIContent collapseAllButtonContent;
	private static GUIContent playButtonContent;
	#endregion

	#endregion

	#region /- Help Menu -----------------------------------------------------------------------------------------------
	[MenuItem("CONTEXT/" + CLASSNAME + "/Edit " + CLASSNAME + "_Editor Script")]
	static void Menu_EditScript(MenuCommand command) { AssetDatabase.OpenAsset(_editor_script); }
	[MenuItem("CONTEXT/" + CLASSNAME + "/Find " + CLASSNAME + " Script")]
	static void Menu_PingScript(MenuCommand command) { EditorGUIUtility.PingObject(_target_script); }
	[MenuItem("CONTEXT/" + CLASSNAME + "/Find " + CLASSNAME + " Editor Script")]
	static void Menu_PingEditorScript(MenuCommand command) { EditorGUIUtility.PingObject(_editor_script); }
	#endregion

	#region /- Initialization ------------------------------------------------------------------------------------------
	/// <summary>
	/// Runs whenever this Inspector panel is shown. Note: Will run multiple times if other Inspector panels are also opened.
	/// </summary>
	void OnEnable()
	{
		_target = (_TemplateScript) target;
		_target_script = MonoScript.FromMonoBehaviour(_target);
		_editor_script = MonoScript.FromScriptableObject(this);

		_initialized = false;
	} // OnEnable()
	#endregion

	#region /- OnInspectorGUI ------------------------------------------------------------------------------------------
	/// <summary>
	/// Initializes the GUI, as some property types (GUIStyles) can't be assigned on declaration, only in OnGUI() calls.
	/// </summary>
	void InitializeGUI()
	{
		// Button GUIStyles for the item listing - can't run GUIStyles outside of an OnGUI call
		miniButtonStyle				= new GUIStyle(EditorStyles.miniButton);
		miniButtonLeftStyle			= new GUIStyle(EditorStyles.miniButtonLeft);
		miniButtonMidStyle			= new GUIStyle(EditorStyles.miniButtonMid);
		miniButtonRightStyle		= new GUIStyle(EditorStyles.miniButtonRight);
		moveDownButtonContent		= new GUIContent("\u25bC", "Move Item Down");
		moveUpButtonContent			= new GUIContent("\u25b2", "Move Item Up");
		duplicateButtonContent		= new GUIContent((Texture2D)EditorGUIUtility.Load("GUI/icons/icon_copy.png"), "Duplicate Item");
		addButtonContent			= new GUIContent("+", "Add Item");
		deleteButtonContent			= new GUIContent("-", "Delete Item");
		expandAllButtonContent		= new GUIContent("\u25bC", "Expand All Items");
		collapseAllButtonContent	= new GUIContent("\u25b2", "Collapse All Items");
		playButtonContent			= new GUIContent(EditorGUIUtility.FindTexture("d_PlayButton"), "Set Item");

        // Other GUI Initializations here
        


        _initialized = true;
	} // InitializeGUI()

	/// <summary>
	/// InspectorGUI
	/// </summary>
	public override void OnInspectorGUI()
	{
		// Update the object's properties after setting them the previous frame.
		serializedObject.UpdateIfRequiredOrScript();

        // Checks ------------------------------------------------------------------------------------------------------
        if (!_initialized)
        {
            InitializeGUI(); // init the GUI
            if (_target.prefsFilename == "") _target.prefsFilename = Path.Combine(Application.dataPath, "xml/templatescript.xml");
            _target.prefsFilename = _target.prefsFilename.Replace("\\", "/");
        }

		// Display the default inspector at the end + other debug options
		_target.DEBUG = EditorGUILayout.Toggle("DEBUG", _target.DEBUG);
		EditorGUILayout.Space();

		// Inspector Body - Start --------------------------------------------------------------------------------------
		_target.m_target = (GameObject) EditorGUILayout.ObjectField("Target GameObject", _target.m_target, typeof(GameObject), true);
		_target.m_minSpeed = EditorGUILayout.Slider("Min Speed", _target.m_minSpeed, 0f, _target.m_maxSpeed);
		_target.m_maxSpeed = EditorGUILayout.Slider("Max Speed", _target.m_maxSpeed, _target.m_minSpeed, 1000f);
		_target.m_speedProperty = EditorGUILayout.Slider("Speed", _target.m_speedProperty, _target.m_minSpeed, _target.m_maxSpeed);

		DrawProperty("_currentDateTime");

		DrawProperty("cmdLineArgs");
		DrawProperty("programArgs");

		if (GUILayout.Button("Process Args"))
		{
			_target.programArgs = new ProgramArgs(_target.cmdLineArgs);
		}

        DrawProperty("prefsFilename");
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Save"))
        {
            Debug.LogFormat("Saving to: {0}", _target.prefsFilename);
            XML.SaveFile(_target._currentDateTime, _target.prefsFilename);
        }
        if (GUILayout.Button("Load"))
        {
            Debug.LogFormat("Loading from: {0}", _target.prefsFilename);
            XML.LoadFile(ref _target._currentDateTime, _target.prefsFilename);
        }
        GUILayout.EndHorizontal();

        // Draw the list of Items
        DrawList(_target.m_items);

        if (GUILayout.Button("Set Random Dates"))
        {
            _target.RandomizeItems();
        }

        // Inspector Body - End ----------------------------------------------------------------------------------------

        // Debug -------------------------------------------------------------------------------------------------------
        if (_target.DEBUG)
		{
			EditorGUILayout.Space();
			EditorGUILayout.Space();
            EditorGUILayout.LabelField("____ Default Inspector ______________");
            //NVYVE.InspectorLabelProperty_Drawer.DrawSeparator("Default Inspector", Color.white);
            DrawDefaultInspector();

			EditorGUILayout.HelpBox("Note: DefaultInspector is drawing, it might block changes to the custom InspectorGUI.", MessageType.Warning);
		}
		EditorGUILayout.Space();

		// GUI.Changed -------------------------------------------------------------------------------------------------
		if (GUI.changed)
		{
			serializedObject.ApplyModifiedProperties();
			EditorUtility.SetDirty(_target);
		}
	} // OnInspectorGUI()


	/// <summary>
	/// Draws the serialized property as if it was rendered in the default inspector (i.e. handles array/lists as a 
	/// list, serialized XML as a tree, etc.)
	/// </summary>
	/// <param name="property_name">Name of the property (use "parent/child" for sub-properties).</param>
	public void DrawProperty(string property_name)
	{
		SerializedProperty property = serializedObject.FindProperty( property_name );
		if (property != null)
		{
			EditorGUI.BeginChangeCheck();

			// Draw the property, including children
			EditorGUILayout.PropertyField(property, true);

			if (EditorGUI.EndChangeCheck())
			{
				serializedObject.ApplyModifiedProperties();
			}
		}
	} // DrawPropertyInspector()
	#endregion

	#region /- List ----------------------------------------------------------------------------------------------------
	/// <summary>
	/// Draws the list of Items
	/// </summary>
	public void DrawList(List<_TemplateItem> items)
	{
		// List Header
		SerializedProperty prop = serializedObject.FindProperty("m_items");
		if (prop == null) return;
		Rect rect = GUILayoutUtility.GetRect(EditorGUIUtility.fieldWidth, 18, GUILayout.ExpandWidth(false));
		prop.isExpanded = EditorGUI.Foldout(rect, prop.isExpanded, ObjectNames.NicifyVariableName(prop.name) + " ("+items.Count+")");

		DrawListButtons(rect, items, prop);

		if (prop.isExpanded)
		{
			EditorGUI.indentLevel++;
			//int size = EditorGUILayout.IntField("Size", items.Count);
			for (int i=0; i<items.Count; i++)
			{
				if ((items[i] != null) && (prop.arraySize == items.Count))
				{
					SerializedProperty propitem = prop.GetArrayElementAtIndex(i);
					DrawItem(items[i], propitem);
				}
			} // loop items

			EditorGUI.indentLevel--;
		} // // isExpanded

	} // DrawList()

	/// <summary>
	///
	/// </summary>
	/// <param name="items"></param>
	/// <param name="rect"></param>
	/// <param name="prop"></param>
	void DrawListButtons(Rect rect, List<_TemplateItem> items, SerializedProperty prop)
	{
		// Add Item Button
		rect.width = miniButtonWidth * 2;
		rect.x = Screen.width - rect.width - 20;
		if (GUI.Button(rect, addButtonContent, miniButtonStyle))
		{
            _TemplateItem newItem = new _TemplateItem();
			newItem.name = "New Item " + _target.m_items.Count;
			_target.AddItem(newItem);
		}

		rect.width = miniButtonWidth;
		rect.x -= rect.width + 10;
		if (GUI.Button(rect, expandAllButtonContent, miniButtonRightStyle))
		{
			for (int i = 0; i < items.Count; i++)
			{
				SerializedProperty propitem = prop.GetArrayElementAtIndex(i);
				propitem.isExpanded = true;
			} // loop items
		}
		rect.x -= rect.width;
		if (GUI.Button(rect, collapseAllButtonContent, miniButtonLeftStyle))
		{
			for (int i = 0; i < items.Count; i++)
			{
				SerializedProperty propitem = prop.GetArrayElementAtIndex(i);
				propitem.isExpanded = false;
			} // loop items
		}
		EditorGUILayout.Space();
	} // DrawListButtons()

	/// <summary>
	/// Draws an individual Item.
	/// </summary>
	/// <param name="item"></param>
	/// <param name="prop"></param>
	public void DrawItem(_TemplateItem item, SerializedProperty prop=null)
	{
		if (prop == null) return;

		Rect rect = GUILayoutUtility.GetRect(EditorGUIUtility.fieldWidth, 18, GUILayout.ExpandWidth(false));
		prop.isExpanded = EditorGUI.Foldout(rect, prop.isExpanded, item.name);
		DrawItemButtons(rect, item); // Draw the list manipulation buttons for this item
		if (prop.isExpanded)
		{
			EditorGUI.indentLevel++;

			// Item Properties
			bool childrenAreExpanded = true;
			int propertyStartingDepth = prop.depth;
			while (prop.NextVisible(childrenAreExpanded) && (propertyStartingDepth < prop.depth))
			{
				childrenAreExpanded = EditorGUILayout.PropertyField(prop, true);
			}

			EditorGUI.indentLevel--;
		} // isExpanded

		EditorGUILayout.Space();
	} // DrawItem()


	/// <summary>
	/// Draws the list management button for this Item.
	/// </summary>
	/// <param name="rect"></param>
	/// <param name="item"></param>
	public void DrawItemButtons(Rect rect, _TemplateItem item)
	{
		// Set the default width of the buttons
		rect.width = miniButtonWidth;

		// Set the initial X position, right side of window - button width - padding
		// Delete Button
		rect.x = Screen.width - rect.width - 20;
		if (GUI.Button(rect, deleteButtonContent, miniButtonRightStyle))
		{
			_target.DeleteItem(item);
		}

		// Duplicate Button
		rect.x -= rect.width;
		if (GUI.Button(rect, duplicateButtonContent, miniButtonMidStyle))
		{
			_target.DuplicateItem(item);
		}

		// Move Down button
		rect.x -= rect.width;
		if (GUI.Button(rect, moveDownButtonContent, miniButtonMidStyle))
		{
			_target.MoveItemDown(item);
		}

		// Move Up Button
		rect.x -= rect.width;
		if (GUI.Button(rect, moveUpButtonContent, miniButtonLeftStyle))
		{
			_target.MoveItemUp(item);
		}

		// Play Button
		rect.width = miniButtonWidth * 2f;
		rect.x -= rect.width + 10;
		if (GUI.Button(rect, playButtonContent, miniButtonStyle))
		{
			_target.SetItem(item);
		}

	} // DrawItemButtons()
	#endregion

} // _TemplateScript_Editor()
