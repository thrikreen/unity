﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SceneHandler
{
    public class SceneHandler : UnitySingleton<SceneHandler>
    {
        #region /- Variables -------------------------------------------------------------------------------------------
        private bool _debug                                 = false;
        public static new bool DEBUG // Use this on an UnitySingleton, so the DEBUG modifies the Instance's
        {
            get { return (Instance != null ? Instance._debug : false); }
            set { if (Instance != null) Instance._debug = value; }
        }
        
        public List<SceneItem> m_items                  = new List<SceneItem>();
        #endregion
        
        #region /- Help ------------------------------------------------------------------------------------------------
        [ContextMenu("Help")]
        void Context_Help() { HelpSystem.OpenURL(this.GetType().Name); }
        #endregion
        
        #region /- Initialize ------------------------------------------------------------------------------------------
        /// <summary>
        /// Prevent any scripts from creating this singleton class via code.
        /// </summary>
        protected SceneHandler() { }
        #endregion

        #region /- Initialize ------------------------------------------------------------------------------------------
        public override void Awake()
        {
            base.Awake();
        }

        void Start()
        {
            //StartCoroutine(DelayLoadScenes());
        }
        #endregion

        #region /- Scene Loading ---------------------------------------------------------------------------------------
        IEnumerator DelayLoadScenes()
        {
            /*
            for (int i = 0; i < sceneNames.Count; i++)
            {
                // Delay
                Debug.LogFormat("[ SCENE ] Loading '{0}' in {1} second(s)...", sceneNames[i], sceneLoadDelay);
                yield return new WaitForSeconds(sceneLoadDelay);

                // Load the scene
                UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(sceneNames[i], LoadSceneMode.Additive);
            } // loop scenes to load
            */

            yield return new WaitForSeconds(1.0f);
        } // DelayLoadScenes()
        #endregion



        #region /- List Functions ------------------------------------------------------------------------------------------
        /// <summary>
        /// Sets the active item to the supplied Item, and does whatever processing it needs to do.
        /// </summary>
        /// <param name="item">Item to activate.</param>
        public void SetItem(SceneItem item)
        {
            string debuginfo = "";

            if (item == null) // No item
            {
            }
            else // Process item here
            {
            }

            if (DEBUG) Debug.LogFormat(gameObject, "[ {0} ] {1}( {2} ):\n{3}", GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, item.name, debuginfo);
        } // Play()

        /// <summary>
        /// Adds the item to the list.
        /// </summary>
        /// <param name="item">Item to add.</param>
        public void AddItem(SceneItem item)
        {
            m_items.Add(item);
        } // AddItem()

        /// <summary>
        /// Gets the item by name, if found.
        /// </summary>
        /// <param name="name">Name of the item to find.</param>
        /// <returns>The item if found, otherwise null.</returns>
        public SceneItem GetItem(string name)
        {
            for (int i = 0; i < m_items.Count; i++)
            {
                if (m_items[i].name.ToLower() == name.ToLower()) return (m_items[i]);
            }
            return (null);
        } // GetItem()

        /// <summary>
        /// Gets the index of the item, if found. Searches by item name and returns the first instance encountered.
        /// </summary>
        /// <param name="name">Name of the item to get the index of. </param>
        /// <returns>The index of the found item.</returns>
        public int GetItemIndex(string name)
        {
            return (GetItemIndex(GetItem(name)));
        } // GetItemIndex()

        /// <summary>
        /// Gets the index of the item, if found.
        /// </summary>
        /// <param name="item">The item to get the index of in the main list.</param>
        /// <returns>The index of the found item.</returns>
        public int GetItemIndex(SceneItem item)
        {
            for (int i = 0; i < m_items.Count; i++)
            {
                if (m_items[i] == item) return (i);
            }
            return (-1);
        } // GetItemIndex()

        /// <summary>
        /// Deletes the specified Item from the main list.
        /// </summary>
        /// <param name="item">Item to delete.</param>
        public void DeleteItem(SceneItem item)
        {
            if (item == null) return;
            m_items.Remove(item);
        } // DeleteItem()

        /// <summary>
        /// Duplicates the item.
        /// </summary>
        /// <param name="item">Item to duplicate.</param>
        public void DuplicateItem(SceneItem item)
        {
            if (item == null) return;
            SceneItem newItem = new SceneItem(item);
            newItem.name += " Copy";
            m_items.Add(newItem);
        } // DuplicateItem()

        /// <summary>
        /// Moves the specified item one down in the list.
        /// </summary>
        /// <param name="item">Item to move.</param>
        public void MoveItemDown(SceneItem item)
        {
            if (item == null) return;
            int index = GetItemIndex(item);
            if (GetItemIndex(item) != (m_items.Count - 1))
            {
                m_items.Remove(item);
                m_items.Insert(index + 1, item);
            }
        } // MoveItemDown()

        /// <summary>
        /// Moves the specified item one up in the list.
        /// </summary>
        /// <param name="item">Item to move.</param>
        public void MoveItemUp(SceneItem item)
        {
            if (item == null) return;
            int index = GetItemIndex(item);
            if (index > 0)
            {
                m_items.Remove(item);
                m_items.Insert(index - 1, item);
            }
        } // MoveItemUp()


        /// <summary>
        /// Sort the items.
        /// </summary>
        public void SortItems()
        {
            m_items.Sort(delegate (SceneItem a, SceneItem b)
            {
                int result = a.name.CompareTo(b.name);
                // Same name, compare by other criteria
                //if (result == 0)  result = a.OTHERPROPERTY.CompareTo(b.OTHERPROPERTY);
                return (result);
            });
        } // SortItems()
        #endregion

    } // SceneManager()

} // SceneManager