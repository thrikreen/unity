﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;

namespace SceneHandler
{
    [Serializable, XmlRoot("SceneItem")]
    public class SceneItem 
    {
  
        #region /- Variables -----------------------------------------------------------------------------------------------
        [XmlElement] public string name             = "";
        [XmlElement] public string path             = "";

        [XmlElement] public LoadSceneMode mode      = LoadSceneMode.Single;
        [XmlElement] public float loadDelay         = 0.0f;

        [XmlElement] public List<int> alsoLoad      = new List<int>();

        // Flags
        [XmlElement] public bool DEBUG              = false;
        [XmlElement] public bool selected           = false;
        [XmlElement] public bool active             = false;
        [XmlElement] public bool locked             = false;

        [DateTimeItem]
        [XmlElement]
        public DateTimeItem loadStarted             = new DateTimeItem();

        [DateTimeItem]
        [XmlElement]
        public DateTimeItem loadFinished            = new DateTimeItem();

        [DateTimeItem]
        [XmlElement]
        public DateTimeItem lastModified            = new DateTimeItem();
        #endregion

        #region /- Constructor ---------------------------------------------------------------------------------------------
        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        public SceneItem()
        {
        } // _TemplateItem()

        //
        public SceneItem(string name, string path="")
        {
            this.name = name;
            this.path = path;
        } // _TemplateItem()

        /// <summary>
        /// Creates a duplicate of the provided _TemplateItem source.
        /// </summary>
        /// <param name="source"></param>
        public SceneItem(SceneItem source)
        {
            string debuginfo = "";
            debuginfo += string.Format("- Source: {0}\n", source.name);

            // Clone via JSON, make a new object and source from that
            SceneItem newSource = new SceneItem();
            newSource = (SceneItem)JsonUtility.FromJson(JsonUtility.ToJson(source), typeof(SceneItem));

            // Parse every field on this class and duplicate the values over
            FieldInfo[] fields = this.GetType().GetFields(BindingFlags.Public|BindingFlags.NonPublic|BindingFlags.Instance);
            foreach (FieldInfo field in fields)
            {
                // Note: object references will copy the pointer, not make a new object. Need to deep copy it
                field.SetValue(this, field.GetValue(newSource));
                debuginfo += string.Format("- Field: {0} = {1}, {2}\n", field.Name, field.GetValue(newSource), field.GetType().IsByRef);
            } // loop fields

        } // SceneItem()
        #endregion
        
        #region /- Selection -------------------------------------------------------------------------------------------
        public virtual void SetSelection(bool new_selection_state)
        {
            selected = new_selection_state;
        } // SetSelection()
        #endregion

    } // SceneItem()
}