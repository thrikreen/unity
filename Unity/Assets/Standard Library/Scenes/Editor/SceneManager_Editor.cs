﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditorInternal;

namespace SceneHandler
{
    [CustomEditor(typeof(SceneHandler))]
    public class SceneHandler_Editor : Editor
    {
        #region /- Variables -------------------------------------------------------------------------------------------
        const string CLASSNAME                              = "SceneHandler";
        public static SceneHandler _target;
        public static MonoScript _target_script;
        public static MonoScript _editor_script;

        #region /- GUI Styles ----------------------------------------------------------------------------------------------
        private bool _initialized                           = false;
        private static float miniButtonWidth                = 30f;
        private static GUIStyle miniButtonStyle;
        private static GUIStyle miniButtonLeftStyle;
        private static GUIStyle miniButtonMidStyle;
        private static GUIStyle miniButtonRightStyle;
        private static GUIContent moveDownButtonContent;
        private static GUIContent moveUpButtonContent;
        private static GUIContent duplicateButtonContent;
        private static GUIContent addButtonContent;
        private static GUIContent deleteButtonContent;
        private static GUIContent expandAllButtonContent;
        private static GUIContent collapseAllButtonContent;
        private static GUIContent playButtonContent;
        #endregion

        private ReorderableList list;
        #endregion

        #region /- Help Menu -------------------------------------------------------------------------------------------
        [MenuItem("CONTEXT/" + CLASSNAME + "/Edit " + CLASSNAME + "_Editor Script")]
        static void Menu_EditScript(MenuCommand command) { AssetDatabase.OpenAsset(_editor_script); }
        [MenuItem("CONTEXT/" + CLASSNAME + "/Find " + CLASSNAME + " Script")]
        static void Menu_PingScript(MenuCommand command) { EditorGUIUtility.PingObject(_target_script); }
        [MenuItem("CONTEXT/" + CLASSNAME + "/Find " + CLASSNAME + " Editor Script")]
        static void Menu_PingEditorScript(MenuCommand command) { EditorGUIUtility.PingObject(_editor_script); }
        #endregion

        #region /- Initialization --------------------------------------------------------------------------------------
        /// <summary>
        /// Runs whenever this Inspector panel is shown. Note: Will run multiple times if other Inspector panels are also opened.
        /// </summary>
        void OnEnable()
        {
            _target = (SceneHandler)target;
            _target_script = MonoScript.FromMonoBehaviour(_target);
            _editor_script = MonoScript.FromScriptableObject(this);
            
            _initialized = false;
        } // OnEnable()
        #endregion

        #region /- OnInspectorGUI ------------------------------------------------------------------------------------------
        /// <summary>
        /// Initializes the GUI, as some property types (GUIStyles) can't be assigned on declaration, only in OnGUI() calls.
        /// </summary>
        void InitializeGUI()
        {
            // Button GUIStyles for the item listing - can't run GUIStyles outside of an OnGUI call
            miniButtonStyle = new GUIStyle(EditorStyles.miniButton);
            miniButtonLeftStyle = new GUIStyle(EditorStyles.miniButtonLeft);
            miniButtonMidStyle = new GUIStyle(EditorStyles.miniButtonMid);
            miniButtonRightStyle = new GUIStyle(EditorStyles.miniButtonRight);
            moveDownButtonContent = new GUIContent("\u25bC", "Move Item Down");
            moveUpButtonContent = new GUIContent("\u25b2", "Move Item Up");
            duplicateButtonContent = new GUIContent((Texture2D)EditorGUIUtility.Load("GUI/icons/icon_copy.png"), "Duplicate Item");
            addButtonContent = new GUIContent("+", "Add Item");
            deleteButtonContent = new GUIContent("-", "Delete Item");
            expandAllButtonContent = new GUIContent("\u25bC", "Expand All Items");
            collapseAllButtonContent = new GUIContent("\u25b2", "Collapse All Items");
            playButtonContent = new GUIContent(EditorGUIUtility.FindTexture("d_PlayButton"), "Set Item");

            // Other GUI Initializations here

            // Reorderable List
            list = new ReorderableList(serializedObject, serializedObject.FindProperty("m_items"), true, true, true, true);
            list.drawElementCallback += DrawElementCallback;
            list.elementHeightCallback += ElementHeightCallback;

            _initialized = true;
        } // InitializeGUI()

        /// <summary>
        /// InspectorGUI
        /// </summary>
        public override void OnInspectorGUI()
        {
            // Update the object's properties after setting them the previous frame.
            serializedObject.UpdateIfRequiredOrScript();

            // Checks ------------------------------------------------------------------------------------------------------
            if (!_initialized) InitializeGUI(); // init the GUI

            // Display the default inspector at the end + other debug options
            SceneHandler.DEBUG = EditorGUILayout.Toggle("DEBUG", SceneHandler.DEBUG);
            EditorGUILayout.Space();

            // Inspector Body - Start --------------------------------------------------------------------------------------
            serializedObject.Update();
            list.DoLayoutList();

            if (GUILayout.Button("Populate Scene List"))
            {
                PopulateSceneList();
            }

            // Draw the list of Items
            DrawList(_target.m_items);

            // Inspector Body - End ----------------------------------------------------------------------------------------


            // Debug -------------------------------------------------------------------------------------------------------
            if (SceneHandler.DEBUG)
            {
                EditorGUILayout.Space();
                EditorGUILayout.Space();
                EditorGUILayout.LabelField("____ Default Inspector ______________");
                DrawDefaultInspector();

                EditorGUILayout.HelpBox("Note: DefaultInspector is drawing, it might block changes to the custom InspectorGUI.", MessageType.Warning);
            }
            EditorGUILayout.Space();

            // GUI.Changed -------------------------------------------------------------------------------------------------
            if (GUI.changed)
            {
                serializedObject.ApplyModifiedProperties();
                EditorUtility.SetDirty(_target);
            }
        } // OnInspectorGUI()
        
        /// <summary>
        /// Draws the serialized property as if it was rendered in the default inspector (i.e. handles array/lists as a 
        /// list, serialized XML as a tree, etc.)
        /// </summary>
        /// <param name="property_name">Name of the property (use "parent/child" for sub-properties).</param>
        public void DrawProperty(string property_name)
        {
            SerializedProperty property = serializedObject.FindProperty( property_name );
            if (property != null)
            {
                EditorGUI.BeginChangeCheck();

                // Draw the property, including children
                EditorGUILayout.PropertyField(property, true);

                if (EditorGUI.EndChangeCheck())
                {
                    serializedObject.ApplyModifiedProperties();
                }
            }
        } // DrawProperty()


        /// <summary>
        /// Draws the serialized property as if it was rendered in the default inspector (i.e. handles array/lists as a 
        /// list, serialized XML as a tree, etc.)
        /// </summary>
        /// <param name="property_name">Name of the property (use "parent/child" for sub-properties).</param>
        public void DrawProperty(Rect rect, string property_name)
        {
            SerializedProperty property = serializedObject.FindProperty( property_name );
            if (property != null)
            {
                EditorGUI.BeginChangeCheck();

                // Draw the property, including children
                EditorGUI.PropertyField(rect, property, true);

                if (EditorGUI.EndChangeCheck())
                {
                    serializedObject.ApplyModifiedProperties();
                }
            }
        } // DrawProperty()


        #endregion

        #region /- Scene Management ------------------------------------------------------------------------------------
        void PopulateSceneList()
        {
            string debugOutput = "";

            debugOutput += string.Format("Found {0} scenes listed:\n", SceneManager.sceneCountInBuildSettings);
            for (int i=0; i<SceneManager.sceneCountInBuildSettings; i++)
            {
                string scenePath = SceneUtility.GetScenePathByBuildIndex(i);
                string sceneName = Path.GetFileNameWithoutExtension(scenePath);
                debugOutput += string.Format("- {0}: {1}", i, sceneName);

                SceneItem item = GetSceneItemByName(sceneName);
                if (item == null)
                {
                    debugOutput += string.Format(" + ADDED");
                    SceneItem newItem = new SceneItem(sceneName, scenePath);
                    scenePath = scenePath.Substring(scenePath.IndexOf("/")+1); // remove first "Asset/" portion as Application.dataPath will also have it.
                    string absPath = Path.Combine(Application.dataPath, scenePath);
                    newItem.lastModified = new DateTimeItem(File.GetLastWriteTime(absPath));
                    _target.m_items.Add(newItem);
                }
                else // Update
                {
                    debugOutput += string.Format(" + UPDATED");
                    scenePath = scenePath.Substring(scenePath.IndexOf("/") + 1); // remove first "Asset/" portion as Application.dataPath will also have it.
                    string absPath = Path.Combine(Application.dataPath, scenePath);
                    item.lastModified = new DateTimeItem(File.GetLastWriteTime(absPath));
                }
                
                debugOutput += "\n";
            } // Loop Scenes

            EditorUtility.SetDirty(_target.gameObject);

            Debug.LogFormat(debugOutput);
        } // PopulateSceneList()

        SceneItem GetSceneItemByName(string name)
        {
            for (int i=0; i<_target.m_items.Count; i++)
            {
                if (_target.m_items[i].name == name) return (_target.m_items[i]);
            }
            return (null);
        } // GetSceneItemByName()
        #endregion

        #region /- Reorderable List ------------------------------------------------------------------------------------
        float ElementHeightCallback(int index)
        {
            float height = EditorGUIUtility.singleLineHeight;

            var element = list.serializedProperty.GetArrayElementAtIndex(index);
            if (element.isExpanded)
            {
                height = (EditorGUIUtility.singleLineHeight + 2) * 11f;

                SerializedProperty loadStarted = element.FindPropertyRelative("loadStarted");
                if (loadStarted.isExpanded) height += (EditorGUIUtility.singleLineHeight + 2) * 7f;

                SerializedProperty loadFinished = element.FindPropertyRelative("loadFinished");
                if (loadFinished.isExpanded) height += (EditorGUIUtility.singleLineHeight + 2) * 7f;

                SerializedProperty lastModified = element.FindPropertyRelative("lastModified");
                if (lastModified.isExpanded) height += (EditorGUIUtility.singleLineHeight + 2) * 7f;
            }

            return (height);
        } // ElementHeightCallback()

        void DrawElementCallback(Rect rect, int index, bool isActive, bool isFocused)
        {
            var element = list.serializedProperty.GetArrayElementAtIndex(index);
            rect.x += 10;
            rect.y += 2;
            rect.width -= 20;

            EditorGUI.PropertyField( new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("name"), GUIContent.none);

            element.isExpanded = EditorGUI.Foldout(new Rect(rect.x, rect.y, 20, EditorGUIUtility.singleLineHeight), element.isExpanded, "");
            if (element.isExpanded)
            {
                rect.y += EditorGUIUtility.singleLineHeight + 2;
                EditorGUI.TextField(new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight), "Path", _target.m_items[index].path);

                rect.y += EditorGUIUtility.singleLineHeight + 2;
                _target.m_items[index].mode = (LoadSceneMode)EditorGUI.EnumPopup(new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight), "Scene Load Mode", _target.m_items[index].mode);

                rect.y += EditorGUIUtility.singleLineHeight + 2;
                _target.m_items[index].loadDelay = EditorGUI.FloatField(new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight), "Load Delay", _target.m_items[index].loadDelay);

                rect.y += EditorGUIUtility.singleLineHeight + 2;
                _target.m_items[index].DEBUG = EditorGUI.Toggle(new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight), "DEBUG", _target.m_items[index].DEBUG);
                rect.y += EditorGUIUtility.singleLineHeight + 2;
                _target.m_items[index].selected = EditorGUI.Toggle(new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight), "Selected", _target.m_items[index].selected);
                rect.y += EditorGUIUtility.singleLineHeight + 2;
                _target.m_items[index].active = EditorGUI.Toggle(new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight), "Active", _target.m_items[index].active);
                rect.y += EditorGUIUtility.singleLineHeight + 2;
                _target.m_items[index].locked = EditorGUI.Toggle(new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight), "Locked", _target.m_items[index].locked);

                // DateTimeItems
                SerializedProperty loadStarted = element.FindPropertyRelative("loadStarted");
                SerializedProperty loadFinished = element.FindPropertyRelative("loadFinished");
                SerializedProperty lastModified = element.FindPropertyRelative("lastModified");

                rect.y += EditorGUIUtility.singleLineHeight + 2;
                EditorGUI.PropertyField(new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight), loadStarted);
                if (loadStarted.isExpanded) rect.y += (EditorGUIUtility.singleLineHeight + 2) * 7f;

                rect.y += EditorGUIUtility.singleLineHeight + 2;
                EditorGUI.PropertyField(new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight), loadFinished);
                if (loadFinished.isExpanded) rect.y += (EditorGUIUtility.singleLineHeight + 2) * 7f;

                rect.y += EditorGUIUtility.singleLineHeight + 2;
                EditorGUI.PropertyField(new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight), lastModified);
                if (lastModified.isExpanded) rect.y += (EditorGUIUtility.singleLineHeight + 2) * 7f;

            }
            else
            {

            }


        } // DrawElementCallback()


        #endregion

        #region /- List ------------------------------------------------------------------------------------------------
        /// <summary>
        /// Draws the list of Items
        /// </summary>
        public void DrawList(List<SceneItem> items)
        {
            // List Header
            SerializedProperty prop = serializedObject.FindProperty("m_items");
            if (prop == null) return;
            Rect rect = GUILayoutUtility.GetRect(EditorGUIUtility.fieldWidth, 18, GUILayout.ExpandWidth(false));
            prop.isExpanded = EditorGUI.Foldout(rect, prop.isExpanded, ObjectNames.NicifyVariableName(prop.name) + " (" + items.Count + ")");

            DrawListButtons(rect, items, prop);

            if (prop.isExpanded)
            {
                EditorGUI.indentLevel++;
                //int size = EditorGUILayout.IntField("Size", items.Count);
                for (int i = 0; i < items.Count; i++)
                {
                    if ((items[i] != null) && (prop.arraySize == items.Count))
                    {
                        SerializedProperty propitem = prop.GetArrayElementAtIndex(i);
                        DrawItem(items[i], propitem);
                    }
                } // loop items

                EditorGUI.indentLevel--;
            } // // isExpanded

        } // DrawList()

        /// <summary>
        ///
        /// </summary>
        /// <param name="items"></param>
        /// <param name="rect"></param>
        /// <param name="prop"></param>
        void DrawListButtons(Rect rect, List<SceneItem> items, SerializedProperty prop)
        {
            // Add Item Button
            rect.width = miniButtonWidth * 2;
            rect.x = Screen.width - rect.width - 20;
            if (GUI.Button(rect, addButtonContent, miniButtonStyle))
            {
                SceneItem newItem = new SceneItem();
                newItem.name = "New Item " + _target.m_items.Count;
                _target.AddItem(newItem);
            }

            rect.width = miniButtonWidth;
            rect.x -= rect.width + 10;
            if (GUI.Button(rect, expandAllButtonContent, miniButtonRightStyle))
            {
                for (int i = 0; i < items.Count; i++)
                {
                    SerializedProperty propitem = prop.GetArrayElementAtIndex(i);
                    propitem.isExpanded = true;
                } // loop items
            }
            rect.x -= rect.width;
            if (GUI.Button(rect, collapseAllButtonContent, miniButtonLeftStyle))
            {
                for (int i = 0; i < items.Count; i++)
                {
                    SerializedProperty propitem = prop.GetArrayElementAtIndex(i);
                    propitem.isExpanded = false;
                } // loop items
            }
            EditorGUILayout.Space();
        } // DrawListButtons()

        /// <summary>
        /// Draws an individual Item.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="prop"></param>
        public void DrawItem(SceneItem item, SerializedProperty prop = null)
        {
            if (prop == null) return;

            Rect rect = GUILayoutUtility.GetRect(EditorGUIUtility.fieldWidth, 18, GUILayout.ExpandWidth(false));
            prop.isExpanded = EditorGUI.Foldout(rect, prop.isExpanded, item.name);
            DrawItemButtons(rect, item); // Draw the list manipulation buttons for this item
            if (prop.isExpanded)
            {
                EditorGUI.indentLevel++;

                // Item Properties
                bool childrenAreExpanded = true;
                int propertyStartingDepth = prop.depth;
                while (prop.NextVisible(childrenAreExpanded) && (propertyStartingDepth < prop.depth))
                {
                    childrenAreExpanded = EditorGUILayout.PropertyField(prop, true);
                }

                EditorGUI.indentLevel--;
            } // isExpanded

            EditorGUILayout.Space();
        } // DrawItem()


        /// <summary>
        /// Draws the list management button for this Item.
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="item"></param>
        public void DrawItemButtons(Rect rect, SceneItem item)
        {
            // Set the default width of the buttons
            rect.width = miniButtonWidth;

            // Set the initial X position, right side of window - button width - padding
            // Delete Button
            rect.x = Screen.width - rect.width - 20;
            if (GUI.Button(rect, deleteButtonContent, miniButtonRightStyle))
            {
                _target.DeleteItem(item);
            }

            // Duplicate Button
            rect.x -= rect.width;
            if (GUI.Button(rect, duplicateButtonContent, miniButtonMidStyle))
            {
                _target.DuplicateItem(item);
            }

            // Move Down button
            rect.x -= rect.width;
            if (GUI.Button(rect, moveDownButtonContent, miniButtonMidStyle))
            {
                _target.MoveItemDown(item);
            }

            // Move Up Button
            rect.x -= rect.width;
            if (GUI.Button(rect, moveUpButtonContent, miniButtonLeftStyle))
            {
                _target.MoveItemUp(item);
            }

            // Play Button
            rect.width = miniButtonWidth * 2f;
            rect.x -= rect.width + 10;
            if (GUI.Button(rect, playButtonContent, miniButtonStyle))
            {
                _target.SetItem(item);
            }

        } // DrawItemButtons()
        #endregion

    } // SceneManager_Editor()
}