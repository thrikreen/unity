﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

public class XML
{
    #region /- Save ----------------------------------------------------------------------------------------------------
    /// <summary>
    /// Saves the object's properties (if supported) into an XML format and returns it as a string.
    /// </summary>
    /// <param name="obj">The target object.</param>
    /// <param name="content">Returns the XML content</param>
    /// <returns>True if the conversion was successful, otherwise false.</returns>
    public static bool Save<T>(T obj, out string content)
    {
        content = "";

        try
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder(); // Redirect to a string builder

            // Convert the object to XML
            StringWriter stringWriter = new StringWriter(sb);
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            serializer.Serialize(stringWriter, obj);
            stringWriter.Close();

            // Convert to a string to be returned.
            content = sb.ToString();
            
            return(true);
        }
        catch (InvalidCastException e)
        {
            Debug.LogError("ERROR: " + e);
            return(false);
        }
    } // Save()

    /// <summary>
    /// Saves the object's properties (if supported) into an XML format and writes to the target file.
    /// </summary>
    /// <param name="obj">The target object.</param>
    /// <param name="filename"></param>
    /// <returns>True if the conversion and file was successfully written to, otherwise false.</returns>
    public static bool SaveFile<T>(T obj, string filename)
    {
        // Check if there is a valid filename
        if (string.IsNullOrEmpty(filename))
        {
            Debug.LogError("ERROR: Target file name is empty.");
            return(false);
        }
        // Check if target path exists
        string target_dir = Path.GetDirectoryName(filename);
        if (!Directory.Exists(target_dir)) // Does not exist, make the directory
        {
            try
            {
                Directory.CreateDirectory(target_dir);
            }
            catch (IOException e)
            {
                Debug.LogError("ERROR: " + e);
                return (false);
            }
        }

        try
        {
            string content = "";
            if (XML.Save(obj, out content))
            {
                File.WriteAllText(filename, content);
                return (true);
            }
            else
            {
                return (false);
            }
        }
        catch (InvalidCastException e)
        {
            Debug.LogError("ERROR: " + e);
            return (false);
        }
    } // SaveFile()
    #endregion

    #region /- Load ----------------------------------------------------------------------------------------------------
    /// <summary>
    /// Loads the object from the provided XML content.
    /// </summary>
    /// <param name="obj">Object to load the XML properties to.</param>
    /// <param name="content">XML text content to load from.</param>
    /// <returns>True if the load was successful, otherwise false.</returns>
    public static bool Load<T>(ref T obj, string content)
    {
        try
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            StringReader stringReader = new StringReader(content);
            obj = (T)serializer.Deserialize(stringReader);
            stringReader.Close();
            return (true);
        }
        catch (InvalidCastException e)
        {
            Debug.LogError("ERROR: " + e);
            return (false);
        }
    } // Load()

    public static bool LoadFile<T>(ref T obj, string filename)
    {
        // Check if we have a target file to read from
        if (!System.IO.File.Exists(filename))
        {
            Debug.LogError("ERROR: File not found at: " + filename);
            return (false);
        }
        string content = File.ReadAllText(filename);
        return (XML.Load(ref obj, content));
    } // LoadFile()
    #endregion
} // XML()
