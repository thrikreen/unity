﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;

public static class Rect_Extensions  
{
	#region /- DrawRect2d ----------------------------------------------------------------------------------------------
	/// <summary>
	/// Draws a Rect in the current window, 2D mode.
	/// </summary>
	/// <param name="source">The rect to display.</param>
	/// <param name="line_colour">Color of the rect lines (defaults to white).</param>
	public static void DrawRect(this Rect source, Color line_colour=default(Color))
	{
		#if UNITY_EDITOR
		Vector3 p1 = new Vector3(source.x, source.y, 0);
		Vector3 p2 = new Vector3(source.x + source.width, source.y, 0);
		Vector3 p3 = new Vector3(source.x + source.width, source.y + source.height, 0);
		Vector3 p4 = new Vector3(source.x, source.y + source.height, 0);

		Handles.color = line_colour;
		Handles.DrawLine(p1, p2);
		Handles.DrawLine(p2, p3);
		Handles.DrawLine(p3, p4);
		Handles.DrawLine(p4, p1);
		#endif
	} // DrawRect()

    /// <summary>
    /// Check are the tooltip is overlapping the subject and render them in Red or not
    /// </summary>
    /// <param name="rect1">Rect1.</param>
    /// <param name="rect2">Rect2.</param>
    /// <param name="line_duration">Line_duration.</param>
    public static void DrawRectOverlap(this Rect rect1, Rect rect2, float line_duration = 0.0f)
    {
        Color rect_color = Color.green;

        if (rect1.Overlaps(rect2)) rect_color = Color.red;

        // Draw the two rects
        rect1.DrawRect3d(rect_color, line_duration);
        rect2.DrawRect3d(rect_color, line_duration);
    } // DrawRectOverlap()
    #endregion

    #region /- DrawRect3d ----------------------------------------------------------------------------------------------
    /// <summary>
    /// Draws the rect in the 3D scene.
    /// </summary>
    /// <param name="source">The rect to display.</param>
    /// <param name="wire_color">Color of the rect lines (defaults to white).</param>
    /// <param name="line_duration">Duration in seconds for how long to display the rect lines.</param>
    /// <param name="height">Normally draws the rect top-down on the X/Z axis, but if this is specified, it will have elevation.</param>
    public static void DrawRect3d(this Rect source, Color wire_color=default(Color), float line_duration=0.0f, float height=0.0f)
	{
		Vector3 upper_left 	= new Vector3(source.x, source.y, height);
		Vector3 lower_left  = new Vector3(source.x, source.y - source.height, height);
		Vector3 upper_right = new Vector3(source.x + source.width, source.y, height);
		Vector3 lower_right = new Vector3(source.x + source.width, source.y - source.height, height);
		
		Debug.DrawLine(lower_left, lower_right, wire_color, line_duration);
		Debug.DrawLine(lower_right, upper_right, wire_color, line_duration);
		Debug.DrawLine(upper_right, upper_left, wire_color, line_duration);
		Debug.DrawLine(upper_left, lower_left, wire_color, line_duration);
	} // DrawRect3d()
    #endregion

} // Rect_Extensions()
