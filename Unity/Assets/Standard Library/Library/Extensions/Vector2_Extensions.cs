﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Vector2_Extensions
{
	#region /- MathF ---------------------------------------------------------------------------------------------------
	/// <summary>
	/// Converts the vector's values to integers (basically truncates the decimal values).
	/// </summary>
	/// <returns>The vector truncated to whole numbers.</returns>
	/// <param name="source_vector">Source_vector.</param>
	public static Vector2 ToInt(this Vector2 source_vector)
	{
		Vector2 output_vector = source_vector;
	
		output_vector.x = (int) output_vector.x;
		output_vector.y = (int) output_vector.y;

		return( output_vector );
	}

	/// <summary>
	/// Rounds the specified source_vector to the nearest steps value.
	/// </summary>
	/// <param name="source_vector">Source_vector.</param>
	public static Vector2 Round(this Vector2 source_vector)
	{
		return( source_vector.Round(1.0f) );
	}

	/// <summary>
	/// Rounds the specified source_vector to the nearest steps value.
	/// </summary>
	/// <param name="source_vector">Source_vector.</param>
	/// <param name="steps">Steps.</param>
	public static Vector2 Round(this Vector2 source_vector, float steps)
	{
		Vector2 output_vector = source_vector;
		
		output_vector.x = Mathf.Floor((output_vector.x + (steps/2)) / steps) * steps;
		output_vector.y = Mathf.Floor((output_vector.y + (steps/2)) / steps) * steps;

		return( output_vector );
	}
	
	/// <summary>
	/// Returns the absolute values of source_vector.
	/// </summary>
	/// <param name="source_vector">Source_vector.</param>
	public static Vector2 Abs(this Vector2 source_vector)
	{
		Vector2 output_vector = source_vector;
		
		output_vector.x = Mathf.Abs(output_vector.x);
		output_vector.y = Mathf.Abs(output_vector.y);

		return( output_vector );
	}

	/// <summary>
	/// Clamp the specified source_vector's parameters to be within the specified min and max values.
	/// </summary>
	/// <param name="source_vector">Source_vector.</param>
	/// <param name="min">Minimum.</param>
	/// <param name="max">Max.</param>
	public static Vector2 Clamp(this Vector2 source_vector, float min, float max)
	{
		source_vector.x = Mathf.Clamp(source_vector.x, min, max);
		source_vector.y = Mathf.Clamp(source_vector.y, min, max);
		return(source_vector);
	}

	/// <summary>
	/// Clamps the angle to be within 0 to 360 degrees, then within a min/max range.
	/// </summary>
	/// <returns>The angle clamped to the min/max range.</returns>
	/// <param name="angle">Angle to be clamped.</param>
	/// <param name="range">A Vector2 of the range in degrees.</param>
	public static Vector2 ClampAngle(this Vector2 angle, Vector2 range )
	{
		return( angle.ClampAngle( range.x, range.y ) );
	}
	/// <summary>
	/// Clamps the angle to be within 0 to 360 degrees, then within a min/max range.
	/// </summary>
	/// <returns>The angle.</returns>
	/// <param name="angle">Angle to be clamped.</param>
	/// <param name="min">Minimum degree allowed.</param>
	/// <param name="max">Maximum degree allowed.</param>
	public static Vector2 ClampAngle(this Vector2 angle, float min, float max )
	{
		angle.x = angle.x.ClampAngle(min, max);
		angle.y = angle.y.ClampAngle(min, max);
		return( angle );
	}

	/// <summary>
	/// Compares two Vector3 values if they are similar. 
	/// </summary>
	/// <param name="source">Source Vector3.</param>
	/// <param name="compare_to">Target Vector3 to compare against.</param>
	/// <param name="tolerance">Tolerance range, defaults to 0.1f</param>
	/// <returns>True if all values are close in approximation; Otherwise false.</returns>
	public static bool Approx(this Vector2 source, float x, float y, float tolerance = 0.1f)
	{
		return (source.Approx(new Vector2(x, y), tolerance));
	}

	/// <summary>
	/// Compares two Vector3 values if they are similar. 
	/// </summary>
	/// <param name="source">Source Vector3.</param>
	/// <param name="compare_to">Target Vector3 to compare against.</param>
	/// <param name="tolerance">Tolerance range, defaults to 0.1f</param>
	/// <returns>True if all values are close in approximation; Otherwise false.</returns>
	public static bool Approx(this Vector2 source, Vector2 compare_to, float tolerance = 0.1f)
	{
		bool result_x = Mathf.Abs(source.x - compare_to.x) < tolerance;
		bool result_y = Mathf.Abs(source.y - compare_to.y) < tolerance;

		return (result_x && result_y);
	} // Approx()
	#endregion

	#region /- GetCentroid ---------------------------------------------------------------------------------------------
	/// <summary>
	/// Gets the centroid of an n-sided polygon.
	/// </summary>
	/// <returns>The centroid as a Vector2.</returns>
	/// <param name="points">List of Vector2 points of the polygon.</param>
	public static Vector2 GetCentroid(this List<Vector2> points)
	{
		return( points.ToArray().GetCentroid() );
	} // GetCentroid()

	/// <summary>
	/// Gets the centroid of an n-sided polygon.
	/// </summary>
	/// <returns>The centroid as a Vector2.</returns>
	/// <param name="points">Array of Vector2 points of the polygon.</param>
	public static Vector2 GetCentroid(this Vector2[] points)
	{
		int n = points.Length;
		float a = points.GetArea();
		int j = 1;
		float cx, cy, t;
		cx = cy = t = 0.0f;

		// Calculate the centroid
		j = 1;
		for (int i=0; i<n; i++)
		{
			Vector3 p1 = points[i];
			Vector3 p2 = points[j];

			t = (p1.x * p2.y) - (p2.x * p1.y);

			cx += (p1.x + p2.x) * t;
			cy += (p1.y + p2.y) * t;
			j = (j + 1) % n;

			Debug.Log(i+") t = "+t+"\nc = "+cx+", "+cy);
		}
		cx = cx / (6.0f * a);
		cy = cy / (6.0f * a);

		return( new Vector2(cx, cy) );
	} // GetCentroid()
	#endregion

	#region /- GetArea ---------------------------------------------------------------------------------------------
	/// <summary>
	/// Gets the area of the n-sided polygon.
	/// </summary>
	/// <returns>The area in world units.</returns>
	/// <param name="points">List of Vector2 points of an n-sided polygon.</param>
	public static float GetArea(this List<Vector2> points)
	{		
		return( points.ToArray().GetArea() );
	}

	/// <summary>
	/// Gets the area of the n-sided polygon.
	/// </summary>
	/// <returns>The area in world units.</returns>
	/// <param name="points">Array of Vector2 points of an n-sided polygon.</param>
	public static float GetArea(this Vector2[] points)
	{		
		int n = points.Length;
		float a = 0.0f;
		int j = 1;

		// Area of the polygon
		for (int i=0; i<n; i++)
		{
			Vector3 p1 = points[i];
			Vector3 p2 = points[j];
			a += (p1.x * p2.y) - (p2.x * p1.y);
			j = (j + 1) % n;
		}
		a *= 0.5f; // divide by half 

		return( a );
	} // GetArea()
	#endregion

	#region /- Vector3 -------------------------------------------------------------------------------------------------
	/// <summary>
	/// Converts a Vector2 to Vector3, swapping the Y and Z values.
	/// </summary>
	/// <returns>a Vector3</returns>
	/// <param name="source">Source Vector2.</param>
	public static Vector3 To3D(this Vector2 source)
	{
		return( new Vector3( source.x, 0, source.y ) );
	}
	#endregion

	#region /- Mathfx --------------------------------------------------------------------------------------------------
	public static Vector2 Hermite(Vector2 from, Vector2 to, float t)
	{
		Vector2 result = from;
		result.x = Mathfx.Hermite(from.x, to.x, t);
		result.y = Mathfx.Hermite(from.y, to.y, t);
		return( result );
	}
	public static Vector2 HermiteAngle(Vector2 from, Vector2 to, float t)
	{
		Vector2 result = from;
		result.x = Mathfx.HermiteAngle(from.x, to.x, t);
		result.y = Mathfx.HermiteAngle(from.y, to.y, t);
		return( result );
	}
	public static Vector2 Sinerp(Vector2 from, Vector2 to, float t)
	{
		Vector2 result = from;
		result.x = Mathfx.Sinerp(from.x, to.x, t);
		result.y = Mathfx.Sinerp(from.y, to.y, t);
		return( result );
	}
	public static Vector2 SinerpAngle(Vector2 from, Vector2 to, float t)
	{
		Vector2 result = from;
		result.x = Mathfx.SinerpAngle(from.x, to.x, t);
		result.y = Mathfx.SinerpAngle(from.y, to.y, t);
		return( result );
	}
	public static Vector2 Coserp(Vector2 from, Vector2 to, float t)
	{
		Vector2 result = from;
		result.x = Mathfx.Coserp(from.x, to.x, t);
		result.y = Mathfx.Coserp(from.y, to.y, t);
		return( result );
	}
	public static Vector2 CoserpAngle(Vector2 from, Vector2 to, float t)
	{
		Vector2 result = from;
		result.x = Mathfx.CoserpAngle(from.x, to.x, t);
		result.y = Mathfx.CoserpAngle(from.y, to.y, t);
		return( result );
	}
	public static Vector2 Berp(Vector2 from, Vector2 to, float t)
	{
		Vector2 result = from;
		result.x = Mathfx.Berp(from.x, to.x, t);
		result.y = Mathfx.Berp(from.y, to.y, t);
		return( result );
	}
	public static Vector2 BerpAngle(Vector2 from, Vector2 to, float t)
	{
		Vector2 result = from;
		result.x = Mathfx.BerpAngle(from.x, to.x, t);
		result.y = Mathfx.BerpAngle(from.y, to.y, t);
		return( result );
	}
	public static Vector2 Bounce(Vector2 from, Vector2 to, float t)
	{
		Vector2 result = from;
		t = Mathfx.Bounce(t);
		result.x = Mathf.Lerp(from.x, to.x, t);
		result.y = Mathf.Lerp(from.y, to.y, t);
		return( result );
	}
	public static Vector2 BounceAngle(Vector2 from, Vector2 to, float t)
	{
		Vector2 result = from;
		t = Mathfx.Bounce(t);
		result.x = Mathf.LerpAngle(from.x, to.x, t);
		result.y = Mathf.LerpAngle(from.y, to.y, t);
		return( result );
	}

	#endregion

} // Vector2_Extensions
