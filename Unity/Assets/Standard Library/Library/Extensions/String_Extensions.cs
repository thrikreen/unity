﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public static class String_Extensions 
{
	#region /- LineWrap ------------------------------------------------------------------------------------------------
	// Source: http://wiki.unity3d.com/index.php/StringUtil
	// lineWrap
	//  Splits string into lines which are _charsPerLine or shorter.
	//
	//  Current word is buffered so that if _charsPerLine is reached in the middle
	//  of a word the entire word appears on the next line.
	//
	//  Note that variable width fonts are not accounted for so it is likely
	//  you will have to set _charsPerLine much shorter than desired so that lines
	//  with several capital letters or wide chars wrap correctly.
	//
	//  Also takes carriage returns (\n) already in the string into account.
	//
	//
	// Parameters:
	//  string _str      - string to process
	//  int _charPerLine - max # of characters per line.
	//  bool _forceWrap  - if set to true, a continuous string of characters with no spaces
	//						word will be forced to wrap to _charsPerLine 
	//						if set to false, word will stay intact and violate _charsPerLine
	//
	// TODO:
	//	Don't count the space at end of a line.
	//  _forceWrap can cause somewhat odd behavior as it is a very simple implementation.
	//
	//  Provided by typeRice - June 12, 2009
	public static string lineWrap( this string _str, int _charsPerLine, bool _forceWrap )
	{
		if( _str.Length < _charsPerLine ) return(_str); // string is shorter than the limit

		string	result = "",
		buf = "";								// Stores current word
		
		int cursor		= 0,					// Position within _str
		charCount	= 0;						// # of chars on current line
		
		bool bLineEmpty = false;				// if a new line was added to the result &
		// buf has not been appended.
		
		while ( cursor < _str.Length )
		{
			buf += _str[cursor];				// add next character to buffer
			charCount++;						// increment count of chars on current line
			
			if ( _str[cursor] == ' ' )			// if space is encountered
			{
				result += buf;					// write the buffer to the result and clear
				buf = "";
				bLineEmpty = false;				// Something added since the last carriage return
			}
			else if ( _str[cursor] == '\n' )	// manual carriage return encountered
			{
				result += buf;					// write the buffer to the result and clear (buf contains the \n already)
				buf = "";
				charCount=0;					// Start new line so reset character count.
			}
			
			if ( charCount >= _charsPerLine ) 	// if charCount has reached max chars per line
			{
				if ( !bLineEmpty )				// If line has something in it.
				{
					result += '\n';				// Start a new line in the result
					charCount = buf.Length;		// reset character count to current buf size as ths will be placed on the new line.
					bLineEmpty = true;			// Newest line is empty
				}
				else if ( _forceWrap )			// else if the line is empty and we want to force word to wrap
				{
					result += buf + '\n';		// write the buffer to the result with a carriage return
					buf = "";					// clear the buffer
					bLineEmpty = true;			// Newest line is empty
					charCount = 0;				// Start new line so reset character count.
				}
			}
			
			cursor++;							// Goto next character
		}
		
		result += buf;							// add any remaining characters in the buffer
		return( result );
	} // lineWrap()
	#endregion

	#region /- Formatting ----------------------------------------------------------------------------------------------
	/// <summary>
	/// Uppercases the first letter of the string.
	/// </summary>
	/// <returns>The string with the first letter capitalized.</returns>
	/// <param name="s">Source string to upper-case the first character (if applicable)</param>
	/// <param name="all_words">If true, it will uppercase all words, not just the first one.</param>
	public static string UppercaseFirst(this string s, bool all_words=false)
	{
		// Check for empty string.
		if (string.IsNullOrEmpty(s)) return string.Empty;

		if (all_words)
		{
			string output = "";

			string[] split = s.Split(' ');
			for (int i=0; i<split.Length; i++)
			{
				output += split[i].UppercaseFirst() + " ";
			}
			
			return (output);
		}
		else
		{
			// Return char and concat the rest of the substring.
			return (char.ToUpper(s[0]) + s.Substring(1));
		}
	}

	/// <summary>
	/// Pads the string to max_length characters at the end (right side), using the specified character.
	/// </summary>
	/// <param name="s">String</param>
	/// <param name="pad_char">Pad_char - the character to pad the string with.</param>
	/// <param name="max_length">Max_length - how many characters to pad to</param>
	/// <param name="padded_chars_only">padded_chars_only - Return the pad portion only. Defaults to false, so normally it returns the source and padded.</param>
	public static string Pad(this string s, char pad_char, int max_length, bool padded_chars_only=false )
	{
		string padded_s = s;
		int pad_length = max_length - padded_s.Length;
		if (padded_chars_only) padded_s = "";
		for (int i=0; i<pad_length; i++) 
		{
			padded_s = pad_char+padded_s;
		}
		return(padded_s);
	}
	#endregion

	#region /- Conversions ---------------------------------------------------------------------------------------------
	/// <summary>
	/// Attempts to convert the string into an integer value.
	/// </summary>
	/// <returns>The string converted to an int, if successful. Returns 0 if unsuccessful.</returns>
	/// <param name="s">The source string.</param>
	public static int ToInt(this string s)
	{
		int result = 0;
		if ( Int32.TryParse(s, out result) )
		{
		}
		else
		{
			//Debug.LogError("Unable to convert '"+s+"' into an integer.");
		}
		return( result );
	} // ToInt()

	/// <summary>
	/// Attempts to convert the string into a float value.
	/// </summary>
	/// <returns>The string converted to a float, if successful. Returns 0 if unsuccessful.</returns>
	/// <param name="s">The source string.</param>
	public static float ToFloat(this string s)
	{
		float result = 0.0f;
		if ( float.TryParse(s, out result) )
		{
		}
		else
		{
			//Debug.LogError("Unable to convert '"+s+"' into a float.");
		}
		return( result );
	} // ToFloat()

	/// <summary>
	/// Attempts to convert the string into a double value.
	/// </summary>
	/// <returns>The double.</returns>
	/// <param name="s">The source string.</param>
	public static double ToDouble(this string s)
	{
		double result = 0;
		if ( double.TryParse(s, out result) )
		{
		}
		else
		{
			//Debug.LogError("Unable to convert '"+s+"' into a float.");
		}
		return( result );
	} // ToDouble()

	/// <summary>
	/// Splits a CamelCase string into separate words.
	/// </summary>
	/// <param name="s"></param>
	/// <returns></returns>
	public static string splitCamelCase(this string s)
	{
        return Regex.Replace(
		Regex.Replace(
			s,
			@"(\P{Ll})(\P{Ll}\p{Ll})",
			"$1 $2"
		),
		@"(\p{Ll})(\P{Ll})",
		"$1 $2"
		);
	}
	#endregion

	#region /- WWW -----------------------------------------------------------------------------------------------------
	/// <summary>
	/// Checks and returns a sanitized URL string. Prefixes with http:// if missing, converts \ to /, and removes double //
	/// Should also make URL spaces into %20
	/// </summary>
	/// <returns>The sanitized URL</returns>
	/// <param name="s">Source string to sanitize into a URL.</param>
	/// <param name="is_path">If set to <c>true</c> assumes the URL is a path and adds a trailing /.</param>
	public static string ToURL(this string s, bool is_path=false)
	{
		string url = s;

		// Remove preceding "http://", add later
		url = url.Replace("http://", "");

		// Sanitize URL
		url = url.Replace("\\", "/"); // Replace all backslashes into a slash
		while (url.Contains("//")) url = url.Replace("//", "/"); // Replace all double slashes to single

		// Encoding
		url = System.Uri.EscapeUriString(url);

		if (is_path) // If no trailing slash, add it
		{
			if (url[url.Length-1] != '/')
			{
				url += "/";
			}
		}

		// Add the protocol prefix
		url = "http://"+url;
		return(url);
	} // ToURL()
	#endregion

	#region /- File I/O ---------------------------------------------------------------------------------------------------
	/// <summary>
	/// Sanitizes a file path (converts backslashes, removes duplicate slashes, etc.)
	/// </summary>
	/// <param name="s">Source file path string.</param>
	/// <param name="is_path">Defaults to false. If set to true, assumes the path is for a directory, and will add a trailing / to the end of the path (even if there's a file extension).</param>
	/// <returns>Sanitized file path.</returns>
	public static string ToPath(this string s, bool is_path = false)
	{
		string path = s;

		// Sanitize path
		path = path.Replace("\\", "/"); // Replace all backslashes into a slash \ -> /
		while (path.Contains("//")) path = path.Replace("//", "/"); // Replace all double slashes to single slash

		if (is_path) // If no trailing slash, add it
		{
			if (path[path.Length - 1] != '/') path += "/";
		}

		return (path);
	} // ToPath()

	public static string ToFilename(this string s)
	{
		char[] invalids = System.IO.Path.GetInvalidFileNameChars();
		string filename = String.Join("_", s.Split(invalids, StringSplitOptions.RemoveEmptyEntries) ).TrimEnd('.');
		return (filename);
	}
	#endregion

	#region /- SplitKeyValue -------------------------------------------------------------------------------------------
	/// <summary>
	/// Splits a string into key/value pairs and returns a Dictionary<string,string>.
	/// </summary>
	/// <returns>A dictionary of the key/value pairs.</returns>
	/// <param name="s">S - the source string to split.</param>
	/// <param name="delimiter">Delimiter between key/value pairs, defaults to ';'.</param>
	/// <param name="equals">Equals character for the key/value assignment, defaults to '='.</param>
	public static Dictionary<string,string> SplitKeyValue(this string s, string delimiter=";", string equals="=")
	{
		Dictionary<string,string> keyvalue_dict = new Dictionary<string, string>();

		// Split by pair separator, defaults to ';'
		string[] pairs = s.Split(new string[]{delimiter}, StringSplitOptions.RemoveEmptyEntries);
		string debuginfo = string.Format("SplitKeyValue( \"{0}\", \'{1}\', \'{2}\' ): {3} pairs\n", s, delimiter, equals, pairs.Length);

		foreach (string prop in pairs)
		{
			// Check if it is in a valid format
			debuginfo += "\"" + prop + "\" : ";
			if (!prop.Contains(equals)) // not in a valid key=value pair format
			{
				debuginfo += " not a valid key/value pair, skipping.\n";
				continue; 
			}
						
			// Separate into the key, value components, default '='. 
			// We retain empty entries as we might want to remove a value from a key and blank it out
			string[] props = prop.Split(new string[]{equals}, 2, StringSplitOptions.None);
			if (props[0] == "")  // no key value
			{
				debuginfo += "no key specified, skipping.\n";
				continue;
			}

			// Add it to the dictionary
			debuginfo += props[0] + " = " + props[1] + "\n";
			keyvalue_dict.Add(props[0], props[1]);
		}
		//Debug.Log( debuginfo );
		return( keyvalue_dict );
	} // SplitKeyValue()


	/// <summary>
	/// Splits a comma separated string line into individual components. Grabbed from: http://stackoverflow.com/questions/1375410/very-simple-c-sharp-csv-reader
	/// </summary>
	/// <param name="s">The source string</param>
	/// <param name="fieldSep">Field separator, defaults to , (comma)</param>
	/// <param name="stringSep">If a field is encapsulated with quotes, it will ignore fieldSep inside of it until it encounters another stringSep. Defaults to " (double quotes)</param>
	/// <returns>A string array of the separated fields.</returns>
	public static string[] CSVRowToStringArray(this string s, char fieldSep = ',', char stringSep = '\"')
	{
		bool bolQuote = false;
		System.Text.StringBuilder bld = new System.Text.StringBuilder();
		List<string> retAry = new List<string>();

		foreach (char c in s.ToCharArray())
		{
			if ((c == fieldSep && !bolQuote))
			{
				retAry.Add(bld.ToString());
				bld = new System.Text.StringBuilder();
			}
			else if (c == stringSep)
			{
				bolQuote = !bolQuote;
			}
			else
			{
				bld.Append(c);
			}
		}

		/* to solve the last element problem */
		retAry.Add(bld.ToString()); /* added this line */
		return retAry.ToArray();
	} // CSVRowToStringArray()
	#endregion

	#region /- Generation ----------------------------------------------------------------------------------------------
	/// <summary>
	/// Generates a string of random characters of X length.
	/// </summary>
	/// <param name="length"></param>
	/// <returns></returns>
	public static string GenerateRandomName(int length = 16)
	{
		string name = "";
		string source = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		for (int i = 0; i < length; i++)
		{
			name += source[UnityEngine.Random.Range(0, source.Length)];
		}
		return (name);
	}
	#endregion


} // String_Extensions()
