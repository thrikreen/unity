﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Collections.Generic;

public enum BoundsType
{
	none,

	/// <summary>
	/// The minimum bounds size on the X,Y,Z axis
	/// </summary>
	minimum,
	
	/// <summary>
	/// The center of the bounds
	/// </summary>
	center,
	
	/// <summary>
	/// The maximum bounds size on the X,Y,Z axis
	/// </summary>
	maximum,
	
	/// <summary>
	/// The extents (size/2)
	/// </summary>
	extents,
	
	/// <summary>
	/// The total size of the bounds.
	/// </summary>
	size
}

public static class GameObject_Extensions 
{
	#region /- Variables -----------------------------------------------------------------------------------------------

	#endregion

	#region /- GetAllChildren ------------------------------------------------------------------------------------------
	/// <summary>
	/// Recursively grabs and returns all GameObjects of the specified parent GameObject.
	/// </summary>
	/// <returns>The all children.</returns>
	/// <param name="parent_object">Parent_object to grab all its children objects.</param>
	public static GameObject[] GetAllChildren(this GameObject parent_object)
	{
		return (parent_object.GetAllChildrenAsList().ToArray());
	} // Returns an array
	public static List<GameObject> GetAllChildrenAsList(this GameObject parent_object)
	{
		List<GameObject> object_list = new List<GameObject>();

		if (parent_object == null) return(object_list);

		// Add the parent object first
		if (!object_list.Contains(parent_object)) object_list.Add(parent_object.gameObject);
		
		// Loop all the children objects for this parent object
		foreach (Transform child_object in parent_object.transform)
		{
			if (child_object == null) continue;
			if (child_object.transform.childCount > 0) // has children, get them via recursion
			{
				object_list.AddRange( child_object.gameObject.GetAllChildrenAsList() );
			}
			else // no children, just add this one
			{
				if (!object_list.Contains(child_object.gameObject)) 
				{
					object_list.Add(child_object.gameObject);
				}
			}
		}
		
		// All done, return list as an array
		return(object_list);
	} // Returns a List
	#endregion

	#region /- GetBounds -----------------------------------------------------------------------------------------------
	/// <summary>
	/// Gets the object bounds.
	/// </summary>
	/// <returns>The bounds.</returns>
	/// <param name="target_obj">Target_obj.</param>
	/// <param name="bounds_filter">Bounds_filter.</param>
	/// <param name="get_children">If set to <c>true</c> get the children and return the bounds for all of them.</param>
	/// <param name="colliders_only">If set to <c>true</c> only calculate objects with colliders. By default it calculates the mesh bounds.</param>
	/// <param name="ignore_empty">If set to <c>true</c> empty GameObjects are ignored, otherwise it factors in its position.</param>
	/// <param name="local_transform">If set to <c>true</c> it won't be an axis-aligned bounds, but based on the object's rotation.</param>
	/// <param name="ignore_objs">Array of gameObjects to ignore in the bounds generation.</param>
	public static Vector3 GetBounds(this GameObject target_obj, 
	                                BoundsType bounds_filter, 
	                                bool get_children=true,
	                                bool colliders_only=false, 
	                                bool ignore_empty=false,
	                                bool local_transform=false,
	                                params GameObject[] ignore_objs
	                                )
	{
		// Returns them minimum, maximum, or center values of the object's bounds - an axis-aligned bounding box (AABB)
		// of the min/max of the object (and it's children), ignoring any objects that match in ignore_objs.
		
		// Variables
		Vector3 min_bounds = Vector3.zero;
		Vector3 max_bounds = Vector3.zero;
		bool is_first_object = true;
		Quaternion original_rotation = target_obj.transform.rotation;
		List<GameObject> all_objects = new List<GameObject>();
		
		// Get our obj list
		if (get_children)
		{
			all_objects = target_obj.GetAllChildrenAsList();
		}
		else
		{
			all_objects.Add(target_obj);
		}
		
		if (local_transform)
		{
			target_obj.transform.rotation = Quaternion.Euler(0, 0, 0);
		}
		
		// Process each object and get its bounds
		//Debug.Log("GetBounds("+all_objects.Count+"):");
		for (int c = 0; c < all_objects.Count; c++)
		{
			GameObject obj = all_objects[c];
			
			// Ignore this object?
			bool skip_obj = false;
			if (ignore_objs.Length > 0)
			{
				foreach (GameObject ignore_obj in ignore_objs)
				{
					if (obj == ignore_obj) skip_obj = true;
				}
			}
			if (skip_obj) continue; // skip to next object
			
			
			MeshFilter meshf = (MeshFilter) obj.GetComponent<MeshFilter>();
			Vector3 min = Vector3.zero;
			Vector3 max = Vector3.zero;
			
			if (colliders_only)
			{
				if (obj.GetComponent<Collider>() == null) continue;
				min = obj.GetComponent<Collider>().bounds.min;
				max = obj.GetComponent<Collider>().bounds.max;
			}
			else if (meshf != null)
			{
				// Get the world position of its bounds, in relation to the mesh object's position and rotation
				min = obj.GetComponent<Renderer>().bounds.min;
				max = obj.GetComponent<Renderer>().bounds.max;
			}
			// To Do: Maybe have some other method to recognize specific object types and their size
			// but that'll depend on needs of the project and custom objects, but they'd go here
			else if (!ignore_empty)
			{
				// possibly empty object, if ignore_empty == false, then just grab its position.
				min = obj.transform.position;
				max = obj.transform.position;
			}
			
			//Debug.Log("Object: " + obj.name + ".min: " + obj.renderer.bounds.min + " -> " + min + "\nObject: " + obj.name + ".max: " + obj.renderer.bounds.max + " -> " + max, obj);
			if (is_first_object) // no bounds stored, so we just take our first object's values
			{
				is_first_object = false;
				min_bounds = min;
				max_bounds = max;
			}
			else
			{
				min_bounds = Vector3.Min(min_bounds, min);
				max_bounds = Vector3.Max(max_bounds, max);
			}
			
		} // Loop objs
		
		if (local_transform)
		{
			target_obj.transform.rotation = original_rotation;
		}
		
		// Return the values based on what type was requested
		if (bounds_filter == BoundsType.minimum) // smallest corner
		{
			return (min_bounds);
		}
		else if (bounds_filter == BoundsType.maximum) // largest corner
		{
			return (max_bounds);
		}
		else if (bounds_filter == BoundsType.center) // center of the gameObject
		{
			return ((min_bounds + max_bounds) / 2);
		}
		else if (bounds_filter == BoundsType.extents) // extents, half of the size
		{
			return ((max_bounds - min_bounds)/2);
		}
		else if (bounds_filter == BoundsType.size) // total size of the box
		{
			return (max_bounds - min_bounds);
		}
		else
		{
			return (Vector3.zero);
		}
	}
    #endregion

    #region /- Reset Transforms ----------------------------------------------------------------------------------------
    /// <summary>
    /// Resets the transforms, placing the object at the world origin, no rotations and scale to 1.0x
    /// </summary>
    /// <param name="targetObject">targetObject to reset its transforms.</param>
    public static void ResetTransforms(this GameObject targetObject)
    {
        targetObject.transform.position = Vector3.zero;
        targetObject.transform.rotation = Quaternion.identity;
        targetObject.transform.localScale = Vector3.one;
    } // ResetTransforms()

    /// <summary>
    /// Resets the local transforms, placing the object at the world origin, no rotations and scale to 1.0x
    /// </summary>
    /// <param name="targetObject">targetObject to reset its transforms.</param>
    public static void ResetLocalTransforms(this GameObject targetObject)
    {
        targetObject.transform.localPosition = Vector3.zero;
        targetObject.transform.localRotation = Quaternion.identity;
        targetObject.transform.localScale = Vector3.one;
    } // ResetLocalTransforms()
    #endregion

    #region /- GameObject ----------------------------------------------------------------------------------------------

    /*
	/// <summary>
	/// Finds the scene GameObject with the specified InstanceID (or with a Component with the InstanceID)
	/// </summary>
	/// <returns>The GameObject with the InstanceID.</returns>
	/// <param name="source_obj">Source_obj.</param>
	/// <param name="instance_id">Instance_id.</param>
	public static GameObject GetByInstanceId(this GameObject source_obj, int instance_id)
	{
		// Get all the scene GameObjects
		GameObject[] objs = (GameObject[]) GameObject.FindObjectsOfType<GameObject>();
		
		for (int i=0; i<objs.Length; i++)
		{
			// Check GameObject
			GameObject target_obj = objs[i];
			if (target_obj == null) continue;

			// Does the InstanceID match?
			int obj_id = target_obj.GetInstanceID();
			if (obj_id == instance_id) return( objs[i] );
			
			// Not the GameObject, check the components on it
			Component[] comps = target_obj.GetComponents<Component>();
			for (int c=0; c<comps.Length; c++)
			{
				Component comp = comps[c];
				if (comp == null) continue;

				// Does the InstanceID match?
				int comp_id = comp.GetInstanceID();
				if (comp_id == instance_id) return( objs[i] );
			} // Loop Components
		} // loop scene GameObjects
		return( null );
	} // GetByInstanceId()
	*/

    /// <summary>
    /// Finds the scene GameObject with the specified InstanceID (or with a Component with the InstanceID)
    /// </summary>
    /// <returns>The GameObject with the InstanceID.</returns>
    /// <param name="source_obj">Source_obj.</param>
    /// <param name="instance_id">Instance_id.</param>
    public static GameObject GetByInstanceId(int instance_id)
	{
		// Get all the scene GameObjects
		GameObject[] objs = (GameObject[]) GameObject.FindObjectsOfType<GameObject>();
		
		for (int i=0; i<objs.Length; i++)
		{
			// Check GameObject
			GameObject target_obj = objs[i];
			if (target_obj == null) continue;
			
			// Does the InstanceID match?
			int obj_id = target_obj.GetInstanceID();
			if (obj_id == instance_id) return( objs[i] );
			
			// Not the GameObject, check the components on it
			Component[] comps = target_obj.GetComponents<Component>();
			for (int c=0; c<comps.Length; c++)
			{
				Component comp = comps[c];
				if (comp == null) continue;
				
				// Does the InstanceID match?
				int comp_id = comp.GetInstanceID();
				if (comp_id == instance_id) return( objs[i] );
			} // Loop Components
		} // loop scene GameObjects
		return( null );
	} // GetByInstanceId()

	/// <summary>
	/// Sets the active state of all the GameObjects in the provided list.
	/// </summary>
	/// <param name="target">Target list of GameObjects.</param>
	/// <param name="activeState">State to set them to.</param>
	public static void SetActive(this List<GameObject> target, bool activeState)
	{
		for (int i=0; i<target.Count; i++) target[i].SetActive(activeState);
	}

	/// <summary>
	/// Sets the active state of all the GameObjects in the provided array.
	/// </summary>
	/// <param name="target">Target array of GameObjects.</param>
	/// <param name="activeState">State to set them to.</param>
	public static void SetActive(this GameObject[] target, bool activeState)
	{
		for (int i = 0; i < target.Length; i++) target[i].SetActive(activeState);
	}
	
	/// <summary>
	/// Sets the active state of all the Components Behaviours in the provided list.
	/// </summary>
	/// <param name="target">Target list of Components.</param>
	/// <param name="activeState">State to set them to.</param>
	public static void SetActive(this List<Behaviour> target, bool activeState)
	{
		for (int i = 0; i < target.Count; i++) target[i].enabled = activeState;
	}
	/// <summary>
	/// Sets the active state of all the Components Behaviours in the provided array.
	/// </summary>
	/// <param name="target">Target array of Components.</param>
	/// <param name="activeState">State to set them to.</param>
	public static void SetActive(this Behaviour[] target, bool activeState)
	{
		for (int i = 0; i < target.Length; i++) target[i].enabled = activeState;
	}
	#endregion

	#region /- GetOrAddComponent ---------------------------------------------------------------------------------------
	/// <summary>
	/// Returns the component of Type T if the game object has one attached, or adds that component class if not found.
	/// </summary>
	/// <returns>The component.</returns>
	/// <param name="child">Child - the Transform to get or add the component to.</param>
	/// <typeparam name="T">The component type to get and/or add.</typeparam>
	public static T GetOrAddComponent<T>(this Transform child) where T : Component 
	{
		T result = child.GetComponent<T>();
		if (result == null) {
			result = child.gameObject.AddComponent<T>();
		}
		return result;
	}

	/// <summary>
	/// Returns the component of Type T if the game object has one attached, or adds that component class if not found.
	/// </summary>
	/// <returns>The component.</returns>
	/// <param name="child">Child - the GameObject to get or add the component to.</param>
	/// <typeparam name="T">The component type to get and/or add.</typeparam>
	public static T GetOrAddComponent<T>(this GameObject child) where T : Component 
	{
		T result = child.GetComponent<T>();
		if (result == null) {
			result = child.AddComponent<T>();
		}
		return result;
	}

	/// <summary>
	/// Returns a dictionary of components, for caching.
	/// </summary>
	/// <returns>The components in a dictionary of component name, component class.</returns>
	/// <param name="obj">Object to get the components from.</param>
	public static Dictionary<string, Component> CacheComponents(this GameObject obj)
	{
		Dictionary<string, Component> component_list = new Dictionary<string, Component>();
		Component[] comp_list = obj.GetComponents<Component>();
		foreach (Component comp in comp_list)
		{
			// Component Name/Type
			string comp_name = comp.GetType().Name.ToLower();
			if ( !component_list.ContainsKey(comp_name) ) component_list.Add(comp_name, comp);
		}
		return( component_list );
	} // CacheComponents()

	/// <summary>
	/// Gets the children of the target GameObject with the specified component.
	/// </summary>
	/// <returns>The children with component.</returns>
	/// <param name="parent_obj">Parent_obj.</param>
	/// <typeparam name="T">The type of component parameter.</typeparam>
	public static GameObject[] GetChildrenWithComponent<T>(this GameObject parent_obj)
	{
		List<GameObject> children_with_component 	= new List<GameObject>();
		GameObject[] children_objs 					= parent_obj.GetAllChildren();

		for (int i=0; i<children_objs.Length; i++)
		{
			GameObject child_obj = children_objs[i];

			if (child_obj == parent_obj) continue;			// skip the parent this is called on
			if (child_obj.GetComponent<T>() != null)		// Has the component
			{
				children_with_component.Add(child_obj);
			}
		} // loop children

		return( children_with_component.ToArray() );
	} // GetChildrenWithComponent()
	#endregion

	#region /- CopyComponent -------------------------------------------------------------------------------------------
	/// <summary>
	/// Copies the component and/or replicate the settings from the source component to this GameObject.
	/// </summary>
	/// <returns>The component.</returns>
	/// <param name="destination">Destination - the target GameObject for ths new component to be duplicated to.</param>
	/// <param name="original">Original.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static T CopyComponent<T>(this GameObject destination, T original) where T : Component
	{
		System.Type type = original.GetType();
		Component copy = destination.GetOrAddComponent<T>();
		
		// Copied fields can be restricted with BindingFlags
		System.Reflection.FieldInfo[] fields = type.GetFields(); 
		foreach (System.Reflection.FieldInfo field in fields)
		{
			field.SetValue(copy, field.GetValue(original));
		}
		return(copy as T);
	}
	#endregion

	#region /- Component Info ------------------------------------------------------------------------------------------
	/// <summary>
	/// Gets the methods for this object.
	/// </summary>
	/// <returns>The methods in a dictionary of name, value</returns>
	/// <param name="obj">Object to get the info from.</param>
	public static List<string> GetMethods(System.Object obj, bool DEBUG=false)
	{
		System.Reflection.MethodInfo[] get_list = obj.GetType().GetMethods( System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly | System.Reflection.BindingFlags.Public );
		string debuginfo = obj.GetType().Name + "."+System.Reflection.MethodBase.GetCurrentMethod().Name+"( "+get_list.Length+" ):\n";
		
		List<string> name_list = new List<string>();
		for (int i=0; i<get_list.Length; i++)
		{
			System.Reflection.MethodInfo info = get_list[i];
			string name = info.Name;
			debuginfo += string.Format("\t- {0}\n", name);
			name_list.Add( name );
		} // loop methods
		
		if (DEBUG) Debug.Log( debuginfo );
		return( name_list );
	} // GetMethods()
	
	/// <summary>
	/// Gets the fields for this object.
	/// </summary>
	/// <returns>The fields in a dictionary of name, value</returns>
	/// <param name="obj">Object to get the info from.</param>
	public static Dictionary<string, System.Object> GetFields(System.Object obj, bool DEBUG=false)
	{
		System.Reflection.FieldInfo[] get_list = obj.GetType().GetFields( System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly | System.Reflection.BindingFlags.Public );
		string debuginfo = obj.GetType().Name + "."+System.Reflection.MethodBase.GetCurrentMethod().Name+"( "+get_list.Length+" ):\n";
		
		Dictionary<string, System.Object> name_value_list = new Dictionary<string, object>();
		
		for (int i=0; i<get_list.Length; i++)
		{
			System.Reflection.FieldInfo info = get_list[i];
			
			string name = info.Name;
			System.Object value = (System.Object) info.GetValue(obj);
			
			debuginfo += string.Format("\t- {0} = {1}, {2}\n", name, value, value==null?"<null>":value.GetType().Name);
			
			name_value_list.Add( name, value );
		} // loop fields
		
		if (DEBUG) Debug.Log( debuginfo );
		return( name_value_list );
	} // GetFields()
	
	/// <summary>
	/// Gets the properties for this object.
	/// </summary>
	/// <returns>The properties in a dictionary of name, value</returns>
	/// <param name="obj">Object to get the info from.</param>
	public static Dictionary<string, System.Object> GetProperties(System.Object obj, bool DEBUG=false)
	{
		System.Reflection.PropertyInfo[] get_list = obj.GetType().GetProperties( System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly | System.Reflection.BindingFlags.Public );
		string debuginfo = obj.GetType().Name + "."+System.Reflection.MethodBase.GetCurrentMethod().Name+"( "+get_list.Length+" ):\n";
		
		Dictionary<string, System.Object> name_value_list = new Dictionary<string, object>();
		
		for (int i=0; i<get_list.Length; i++)
		{
			System.Reflection.PropertyInfo info = get_list[i];
			
			string name = info.Name;
			System.Object value = (System.Object) info.GetValue(obj, null);
			
			debuginfo += string.Format("\t- {0} = {1}, {2}\n", name, value, value==null?"<null>":value.GetType().Name);
			
			name_value_list.Add( name, value );
		} // loop properties
		
		if (DEBUG) Debug.Log( debuginfo );
		return( name_value_list );
	} // GetProperties()
	#endregion


	#region /- Component Info ------------------------------------------------------------------------------------------
	/// <summary>
	/// Sets the layer for this GameObject and all its children.
	/// </summary>
	/// <param name="obj"></param>
	/// <param name="target_layer"></param>
	/// <param name="children"></param>
	public static void SetLayer(this GameObject obj, string target_layer, bool children = false)
	{
		int int_target_layer = LayerMask.NameToLayer(target_layer);

		if (int_target_layer == -1)
		{
			Debug.LogError(obj.name + " could not be set to layer " + target_layer + " because no such layer exists");
			return;
		}

		SetLayer(obj, int_target_layer);
	} // SetLayer()

	/// <summary>
	/// Sets the layer for this GameObject and all its children.
	/// </summary>
	/// <param name="obj"></param>
	/// <param name="target_layer"></param>
	/// <param name="children"></param>
	public static void SetLayer(this GameObject obj, int target_layer, bool children = false)
    {
		// Check
		if (obj == null) return;

		// Check Layer
		string string_target_layer;
		try
		{
            string_target_layer = LayerMask.LayerToName(target_layer);
        }
		catch
		{
            Debug.LogError(obj.name + " could not be set to layer " + target_layer + " because there was an error finding this layer");
            return;
        }
        if (string.IsNullOrEmpty(string_target_layer))
        {
            Debug.LogError(obj.name + " could not be set to layer " + target_layer + " because this layer has not been assigned");
            return;
        }
		
		// Set the layer
        obj.layer = target_layer;

		// Process children too?
		if (children)
		{
			foreach (GameObject go in obj.GetAllChildren())
			{
				if (go == null) continue;
				go.layer = target_layer;
			}
		}
	} // SetLayer()
	#endregion
}
