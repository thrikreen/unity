﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Float_Extensions
{
	#region /- MathF ---------------------------------------------------------------------------------------------------
	/// <summary>
	/// Converts the float's values to integers (basically truncates the decimal values).
	/// </summary>
	/// <returns>The float truncated to whole numbers.</returns>
	/// <param name="source_vector">Source float value to convert into an integer.</param>
	public static float ToInt(this float source_value)
	{
		return( (int) source_value );
    } // ToInt()

    /// <summary>
    /// Rounds the specified source_value to the nearest steps value.
    /// </summary>
    /// <param name="source_vector">source_value.</param>
    /// <param name="steps">Steps.</param>
    public static float Round(this float source_value, float steps=1.0f)
	{
		return( Mathf.Floor((source_value + (steps/2)) / steps) * steps );
    } // Round()

    /// <summary>
    /// Returns the absolute values of source_value.
    /// </summary>
    /// <param name="source_vector">source_value.</param>
    public static float Abs(this float source_value)
	{
		return( Mathf.Abs(source_value) );
    } // Abs()

    /// <summary>
    /// Clamp the specified float to be within the specified min and max values.
    /// </summary>
    /// <param name="source_vector">Source float value.</param>
    /// <param name="min">Minimum.</param>
    /// <param name="max">Max.</param>
    public static float Clamp(this float source_value, float min, float max)
	{
		return( Mathf.Clamp(source_value, min, max) );
    } // Clamp()q

    /// <summary>
    /// Clamps the angle to be within 0 to 360 degrees, then within a min/max range.
    /// </summary>
    /// <returns>The angle clamped to the min/max range.</returns>
    /// <param name="angle">Angle to be clamped.</param>
    /// <param name="range">A Vector2 of the range in degrees.</param>
    public static float ClampAngle(this float angle, Vector2 range )
	{
		return( angle.ClampAngle( range.x, range.y ) );
    } // ClampAngle()

    /// <summary>
    /// Clamps the angle to be within 0 to 360 degrees, then within a min/max range.
    /// </summary>
    /// <returns>The angle.</returns>
    /// <param name="angle">Angle to be clamped.</param>
    /// <param name="min">Minimum degree allowed.</param>
    /// <param name="max">Maximum degree allowed.</param>
    public static float ClampAngle(this float angle, float min, float max )
	{
		if (angle < -360) angle += 360;
		if (angle > 360) angle -= 360;
		return( Mathf.Clamp( angle, min, max ) );
    } // ClampAngle()

    /// <summary>
    /// Checks if the supplied value is close enough to the target value, specified by the range for min/max.
    /// </summary>
    /// <param name="value">The value to co</param>
    /// <param name="target">The target value to come close to</param>
    /// <param name="threshold">The range where the value is considered close enough to the target value.</param>
    /// <returns>True if in range, otherwise false.</returns>
    public static bool InRange(this float value, float target, float threshold)
	{
		if (value < (target - threshold)) return (false);
		if (value > (target + threshold)) return (false);
		return (true);
	} // InRange()

	/// <summary>
	/// Checks if the value is between two other values.
	/// </summary>
	/// <param name="value">The value to check.</param>
	/// <param name="min">Minimum allowed value.</param>
	/// <param name="max">Maximum allowed value.</param>
	/// <returns>True if between the values, otherwise false.</returns>
	public static bool IsBetween(this float value, float min, float max)
	{
		if (value < min) return (false);
		if (value > max) return (false);
		return (true);
	} // IsBetween()
	#endregion

	#region /- Lerp ----------------------------------------------------------------------------------------------------
	/// <summary>
	/// Gets the index normalized between 0 and 1 for the supplied time.
	/// Example: float time_index = Time.time.GetLerpIndex(time_timerstart, time_timerend);
	/// Vector3.Lerp(p1, p2, time_index);
	/// </summary>
	/// <returns>The lerp index.</returns>
	/// <param name="current_time">Current_time.</param>
	/// <param name="start_time">Start_time.</param>
	/// <param name="end_time">End_time.</param>
	public static float GetLerpIndex(this float current_time, float start_time, float end_time)
	{
		float time_duration = end_time - start_time;
		float time_current = current_time - start_time;
		float time_index = time_current / time_duration;
		time_index = Mathf.Clamp(time_index, 0, 1.0f);
		return( time_index );
	}
	#endregion

	#region /- Snap ----------------------------------------------------------------------------------------------------
	/// <summary>
	/// Snap the specified source value to the target value, if it is within the threshold of said target value.
	/// </summary>
	/// <param name="source">Source.</param>
	/// <param name="target">Target.</param>
	/// <param name="threshold">Threshold.</param>
	public static float Snap(this float source, float target, float threshold)
	{
		float result = source;
		string debuginfo = string.Format("{0} < {1} < {2} = ", (target-threshold), source, (target+threshold));

		// Check if the value of source is within the min and max thresholds of target
		if (source > (target - threshold))
		{
			debuginfo += ">";
			result = target;
		}
		else if (source < (target + threshold))
		{
			debuginfo += "<";
			result = target;
		}
		// else return the source, no change.

		Debug.LogError(debuginfo);

		return( result );
	} // Snap()
	#endregion


} // Float_Extensions
