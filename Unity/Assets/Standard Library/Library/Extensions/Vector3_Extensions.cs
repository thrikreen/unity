﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Vector3_Extensions
{
	#region /- MathF ---------------------------------------------------------------------------------------------------
	/// <summary>
	/// Converts the vector's values to integers (basically truncates the decimal values).
	/// </summary>
	/// <returns>The vector truncated to whole numbers.</returns>
	/// <param name="source_vector">Source_vector.</param>
	public static Vector3 ToInt(this Vector3 source_vector)
	{
		Vector3 output_vector = source_vector;

		output_vector.x = (int) output_vector.x;
		output_vector.y = (int) output_vector.y;
		output_vector.z = (int) output_vector.z;

		return( output_vector );
	}

	/// <summary>
	/// Rounds the specified source_vector to the nearest steps value.
	/// </summary>
	/// <param name="source_vector">Source_vector.</param>
	public static Vector3 Round(this Vector3 source_vector)
	{
		return( source_vector.Round(1.0f) );
	}

	/// <summary>
	/// Rounds the specified source_vector to the nearest steps value.
	/// </summary>
	/// <param name="source_vector">Source_vector.</param>
	/// <param name="steps">Steps.</param>
	public static Vector3 Round(this Vector3 source_vector, float steps)
	{
		Vector3 output_vector = source_vector;
		
		output_vector.x = Mathf.Floor((output_vector.x + (steps/2)) / steps) * steps;
		output_vector.y = Mathf.Floor((output_vector.y + (steps/2)) / steps) * steps;
		output_vector.z = Mathf.Floor((output_vector.z + (steps/2)) / steps) * steps;

		return( output_vector );
	}

	/// <summary>
	/// Returns the absolute values of source_vector.
	/// </summary>
	/// <param name="source_vector">Source_vector.</param>
	public static Vector3 Abs(this Vector3 source_vector)
	{
		Vector3 output_vector = source_vector;

		output_vector.x = Mathf.Abs(output_vector.x);
		output_vector.y = Mathf.Abs(output_vector.y);
		output_vector.z = Mathf.Abs(output_vector.z);

		return( output_vector );
	}

	/// <summary>
	/// Clamp the specified source_vector's parameters to be within the specified min and max values.
	/// </summary>
	/// <param name="source_vector">Source_vector.</param>
	/// <param name="min">Minimum.</param>
	/// <param name="max">Max.</param>
	public static Vector3 Clamp(this Vector3 source_vector, float min, float max)
	{
		source_vector.x = Mathf.Clamp(source_vector.x, min, max);
		source_vector.y = Mathf.Clamp(source_vector.y, min, max);
		source_vector.z = Mathf.Clamp(source_vector.z, min, max);
		return(source_vector);
	}
		
	public static Vector3 Clamp(this Vector3 source_vector, Vector3 min, Vector3 max)
	{
		source_vector.x = Mathf.Clamp(source_vector.x, min.x, max.x);
		source_vector.y = Mathf.Clamp(source_vector.y, min.y, max.y);
		source_vector.z = Mathf.Clamp(source_vector.z, min.z, max.z);
		return(source_vector);
	}

	/// <summary>
	/// Clamps the angle to be within 0 to 360 degrees, then within a min/max range.
	/// </summary>
	/// <returns>The angle clamped to the min/max range.</returns>
	/// <param name="angle">Angle to be clamped.</param>
	/// <param name="range">A Vector2 of the range in degrees.</param>
	public static Vector3 ClampAngle(this Vector3 angle, Vector2 range )
	{
		return( angle.ClampAngle( range.x, range.y ) );
	}
	/// <summary>
	/// Clamps the angle to be within 0 to 360 degrees, then within a min/max range.
	/// </summary>
	/// <returns>The angle.</returns>
	/// <param name="angle">Angle to be clamped.</param>
	/// <param name="min">Minimum degree allowed.</param>
	/// <param name="max">Maximum degree allowed.</param>
	public static Vector3 ClampAngle(this Vector3 angle, float min, float max )
	{
		angle.x = angle.x.ClampAngle(min, max);
		angle.y = angle.y.ClampAngle(min, max);
		angle.z = angle.z.ClampAngle(min, max);
		return( angle );
	}

	/// <summary>
	/// Compares two Vector3 values if they are similar. 
	/// </summary>
	/// <param name="source">Source Vector3.</param>
	/// <param name="compare_to">Target Vector3 to compare against.</param>
	/// <param name="tolerance">Tolerance range, defaults to 0.1f</param>
	/// <returns>True if all values are close in approximation; Otherwise false.</returns>
	public static bool Approx(this Vector3 source, float x, float y, float z, float tolerance = 0.1f)
	{
		return (source.Approx(new Vector3(x, y, z), tolerance));
	}

	/// <summary>
	/// Compares two Vector3 values if they are similar. 
	/// </summary>
	/// <param name="source">Source Vector3.</param>
	/// <param name="compare_to">Target Vector3 to compare against.</param>
	/// <param name="tolerance">Tolerance range, defaults to 0.1f</param>
	/// <returns>True if all values are close in approximation; Otherwise false.</returns>
	public static bool Approx(this Vector3 source, Vector3 compare_to, float tolerance = 0.1f)
	{
		/*
		bool result_x = Mathf.Approximately(source.x, compare_to.x);
		bool result_y = Mathf.Approximately(source.y, compare_to.y);
		bool result_z = Mathf.Approximately(source.z, compare_to.z);
		*/
		bool result_x = Mathf.Abs(source.x - compare_to.x) < tolerance;
		bool result_y = Mathf.Abs(source.y - compare_to.y) < tolerance;
		bool result_z = Mathf.Abs(source.z - compare_to.z) < tolerance;

		return (result_x && result_y && result_z);
	} // Approx()

	/*
	public static bool Approximately(float a, float b)
	{
		return (Mathf.Abs(b - a) < Mathf.Max(1E-06f * Mathf.Max(Mathf.Abs(a), Mathf.Abs(b)), 1.121039E-44f));
	}
	*/
	#endregion

	#region /- GetSurfaceDirection -------------------------------------------------------------------------------------
	/// <summary>
	/// Gets the surface direction, based on the desired vector direction and the surface normal.
	/// </summary>
	/// <returns>The surface direction.</returns>
	/// <param name="desired_direction">Desired_direction.</param>
	/// <param name="surface_normal">Surface_normal.</param>
	public static Vector3 GetSurfaceDirection(this Vector3 desired_direction, Vector3 surface_normal, bool normalize=true)
	{
		float surface_dot = Vector3.Dot(desired_direction, surface_normal);
		
		// Basically gets the cosine of the surface to 
		Vector3 surface_direction = (desired_direction - (surface_dot * surface_normal));
		
		if (normalize) surface_direction = surface_direction.normalized;
		
		return( surface_direction );
	}
	#endregion

	#region /- DrawNormal ----------------------------------------------------------------------------------------------
	/// <summary>
	/// Draws the normal.
	/// </summary>
	/// <param name="point">Point. Vector3 that the normal projects from.</param>
	/// <param name="normal">Normal. Direction of the normal</param>
	/// <param name="aim">Aim. Up vector, orientation of the arrow head</param>
	/// <param name="ray_color">Ray_color. Color of the normal</param>
	/// <param name="arrow_size">Arrow_size. Size of the arrow head</param>
	/// <param name="duration">Duration of ray</param>
	public static void DrawNormal(this Vector3 point, Vector3 normal)
	{
		// No aim defined, default aim vector to the main camera
		Ray camera_ray = Camera.main.ScreenPointToRay(new Vector3( Screen.width/2, Screen.height/2, 0) );
		Vector3 camera_direction = Vector3.Cross(normal, camera_ray.direction);

		point.DrawNormal(normal, camera_direction, Color.white);
	}
	public static void DrawNormal(this Vector3 point, Vector3 normal, Color ray_color, float duration = 0.0f, float arrow_size = 0.2f)
	{
		// No aim defined, default aim vector to the main camera
		//Ray camera_ray = Camera.main.ScreenPointToRay(new Vector3( Screen.width/2, Screen.height/2, 0) );
		Vector3 camera_direction = Camera.main.transform.forward; //Vector3.Cross(normal, camera_ray.direction);

		point.DrawNormal(normal, camera_direction, ray_color, duration, arrow_size);
	}
	public static void DrawNormal(this Vector3 point, Vector3 normal, Vector3 aim)
	{
		point.DrawNormal(normal, aim, Color.white);
	}
	public static void DrawNormal(this Vector3 point, Vector3 normal, Vector3 aim, Color ray_color, float duration = 0.0f, float arrow_size = 0.2f)
	{
		#if UNITY_EDITOR
		// Normal
		Debug.DrawRay(point, normal, ray_color, duration);
		
		// Arrow direction, point back in the same direction as the normal, before we rotate it
		Vector3 arrow_head = -normal.normalized;

		// Get the Cross product so we're on the same plane as the normal body
		//Vector3 right = Vector3.Cross(arrow_head, aim).normalized;
		//right = Vector3.Cross(arrow_head, right).normalized;
		//Debug.DrawRay(point + normal, right, Color.cyan);
		//Vector3 up = Vector3.Cross(arrow_head, right).normalized;
		//Debug.DrawRay(point + normal, up, Color.magenta);
		
		// Rotate along that axis
		Vector3 arrow_head_l = Quaternion.AngleAxis(15, aim) * arrow_head * arrow_size;
		Vector3 arrow_head_r = Quaternion.AngleAxis(-15, aim) * arrow_head * arrow_size;
		
		// Draw the rays
		Debug.DrawRay(point + normal, arrow_head_l, ray_color, duration);
		Debug.DrawRay(point + normal, arrow_head_r, ray_color, duration);
		#endif
	}
	#endregion

	#region /- GetCentroid ---------------------------------------------------------------------------------------------
	/// <summary>
	/// Gets the centroid of a polygon along the X-Axis (planar on the Y/Z-axis)
	/// </summary>
	/// <returns>The centroid.</returns>
	/// <param name="points3">List of Vector3 points of the polygon.</param>
	public static Vector3 GetCentroidX(this List<Vector3> points3)
	{
		return( points3.ToArray().GetCentroidX() );
	} // GetCentroidX()
	/// <summary>
	/// Gets the centroid of a polygon along the X-Axis (planar on the Y/Z-axis)
	/// </summary>
	/// <returns>The centroid.</returns>
	/// <param name="points3">Array of Vector3 points of the polygon.</param>
	public static Vector3 GetCentroidX(this Vector3[] points3)
	{
		// Convert from 3D to 2D on the X-axis
		List<Vector2> points2 = new List<Vector2>();
		foreach (Vector3 point3 in points3)
		{
			points2.Add( new Vector2( point3.y, point3.z ) );
		}
		
		// Get the Centroid
		Vector2 centroid2 = points2.GetCentroid();
		
		// Convert back to 3D
		return( new Vector3(0.0f, centroid2.x, centroid2.y) );
	} // GetCentroidX()

	/// <summary>
	/// Gets the centroid of a polygon along the Y-Axis (planar on the X/Z-axis)
	/// </summary>
	/// <returns>The centroid.</returns>
	/// <param name="points3">List of Vector3 points of the polygon.</param>
	public static Vector3 GetCentroidY(this List<Vector3> points3)
	{
		return( points3.ToArray().GetCentroidY() );
	}
	/// <summary>
	/// Gets the centroid of a polygon along the Y-Axis (planar on the X/Z-axis)
	/// </summary>
	/// <returns>The centroid.</returns>
	/// <param name="points3">Array of Vector3 points of the polygon.</param>
	public static Vector3 GetCentroidY(this Vector3[] points3)
	{
		// Convert from 3D to 2D on the Y-axis
		List<Vector2> points2 = new List<Vector2>();
		foreach (Vector3 point3 in points3)
		{
			points2.Add( new Vector2( point3.x, point3.z ) );
		}

		// Get the Centroid
		Vector2 centroid2 = points2.GetCentroid();

		// Convert back to 3D
		return( new Vector3(centroid2.x, 0.0f, centroid2.y) );
	} // GetCentroid()

	/// <summary>
	/// Gets the centroid of a polygon along the Z-Axis (planar on the X/Y-axis)
	/// </summary>
	/// <returns>The centroid.</returns>
	/// <param name="points3">List of Vector3 points of the polygon.</param>
	public static Vector3 GetCentroidZ(this List<Vector3> points3)
	{
		return( points3.ToArray().GetCentroidZ() );
	}
	/// <summary>
	/// Gets the centroid of a polygon along the Z-Axis (planar on the X/Y-axis)
	/// </summary>
	/// <returns>The centroid.</returns>
	/// <param name="points3">Array of Vector3 points of the polygon.</param>
	public static Vector3 GetCentroidZ(this Vector3[] points3)
	{
		// Convert from 3D to 2D on the Z-axis
		List<Vector2> points2 = new List<Vector2>();
		foreach (Vector3 point3 in points3)
		{
			points2.Add( new Vector2( point3.x, point3.y ) );
		}
		
		// Get the Centroid
		Vector2 centroid2 = points2.GetCentroid();
		
		// Convert back to 3D
		return( new Vector3(centroid2.x, centroid2.y, 0.0f) );
	} // GetCentroid()
	#endregion

	#region /- Vector3 -------------------------------------------------------------------------------------------------
	/// <summary>
	/// Converts a Vector2 to Vector3, swapping the Y and Z values.
	/// </summary>
	/// <returns>a Vector3</returns>
	/// <param name="source">Source Vector2.</param>
	public static Vector3 To3D(this Vector3 source)
	{
		return( new Vector3( source.x, source.z, source.y ) );
	}
	#endregion

	#region /- Zero Axis ---------------------------------------------------------------------------------------------
	/// <summary>
	/// Zeros the X value on this Vector3.
	/// </summary>
	/// <returns>The Vector3 with the X value set to Zero.</returns>
	/// <param name="source_vector">The source Vector3.</param>
	public static Vector3 ZeroX(this Vector3 source_vector)
	{
		source_vector.x = 0;
		return(source_vector);
	}
	/// <summary>
	/// Zeros the Y value on this Vector3.
	/// </summary>
	/// <returns>The Vector3 with the Y value set to Zero.</returns>
	/// <param name="source_vector">The source Vector3.</param>
	public static Vector3 ZeroY(this Vector3 source_vector)
	{
		source_vector.y = 0;
		return(source_vector);
	}
	/// <summary>
	/// Zeros the Z value on this Vector3.
	/// </summary>
	/// <returns>The Vector3 with the Z value set to Zero.</returns>
	/// <param name="source_vector">The source Vector3.</param>
	public static Vector3 ZeroZ(this Vector3 source_vector)
	{
		source_vector.z = 0;
		return(source_vector);
	}

	#endregion

	#region /- Mathfx --------------------------------------------------------------------------------------------------
	public static Vector3 LerpAngle(Vector3 from, Vector3 to, float t)
	{
		Vector3 result = from;
		result.x = Mathf.LerpAngle(from.x, to.x, t);
		result.y = Mathf.LerpAngle(from.y, to.y, t);
		result.z = Mathf.LerpAngle(from.z, to.z, t);
		return( result );
	}
	public static Vector3 Hermite(Vector3 from, Vector3 to, float t)
	{
		Vector3 result = from;
		result.x = Mathfx.Hermite(from.x, to.x, t);
		result.y = Mathfx.Hermite(from.y, to.y, t);
		result.z = Mathfx.Hermite(from.z, to.z, t);
		return( result );
	}
	public static Vector3 HermiteAngle(Vector3 from, Vector3 to, float t)
	{
		Vector3 result = from;
		result.x = Mathfx.HermiteAngle(from.x, to.x, t);
		result.y = Mathfx.HermiteAngle(from.y, to.y, t);
		result.z = Mathfx.HermiteAngle(from.z, to.z, t);
		return( result );
	}
	public static Vector3 Sinerp(Vector3 from, Vector3 to, float t)
	{
		Vector3 result = from;
		result.x = Mathfx.Sinerp(from.x, to.x, t);
		result.y = Mathfx.Sinerp(from.y, to.y, t);
		result.z = Mathfx.Sinerp(from.z, to.z, t);
		return( result );
	}
	public static Vector3 SinerpAngle(Vector3 from, Vector3 to, float t)
	{
		Vector3 result = from;
		result.x = Mathfx.SinerpAngle(from.x, to.x, t);
		result.y = Mathfx.SinerpAngle(from.y, to.y, t);
		result.z = Mathfx.SinerpAngle(from.z, to.z, t);
		return( result );
	}
	public static Vector3 Coserp(Vector3 from, Vector3 to, float t)
	{
		Vector3 result = from;
		result.x = Mathfx.Coserp(from.x, to.x, t);
		result.y = Mathfx.Coserp(from.y, to.y, t);
		result.z = Mathfx.Coserp(from.z, to.z, t);
		return( result );
	}
	public static Vector3 CoserpAngle(Vector3 from, Vector3 to, float t)
	{
		Vector3 result = from;
		result.x = Mathfx.CoserpAngle(from.x, to.x, t);
		result.y = Mathfx.CoserpAngle(from.y, to.y, t);
		result.z = Mathfx.CoserpAngle(from.z, to.z, t);
		return( result );
	}
	public static Vector3 Berp(Vector3 from, Vector3 to, float t)
	{
		Vector3 result = from;
		result.x = Mathfx.Berp(from.x, to.x, t);
		result.y = Mathfx.Berp(from.y, to.y, t);
		result.z = Mathfx.Berp(from.z, to.z, t);
		return( result );
	}
	public static Vector3 BerpAngle(Vector3 from, Vector3 to, float t)
	{
		Vector3 result = from;
		result.x = Mathfx.BerpAngle(from.x, to.x, t);
		result.y = Mathfx.BerpAngle(from.y, to.y, t);
		result.z = Mathfx.BerpAngle(from.z, to.z, t);
		return( result );
	}
	public static Vector3 Bounce(Vector3 from, Vector3 to, float t)
	{
		Vector3 result = from;
		t = Mathfx.Bounce(t);
		result.x = Mathf.Lerp(from.x, to.x, t);
		result.y = Mathf.Lerp(from.y, to.y, t);
		result.z = Mathf.Lerp(from.z, to.z, t);
		return( result );
	}
	public static Vector3 BounceAngle(Vector3 from, Vector3 to, float t)
	{
		Vector3 result = from;
		t = Mathfx.Bounce(t);
		result.x = Mathf.LerpAngle(from.x, to.x, t);
		result.y = Mathf.LerpAngle(from.y, to.y, t);
		result.z = Mathf.LerpAngle(from.z, to.z, t);
		return( result );
	}
	#endregion

	#region /- Snap ----------------------------------------------------------------------------------------------------
	public static Vector3 Snap(this Vector3 source, float target, float threshold)
	{
		Vector3 result = source;
		source.x = source.x.Snap(target, threshold);
		source.y = source.y.Snap(target, threshold);
		source.z = source.z.Snap(target, threshold);
		return( result );
	}

	public static Vector3 Snap(this Vector3 source, Vector3 target, float threshold)
	{
		Vector3 result = source;
		source.x = source.x.Snap(target.x, threshold);
		source.y = source.y.Snap(target.y, threshold);
		source.z = source.z.Snap(target.z, threshold);
		return( result );
	}

	#endregion


} // Vector3_Extensions()
