﻿using UnityEngine;
using System;
/*
Credit: http://wiki.unity3d.com/index.php?title=Mathfx

The following snippet provides short functions for floating point numbers. See the usage section for individualized information.

*/
public class Mathfx
{
	/// <summary>
	/// This method will interpolate while easing in and out at the limits.
	/// </summary>
	/// <param name="start">Start.</param>
	/// <param name="end">End.</param>
	/// <param name="value">Value.</param>
	public static float Hermite(float start, float end, float value)
	{
		return Mathf.Lerp(start, end, value * value * (3.0f - 2.0f * value));
	}
	/// <summary>
	/// This method will interpolate while easing in and out at the limits, and hopefully handle wrapping around 360 
	/// degrees properly.
	/// </summary>
	/// <param name="start">Start.</param>
	/// <param name="end">End.</param>
	/// <param name="value">Value.</param>
	public static float HermiteAngle(float start, float end, float value)
	{
		return Mathf.LerpAngle(start, end, value * value * (3.0f - 2.0f * value));
	}

	/// <summary>
	/// Short for 'sinusoidal interpolation', this method will interpolate while easing around the end, when value is near one.
	/// </summary>
	/// <param name="start">Start.</param>
	/// <param name="end">End.</param>
	/// <param name="value">Value.</param>
	public static float Sinerp(float start, float end, float value)
	{
		return Mathf.Lerp(start, end, Mathf.Sin(value * Mathf.PI * 0.5f));
	}
	
	/// <summary>
	/// Short for 'sinusoidal interpolation', this method will interpolate while easing around the end, when value is near one. 
	/// And hopefully handle wrapping around 360 degrees properly.
	/// </summary>
	/// <param name="start">Start.</param>
	/// <param name="end">End.</param>
	/// <param name="value">Value.</param>
	public static float SinerpAngle(float start, float end, float value)
	{
		return Mathf.LerpAngle(start, end, Mathf.Sin(value * Mathf.PI * 0.5f));
	}

	/// <summary>
	/// Similar to Sinerp, except it eases in, when value is near zero, instead of easing out (and uses cosine instead of sine).
	/// </summary>
	/// <param name="start">Start.</param>
	/// <param name="end">End.</param>
	/// <param name="value">Value.</param>
	public static float Coserp(float start, float end, float value)
	{
		return Mathf.Lerp(start, end, 1.0f - Mathf.Cos(value * Mathf.PI * 0.5f));
	}

	/// <summary>
	/// Similar to Sinerp, except it eases in, when value is near zero, instead of easing out (and uses cosine instead of sine).
	/// And hopefully handle wrapping around 360 degrees properly.
	/// </summary>
	/// <param name="start">Start.</param>
	/// <param name="end">End.</param>
	/// <param name="value">Value.</param>
	public static float CoserpAngle(float start, float end, float value)
	{
		return Mathf.LerpAngle(start, end, 1.0f - Mathf.Cos(value * Mathf.PI * 0.5f));
	}

	/// <summary>
	/// Short for 'boing-like interpolation', this method will first overshoot, then waver back and forth around the end value before coming to a rest.
	/// </summary>
	/// <param name="start">Start.</param>
	/// <param name="end">End.</param>
	/// <param name="value">Value.</param>
	public static float Berp(float start, float end, float value)
	{
		value = Mathf.Clamp01(value);
		value = (Mathf.Sin(value * Mathf.PI * (0.2f + 2.5f * value * value * value)) * Mathf.Pow(1f - value, 2.2f) + value) * (1f + (1.2f * (1f - value)));
		return start + (end - start) * value;
	}
	/// <summary>
	/// Short for 'boing-like interpolation', this method will first overshoot, then waver back and forth around the end 
	/// value before coming to a rest. Placeholder, replicates existing Berp() eventually update to handle wrapping around 360 degrees properly. 
	/// </summary>
	/// <param name="start">Start.</param>
	/// <param name="end">End.</param>
	/// <param name="value">Value.</param>
	public static float BerpAngle(float start, float end, float value)
	{
		//TODO: Replicates the existing Berp() call, need to update it later
		value = Mathf.Clamp01(value);
		value = (Mathf.Sin(value * Mathf.PI * (0.2f + 2.5f * value * value * value)) * Mathf.Pow(1f - value, 2.2f) + value) * (1f + (1.2f * (1f - value)));
		return start + (end - start) * value;
	}

	/// <summary>
	/// Works like Lerp, but has ease-in and ease-out of the values.
	/// </summary>
	/// <returns>The step.</returns>
	/// <param name="x">The x coordinate.</param>
	/// <param name="min">Minimum.</param>
	/// <param name="max">Max.</param>
	public static float SmoothStep (float x, float min, float max) 
	{
		x = Mathf.Clamp (x, min, max);
		float v1 = (x-min)/(max-min);
		float v2 = (x-min)/(max-min);
		return -2*v1 * v1 *v1 + 3*v2 * v2;
	}

	/// <summary>
	/// Short for 'linearly interpolate', this method is equivalent to Unity's Mathf.Lerp, included for comparison.
	/// </summary>
	/// <param name="start">Start.</param>
	/// <param name="end">End.</param>
	/// <param name="value">Value.</param>
	/*
	public static float Lerp(float start, float end, float value)
	{
		return ((1.0f - value) * start) + (value * end);
	}
	*/

	/// <summary>
	/// Will return the nearest point on a line to a point. Useful for making an object follow a track.
	/// </summary>
	/// <returns>The point.</returns>
	/// <param name="lineStart">Line start.</param>
	/// <param name="lineEnd">Line end.</param>
	/// <param name="point">Point.</param>
	public static Vector3 NearestPoint(Vector3 lineStart, Vector3 lineEnd, Vector3 point)
	{
		Vector3 lineDirection = Vector3.Normalize(lineEnd-lineStart);
		float closestPoint = Vector3.Dot((point-lineStart),lineDirection)/Vector3.Dot(lineDirection,lineDirection);
		return lineStart+(closestPoint*lineDirection);
	}

	/// <summary>
	/// Works like NearestPoint except the end of the line is clamped.
	/// </summary>
	/// <returns>The point strict.</returns>
	/// <param name="lineStart">Line start.</param>
	/// <param name="lineEnd">Line end.</param>
	/// <param name="point">Point.</param>
	public static Vector3 NearestPointStrict(Vector3 lineStart, Vector3 lineEnd, Vector3 point)
	{
		Vector3 fullDirection = lineEnd-lineStart;
		Vector3 lineDirection = Vector3.Normalize(fullDirection);
		float closestPoint = Vector3.Dot((point-lineStart),lineDirection)/Vector3.Dot(lineDirection,lineDirection);
		return lineStart+(Mathf.Clamp(closestPoint,0.0f,Vector3.Magnitude(fullDirection))*lineDirection);
	}

	/// <summary>
	/// Returns a value between 0 and 1 that can be used to easily make bouncing GUI items (a la OS X's Dock)
	/// </summary>
	/// <param name="x">The x coordinate.</param>
	public static float Bounce(float x) 
	{
		return Mathf.Abs(Mathf.Sin(6.28f*(x+1f)*(x+1f)) * (1f-x));
	}
	
	// test for value that is near specified float (due to floating point inprecision)
	// all thanks to Opless for this!
	public static bool Approx(float val, float about, float range) 
	{
		return ( ( Mathf.Abs(val - about) < range) );
	}
	
	// test if a Vector3 is close to another Vector3 (due to floating point inprecision)
	// compares the square of the distance to the square of the range as this 
	// avoids calculating a square root which is much slower than squaring the range
	public static bool Approx(Vector3 val, Vector3 about, float range) 
	{
		return ( (val - about).sqrMagnitude < range*range);
	}
	
	/// <summary>
	/// CLerp - Circular Lerp - is like lerp but handles the wraparound from 0 to 360.
	/// This is useful when interpolating eulerAngles and the object crosses the 0/360 boundary.  The standard 
	/// Lerp function causes the object to rotate in the wrong direction and looks stupid. Clerp fixes that.
	/// </summary>
	/// <param name="start">Start.</param>
	/// <param name="end">End.</param>
	/// <param name="value">Value.</param>
	public static float Clerp(float start , float end, float value)
	{
		float min = 0.0f;
		float max = 360.0f;
		float half = Mathf.Abs((max - min)/2.0f);//half the distance between min and max
		float retval = 0.0f;
		float diff = 0.0f;
		
		if ((end - start) < -half)
		{
			diff = ((max - start)+end)*value;
			retval =  start+diff;
		}
		else if ((end - start) > half)
		{
			diff = -((max - end)+start)*value;
			retval =  start+diff;
		}
		else 
		{
			retval =  start+(end-start)*value;
		}
		
		// Debug.Log("Start: "  + start + "   End: " + end + "  Value: " + value + "  Half: " + half + "  Diff: " + diff + "  Retval: " + retval);
		return retval;
	} // Clerp()
	
}