﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public static class EditorGameObject_Extensions 
{
	#region /- Find GameObjects ----------------------------------------------------------------------------------------------
	/// <summary>
	/// Gets all objects at the scene root. Use in this format: foreach (GameObject obj in GetSceneRootObjects()) { ... }
	/// </summary>
	/// <returns></returns>
	public static IEnumerable<GameObject> GetSceneRootObjects()
	{
		var prop = new HierarchyProperty(HierarchyType.GameObjects);
		var expanded = new int[0];
		while (prop.Next(expanded))
		{
			yield return prop.pptrValue as GameObject;
		}

	} // GetSceneRootObjects()

	/// <summary>
	/// Finds all GameObjects with the specified name, even if they are deactivated.
	/// </summary>
	/// <param name="target_name">Name of the GameObject to find.</param>
	/// <returns>A list of GameObjects with the matching name.</returns>
	public static List<GameObject> FindAll(string target_name)
	{
		List<GameObject> found_objs = new List<GameObject>();

		// Loop all scene root objects
		foreach (GameObject obj in GetSceneRootObjects())
		{
			// Loop all the children (and itself) for a name match
			List<GameObject> children = obj.GetAllChildrenAsList();
			for (int i = 0; i < children.Count; i++)
			{
				if (children[i].name == target_name) found_objs.Add(children[i]);
			}
		}
		return (found_objs);
	} // FindAll()

	/// <summary>
	/// Finds the first GameObject instance that matches the supplied name.
	/// </summary>
	/// <param name="target_name">Name of the GameObject to find.</param>
	/// <returns>The first GameObject that matches the name.</returns>
	public static GameObject Find(string target_name)
	{
		// Loop all scene root objects
		foreach (GameObject obj in GetSceneRootObjects())
		{
			// Loop all the children (and itself) for a name match
			List<GameObject> children = obj.GetAllChildrenAsList();
			for (int i = 0; i < children.Count; i++)
			{
				if (children[i].name == target_name)
				{
					return (children[i]);
				}
			}
		}
		return (null);
	} // Find()
    #endregion



} // EditorGameObject_Extensions()
