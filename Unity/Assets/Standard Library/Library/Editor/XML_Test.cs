﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System;
using System.Collections;
using System.IO;

public class XML_Test
{
    string xmldate = "<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n<dateTime>2000-01-01T00:00:00</dateTime>";
    DateTime someDateTime = DateTime.Parse("January 1, 2000");
    string targetFilename = Path.Combine(Application.dataPath, "unit_test_xml.xml");

    [Test]
    public void SaveToText()
    {
        // Setup
        string content = "";

        // Execute
        XML.Save(someDateTime, out content);

        // Results
        Assert.AreEqual(content, xmldate);
    } // SaveToText()


    [Test]
    public void SaveToFile()
    {
        // Setup

        // Execute
        XML.SaveFile(someDateTime, targetFilename);
        string content = File.ReadAllText(targetFilename);

        // Clean Up
        File.Delete(targetFilename);

        // Results
        Assert.AreEqual(content, xmldate);
    } // SaveToFile()

    [Test]
    public void LoadFromText()
    {
        // Setup
        DateTime loadDateTime = new DateTime();

        // Execute
        XML.Load(ref loadDateTime, xmldate);

        // Results
        Assert.AreEqual(loadDateTime, loadDateTime);
    } // LoadFromText()

    [Test]
    public void LoadFromFile()
    {
        // Setup
        DateTime loadDateTime = new DateTime();
        File.WriteAllText(targetFilename, xmldate);

        // Execute
        XML.LoadFile(ref loadDateTime, targetFilename);

        // Clean Up
        File.Delete(targetFilename);

        // Results
        Assert.AreEqual(loadDateTime, loadDateTime);
    } // LoadFromFile()

} // XML_Test()
