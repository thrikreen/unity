﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

public static class EditorPropertyDrawer_Extensions
{
    #region /- Get Object Reference ------------------------------------------------------------------------------------
    /// <summary>
    /// Gets the target object instance that this PropertyDrawer points to, makes it easier to access directly 
    /// than having to deal with serialized data and all that.
    /// Ref: http://answers.unity3d.com/questions/425012/get-the-instance-the-serializedproperty-belongs-to.html
    /// </summary>
    /// <param name="prop">The SerializedProperty</param>
    /// <returns>The target object that this SerializedProperty points to. Will have to typecast it back to the object type.</returns>
    public static object GetParent(SerializedProperty prop)
    {
        // Get the inspector object that this property is a child of
        object obj = prop.serializedObject.targetObject;

        string path = prop.propertyPath.Replace(".Array.data[", "[");
        string[] elements = path.Split('.');
        foreach (string element in elements.Take(elements.Length - 1))
        {
            if (element.Contains("["))
            {
                // Extract the value at the array index for this property
                string elementName = element.Substring(0, element.IndexOf("["));
                int index = Convert.ToInt32(element.Substring(element.IndexOf("[")).Replace("[","").Replace("]",""));
                obj = GetValue(obj, elementName, index);
            }
            else // Get the objec
            {
                obj = GetValue(obj, element);
            }
        } // loop propertyPath segments

        return obj;
    } // GetParent()

    /// <summary>
    /// Gets the value of the property from the target object.
    /// </summary>
    /// <param name="source">The target object</param>
    /// <param name="name">The property path</param>
    /// <returns></returns>
    public static object GetValue(object source, string name)
    {
        if (source == null) return null;
        var type = source.GetType();
        var f = type.GetField(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
        if (f == null)
        {
            var p = type.GetProperty(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
            if (p == null)
                return null;
            return p.GetValue(source, null);
        }
        return f.GetValue(source);
    } // GetValue()

    /// <summary>
    /// Gets the value of a property in an array from the target object.
    /// </summary>
    /// <param name="source"></param>
    /// <param name="name"></param>
    /// <param name="index"></param>
    /// <returns></returns>
    public static object GetValue(object source, string name, int index)
    {
        var enumerable = GetValue(source, name) as IEnumerable;
        var enm = enumerable.GetEnumerator();
        while (index-- >= 0)
            enm.MoveNext();
        return enm.Current;
    } // GetValue()
    #endregion
} // EditorPropertyDrawer_Extensions()
