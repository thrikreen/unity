﻿using UnityEngine;

/// <summary>
/// UnitySingleton class to be utilized and inherited by others. Handles creation of the instance, marking the first
/// one as DontDestroyOnLoad so it persists. Checks for any duplicates and removes the duplicates.
/// 
/// Be aware this will not prevent a non singleton constructor such as `T myT = new T();`
/// To prevent that, add `protected T () {}` to your singleton class.
/// 
/// As a note, this is made as MonoBehaviour because we need Coroutines http://wiki.unity3d.com/index.php/Singleton
/// </summary>
public class UnitySingleton<T> : MonoBehaviour where T : MonoBehaviour
{
	#region /- Variables --------------------------------------------------------------------------------------------------
	private static T _instance;
	private static object _lock                     = new object();
	private static bool _application_is_quitting    = false;

	private static GameObject _gameObject;
	/// <summary>
	/// Reference to the scene GameObject that contains this UnitySingleton component.
	/// </summary>
	public static GameObject Container
	{
		get { return (_gameObject); }
	}

	// Debug Flag
	private static bool _debug						= false;
	public static bool DEBUG
	{
		get { return (_debug); }
		set { _debug = value; }
	}
	#endregion

	#region /- Singleton - Get/Set ---------------------------------------------
	/// <summary>
	/// Access the Singleton instance. Creates if not found, along with the necessary scene GameObject.
	/// If there are duplicates, it will remove the later duplicates, allowing only the first one to exist.
	/// </summary>
	public static T Instance
	{
		get
		{
			if (_application_is_quitting)
			{
				Debug.LogWarningFormat("[ {0} ] Instance {1} already destroyed on application quit. Won't create again - returning null.", typeof(T).Name, typeof(T));
				_application_is_quitting = false;
				return null;
			}

			lock (_lock)
			{
				if (_instance == null) // Singleton not assigned/found
				{
					// Find if any existing in the scene
					_instance = (T)FindObjectOfType(typeof(T));
					if (FindObjectsOfType(typeof(T)).Length > 1) // Found more than one?!
					{
						Debug.LogErrorFormat("[ {0} ] Something went really wrong - there should never be more than 1 singleton.", typeof(T).Name);
						return (_instance);
					}

					if (_instance == null) // Not found in scene, create new GameObject and component
					{
						_gameObject = new GameObject();
						_instance = _gameObject.AddComponent<T>();
						_gameObject.name = typeof(T).ToString() + " (singleton)";

						DontDestroyOnLoad(_gameObject);

						if (DEBUG) Debug.LogFormat("[ {0} ] Singleton needed in the scene, '{1}' created.", typeof(T).Name, _gameObject.name);
					}
					else
					{
						_gameObject = _instance.gameObject;

						if (DEBUG) Debug.LogFormat(_gameObject, "[ {0} ] Using instance already created in '{1}.{2]'", typeof(T).Name, _gameObject.scene.name, _gameObject.name);
					}
				}

				return _instance;
			} // lock


		} // Get
	}
	#endregion
    
	#region /- Singleton - Events ----------------------------------------------
	/// <summary>
	/// When Unity quits, it destroys objects in a random order.
	/// In principle, a Singleton is only destroyed when application quits.
	/// If any script calls Instance after it have been destroyed, 
	///   it will create a buggy ghost object that will stay on the Editor scene
	///   even after stopping playing the Application. Really bad!
	/// So, this was made to be sure we're not creating that buggy ghost object.
	/// </summary>
	public virtual void OnDestroy()
	{
		_application_is_quitting = true;
	}

	public virtual void Awake()
	{
		SetupSingleton();
	}

	/// <summary>
	/// Initializes the singleton!
	/// </summary>
	void SetupSingleton()
	{
		if (_instance == null) // Not assigned yet
		{
			DontDestroyOnLoad(this.gameObject);
			_instance = this as T;
			_gameObject = this.gameObject;
		}
		else if (_instance != this) // not null, and this is not the assigned instance
		{
			if (Application.isPlaying)
			{
				Debug.LogErrorFormat(gameObject, "[ {0} ] Cannot have two instances of this singleton, deleting object '{1}.{2}'", typeof(T).Name, gameObject.scene.name, gameObject.name);
				Destroy(gameObject);
			}
			else // in Editor mode, warn the user and ping the offending GameObject
			{
				Debug.LogErrorFormat(gameObject, "[ {0} ] Cannot have two instances of this singleton. Please remove this component '{1}.{2}'", typeof(T).Name, gameObject.scene.name, gameObject.name);
			}
			return;
		}
	} // SetupSingleton()
    #endregion



} // UnitySingleton()
