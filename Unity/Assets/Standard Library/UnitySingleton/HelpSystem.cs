﻿using System;
using UnityEngine;

public class HelpSystem
{
    public static void OpenURL(string className)
    {
        Uri baseUri = new Uri("http://thrikreen.com/unity3d/");
        Uri openUri = new Uri(baseUri, className.ToLower());
        Debug.LogFormat("[ HELP ] Opening: {0}", openUri.ToString());
        Application.OpenURL(openUri.ToString());
    }
}
