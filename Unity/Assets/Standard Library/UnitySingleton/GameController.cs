﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[HelpURL("http://thrikreen.com/unity3d/gamecontroller")]
public class GameController : UnitySingleton<GameController>
{
    #region /- Variables --------------------------------------------------------------------------------------------------
    private bool _debug                                 = false;
    public static new bool DEBUG // Use this on an UnitySingleton, so the DEBUG modifies the Instance's
    {
        get { return (Instance != null ? Instance._debug : false); }
        set { if (Instance != null) Instance._debug = value; }
    }


    //[DateTimeItem]
    //public DateTimeItem dateTimeItemStarted     = new DateTimeItem();

    [DateTimeItem]
    public DateTimeItem dateTimeStarted         = new DateTimeItem();
    #endregion
    
    #region /- Help ------------------------------------------------------------------------------------------------
    [ContextMenu("Help")]
    void Context_Help() { HelpSystem.OpenURL(this.GetType().Name); }
    #endregion


} // GameController()
