﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

/// <summary>
/// 
/// </summary>
public class _TemplateScript : MonoBehaviour
{
	#region /- Variables -----------------------------------------------------------------------------------------------
	public bool DEBUG                           = true;
    /* 
	public static new bool DEBUG // Use this on an UnitySingleton, so the DEBUG modifies the Instance's
	{
		get { return (Instance != null ? Instance._debug : false); }
		set { if (Instance != null) Instance._debug = value; }
	}
    private bool _debug                         = false;
	*/

    // References (Components, Scene GameObjects, etc.)
    public GameObject m_target;

	// Speed
	private float _speed						        = 1.0f;				// Private/Hidden from Inspector
	public float m_minSpeed                             = 0.0f;				// Member
	public float m_maxSpeed                             = 10.0f;
	public float m_speedProperty									        // Property
	{
		get
		{
			return (_speed);
		}
		set
		{
			_speed = Mathf.Clamp(value, m_minSpeed, m_maxSpeed);
		}
	}

    public List<_TemplateItem> m_items			        = new List<_TemplateItem>();

    [DateTimeItem("yyyy-MM-dd HH:mm:ss.fff")]
    public DateTimeItem _currentDateTime                = new DateTimeItem();
    public DateTime currentDateTime { get { return (_currentDateTime.datetime); } set { _currentDateTime.datetime = value; } }

	public string cmdLineArgs                           = "-cmd arg -cmd2 arg arg -cmd3 \"arg and more arg\" -cmd4 arg -cmd5 -cmd6 cmd7";
	public ProgramArgs programArgs                      = new ProgramArgs();

    public string prefsFilename                         = "";
	#endregion

	#region /- Help ----------------------------------------------------------------------------------------------------
	[ContextMenu("Help")]
	void Context_Help() { HelpSystem.OpenURL(this.GetType().Name); }
	#endregion

	#region /- Initialize ----------------------------------------------------------------------------------------------
	/// <summary>
	/// Initializes references to other components. Runs after OnEnable().
	/// </summary>
	void Start()
	{
		string debuginfo = "";

		// Component References Check
		if (m_target == null) m_target = gameObject;
		if (m_target == null) debuginfo += "- Missing GameObject for m_target.\n";

        // Other start up processes and checks
        currentDateTime = DateTime.Now;

		// CommandLine Parameters
		//cmdLineArgs = System.Environment.CommandLine;
		programArgs = new ProgramArgs(cmdLineArgs);
		debuginfo += "- programArgs:\n" + programArgs.ToString() + "\n";
		
		if (debuginfo != "") Debug.LogErrorFormat(gameObject, "[ {0} ] {1}():\n{2}", GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, debuginfo);
	} // Start()

	/// <summary>
	/// Runs when this component (and by extension, the GameObject it is attached to) has become active.
	/// </summary>
	void OnEnable()
	{
		//string debuginfo = "";
		//if (DEBUG) Debug.LogFormat(gameObject, "[ {0} ] {1}():\n{2}", GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, debuginfo);
	} // OnEnable()

	/// <summary>
	/// Runs when this component (and by extension, the GameObject it is attached to) has become inactive.
	/// </summary>
	void OnDisable()
	{
	} // OnDisable()
	#endregion

	#region /- List Functions ------------------------------------------------------------------------------------------
	/// <summary>
	/// Sets the active item to the supplied Item, and does whatever processing it needs to do.
	/// </summary>
	/// <param name="item">Item to activate.</param>
	public void SetItem(_TemplateItem item)
	{
		string debuginfo = "";

		if (item == null) // No item
		{
		}
		else // Process item here
		{
		}

		if (DEBUG) Debug.LogFormat(gameObject, "[ {0} ] {1}( {2} ):\n{3}", GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, item.name, debuginfo);
	} // Play()

	/// <summary>
	/// Adds the item to the list.
	/// </summary>
	/// <param name="item">Item to add.</param>
	public void AddItem(_TemplateItem item)
	{
		m_items.Add(item);
	} // AddItem()

	/// <summary>
	/// Gets the item by name, if found.
	/// </summary>
	/// <param name="name">Name of the item to find.</param>
	/// <returns>The item if found, otherwise null.</returns>
	public _TemplateItem GetItem(string name)
	{
		for (int i = 0; i < m_items.Count; i++)
		{
			if (m_items[i].name.ToLower() == name.ToLower()) return (m_items[i]);
		}
		return (null);
	} // GetItem()

	/// <summary>
	/// Gets the index of the item, if found. Searches by item name and returns the first instance encountered.
	/// </summary>
	/// <param name="name">Name of the item to get the index of. </param>
	/// <returns>The index of the found item.</returns>
	public int GetItemIndex(string name)
	{
		return (GetItemIndex(GetItem(name)));
	} // GetItemIndex()

	/// <summary>
	/// Gets the index of the item, if found.
	/// </summary>
	/// <param name="item">The item to get the index of in the main list.</param>
	/// <returns>The index of the found item.</returns>
	public int GetItemIndex(_TemplateItem item)
	{
		for (int i = 0; i < m_items.Count; i++)
		{
			if (m_items[i] == item) return (i);
		}
		return (-1);
	} // GetItemIndex()

	/// <summary>
	/// Deletes the specified Item from the main list.
	/// </summary>
	/// <param name="item">Item to delete.</param>
	public void DeleteItem(_TemplateItem item)
	{
		if (item == null) return;
		m_items.Remove(item);
	} // DeleteItem()

	/// <summary>
	/// Duplicates the item.
	/// </summary>
	/// <param name="item">Item to duplicate.</param>
	public void DuplicateItem(_TemplateItem item)
	{
		if (item == null) return;
        _TemplateItem newItem = new _TemplateItem(item);
		newItem.name += " Copy";
		m_items.Add(newItem);
	} // DuplicateItem()

	/// <summary>
	/// Moves the specified item one down in the list.
	/// </summary>
	/// <param name="item">Item to move.</param>
	public void MoveItemDown(_TemplateItem item)
	{
		if (item == null) return;
		int index = GetItemIndex(item);
		if (GetItemIndex(item) != (m_items.Count - 1))
		{
			m_items.Remove(item);
			m_items.Insert(index + 1, item);
		}
	} // MoveItemDown()

	/// <summary>
	/// Moves the specified item one up in the list.
	/// </summary>
	/// <param name="item">Item to move.</param>
	public void MoveItemUp(_TemplateItem item)
	{
		if (item == null) return;
		int index = GetItemIndex(item);
		if (index > 0)
		{
			m_items.Remove(item);
			m_items.Insert(index - 1, item);
		}
	} // MoveItemUp()


	/// <summary>
	/// Sort the items.
	/// </summary>
	public void SortItems()
	{
		m_items.Sort(delegate (_TemplateItem a, _TemplateItem b)
		{
			int result = a.name.CompareTo(b.name);
			// Same name, compare by other criteria
			//if (result == 0)  result = a.OTHERPROPERTY.CompareTo(b.OTHERPROPERTY);
			return (result);
		});
	} // SortItems()

    public void RandomizeItems()
    {
        string debuginfo = "";

        for (int i=0; i<m_items.Count; i++)
        {
            debuginfo += string.Format("- #{0}: ", i);

            _TemplateItem item = m_items[i];
            item.name = item.GetType().Name + " " + i;
            item.id = i;

            int year = Random.Range(1980, 2017);
            int month = Random.Range(1, 12);
            int day = Random.Range(1, 28);
            int hour = Random.Range(0, 23);
            int min = Random.Range(0, 59);
            int sec = Random.Range(0, 59);
            int ms = Random.Range(0, 999);

            item.gameObject = gameObject;

            string datestring = string.Format("{0:0000}-{1:00}-{2:00} {3:00}:{4:00}:{5:00}.{6:000}", year, month, day, hour, min, sec, ms);
            item.lastmodified = new DateTimeItem(datestring);
            debuginfo += string.Format("- datetime: {0}\n", item.lastmodified.datetimestring);

        } // loop items

        debuginfo += "\n";
        if (DEBUG) Debug.LogFormat(gameObject, "[ {0} ] {1}( {2} ):\n{3}", GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, m_items.Count, debuginfo);
    }
    #endregion

} // _TemplateScript()
