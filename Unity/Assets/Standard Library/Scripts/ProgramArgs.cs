﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgramArgs
{
	#region /- Variables -----------------------------------------------------------------------------------------------
	static bool DEBUG                               = true;
	public Dictionary<string, string[]> properties	= new Dictionary<string, string[]>();

	// Indexed Accessor
	public string[] this[string key]
	{
		get
		{
			if (properties.ContainsKey(key)) return (properties[key]);
			else return (null);
		}
		set
		{
			properties[key] = value;
		}
	}

	// Properties
	public int Count { get { return (properties.Count); } }
	public Dictionary<string, string[]>.KeyCollection Keys { get { return (properties.Keys); } }
	public Dictionary<string, string[]>.ValueCollection Values { get { return (properties.Values); } }

	#endregion

	#region /- Constructor ---------------------------------------------------------------------------------------------
	/// <summary>
	/// Creates a new instance of ProgramArgs.
	/// </summary>
	public ProgramArgs()
	{
	}

	/// <summary>
	/// Creates a new instance of ProgramArgs, duplicating the values from the source object.
	/// </summary>
	/// <param name="source"></param>
	public ProgramArgs(ProgramArgs source)
	{
		string output = "";
		foreach (string key in source.Keys)
		{
			// Make a new list of the key's values
			List<string> values = new List<string>(source[key]);
			this.properties.Add(key, values.ToArray());
		} // loop keys
		if (DEBUG) Debug.LogFormat("[ {0} ] {1}():\n{2}", GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, output);
	}

	public ProgramArgs(string source, char delimiter=' ', char commandPrefix='-')
	{
		string output = "";
		string[] args = source.Split(delimiter);

		// Application Name - depending on the supplied commandline args string, it might include the app name too
		output += string.Format("FriendlyName: {0}\n", System.AppDomain.CurrentDomain.FriendlyName);
		//output += string.Format("GetExecutingAssembly: {0}\n", System.Reflection.Assembly.GetExecutingAssembly().GetName().ToString());
		output += string.Format("ProcessName: {0}\n", System.Diagnostics.Process.GetCurrentProcess().ProcessName);
		output += string.Format("FileName: {0}\n", System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);

		// Process the args list
		for (int i = 0; i < args.Length; i++)
		{
			if (args[i].StartsWith(commandPrefix.ToString())) // is a command
			{
				string key = args[i].Substring(1).ToLower(); // convert to lowercase, trim out the '-' prefix
				output += string.Format("- {0} = ", key);

				// Grabbing the following args until hitting another command
				// Also recognize strings if they start with "
				List<string> values = new List<string>();
				for (int j = (i + 1); j < args.Length; j++)
				{
					string value = args[j];
					if (args[j].StartsWith("\"")) // Start of a quote
					{
						// Continue grabbing the other items until one ends with a "
						for (int k=(j+1); k < args.Length; k++)
						{
							value += " " + args[k];
							if (args[k].EndsWith("\""))
							{
								// Update the args index
								j = k;

								// Replace the quotes
								value = value.Replace("\"", "");
								break;
							}
						}
					}
					else if (args[j].StartsWith(commandPrefix.ToString())) // Hit another Command, stop
					{
						break;
					}
					// else Regular string word, add as usual

					values.Add(value);
				} // loop command args

				// Debug, display the values
				for (int v = 0; v < values.Count; v++)
				{
					output += string.Format("\"{0}\" ", values[v]);
				} // loop values
				output += "\n";

				// Add to the dictionary
				properties.Add(key, values.ToArray());
			} // is command
			else // is not a command
			{
				continue;
			}
		} // loop args
		
		if (DEBUG) Debug.LogFormat("[ {0} ] {1}():\n{2}", GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, output);
	}
	#endregion

	#region /- Functions - Output --------------------------------------------------------------------------------------
	/// <summary>
	/// Outputs the contents of the properties to a string.
	/// </summary>
	/// <returns></returns>
	public override string ToString()
	{
		string output = "";

		foreach (string key in Keys)
		{
			output += string.Format("{0}=", key);
			if (properties[key] == null)
			{
				output += string.Format("<null>");
			}
			else
			{
				for (int i = 0; i < properties[key].Length; i++)
				{
					output += string.Format("\"{0}\"", properties[key][i]);
					if (i < (properties[key].Length - 1)) output += ", ";
				} // loop values
			}
			output += "\n";
		} // loop keys
		return (output);
	} // ToString()
	#endregion

	#region /- Functions - Process Args --------------------------------------------------------------------------------
	/// <summary>
	/// Template function on how to process the command line parameters after they have been parsed by the system.
	/// </summary>
	public void ProcessArgs()
	{
		foreach (string key in this.Keys)
		{
			switch (key)
			{
				case "cmd":
					if (this.properties[key] == null) break;		// No values for this key
					if (this.properties[key].Length == 0) break;    // No parameters, some options might not need a value set, adjust as needed

					// Process the args here:
					// var.x = this.properties[key][0].ToFloat();
					// var.y = this.properties[key][1].ToFloat();
					// var.z = this.properties[key][2].ToFloat();

					break;
			} // switch (key)
		} // loop keys
	} // ProcessArgs()
	#endregion

} // ProgramArgs()

