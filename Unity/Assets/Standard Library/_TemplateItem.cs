﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;

/// <summary>
/// Generic _TemplateItem object template
/// </summary>
[System.Serializable, XmlRoot("_templateitem")]
public class _TemplateItem 
{
	#region /- Variables -----------------------------------------------------------------------------------------------
	[XmlElement] public string name							= "";
	[XmlElement] public int id								= -1;

    // Scene
    [XmlIgnore] public GameObject gameObject;

    // Flags
    [XmlElement] public bool DEBUG                          = false;
    [XmlElement] public bool selected						= false;
    [XmlElement] public bool active							= false;
    [XmlElement] public bool locked							= false;
    
    // Transforms

    // Other Stuff

    [DateTimeItem]
    [XmlElement] public DateTimeItem lastmodified           = new DateTimeItem();
    #endregion

    #region /- Constructor ---------------------------------------------------------------------------------------------
    /// <summary>
    /// Creates a new instance of this class.
    /// </summary>
    public _TemplateItem()
	{
	} // _TemplateItem()

    //
    public _TemplateItem(string name, int id=-1, GameObject gameObject=null)
    {
        this.name = name;
        this.id = id;
        this.gameObject = gameObject;
    } // _TemplateItem()


    /// <summary>
    /// Creates a duplicate of the provided _TemplateItem source.
    /// </summary>
    /// <param name="source"></param>
    public _TemplateItem(_TemplateItem source)
	{
        string debuginfo = "";
        debuginfo += string.Format("- Source: {0}\n", source.name);

        // Clone via JSON, make a new object and source from that
        _TemplateItem newSource = new _TemplateItem();
        newSource = (_TemplateItem) JsonUtility.FromJson(JsonUtility.ToJson(source), typeof(_TemplateItem));
        
        // Parse every field on this class and duplicate the values over
        FieldInfo[] fields = this.GetType().GetFields(BindingFlags.Public|BindingFlags.NonPublic|BindingFlags.Instance);
		foreach (FieldInfo field in fields)
		{
            // Note: object references will copy the pointer, not make a new object. Need to deep copy it
            field.SetValue(this, field.GetValue(newSource));
            debuginfo += string.Format("- Field: {0} = {1}, {2}\n", field.Name, field.GetValue(newSource), field.GetType().IsByRef);
        } // loop fields

        // Duplicate 
        //this.lastmodified.datetimestring = source.lastmodified.datetime.ToString("yyyy-MM-dd HH:mm:ss.fff");

        //Debug.LogFormat(gameObject, "[ {0} ] {1}(): Clone\n{2}", GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, debuginfo);
    } // _TemplateItem()
    #endregion

    #region /- Selection -------------------------------------------------------------------------------------------
    public virtual void SetSelection(bool new_selection_state)
	{
		selected = new_selection_state;
	} // SetSelection()

	public virtual void SetLayer(int target_layer)
	{
		//gameObject.SetLayer(target_layer, true);
	} // SetLayer()
	#endregion

} // _TemplateItem()
