﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(TargetTracking))]
public class TargetTracking_Editor : Editor
{
    #region /- Variables -----------------------------------------------------------------------------------------------
    const string CLASSNAME                              = "TargetTracking";
    public static TargetTracking _target;
    public static MonoScript _target_script;
    public static MonoScript _editor_script;
    #endregion

    #region /- Help Menu -----------------------------------------------------------------------------------------------
    [MenuItem("CONTEXT/" + CLASSNAME + "/Edit " + CLASSNAME + "_Editor Script")]
    static void Menu_EditScript(MenuCommand command) { AssetDatabase.OpenAsset(_editor_script); }
    [MenuItem("CONTEXT/" + CLASSNAME + "/Find " + CLASSNAME + " Script")]
    static void Menu_PingScript(MenuCommand command) { EditorGUIUtility.PingObject(_target_script); }
    [MenuItem("CONTEXT/" + CLASSNAME + "/Find " + CLASSNAME + " Editor Script")]
    static void Menu_PingEditorScript(MenuCommand command) { EditorGUIUtility.PingObject(_editor_script); }
    #endregion

    #region /- Initialization ------------------------------------------------------------------------------------------
    /// <summary>
    /// Runs whenever this Inspector panel is shown. Note: Will run multiple times if other Inspector panels are also opened.
    /// </summary>
    void OnEnable()
    {
        _target = (TargetTracking)target;
        _target_script = MonoScript.FromMonoBehaviour(_target);
        _editor_script = MonoScript.FromScriptableObject(this);

    } // OnEnable()
    #endregion

    #region /- OnInspectorGUI ------------------------------------------------------------------------------------------
    /// <summary>
    /// InspectorGUI
    /// </summary>
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (GUI.changed)
        {
            if (Application.isPlaying) _target.CreateRings();
        }
    } // OnInspectorGUI()
    #endregion


} // _TemplateScript_Editor()
