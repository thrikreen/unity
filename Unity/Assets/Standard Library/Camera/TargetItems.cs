﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;
using Vectrosity;

/// <summary>
/// TargetRingItem for managing targetting rings on the HUD
/// </summary>
[System.Serializable, XmlRoot("targetringitem")]
public class TargetRingItem
{
    #region /- Variables -----------------------------------------------------------------------------------------------
    [XmlElement] public string name                         = "";
    [XmlElement] public int id                              = -1;
    [XmlElement] public bool enabled                        = true;

    [XmlIgnore] public VectorLine line;

    [XmlElement] public Color color                         = new Color(0f, 1f, 0f, 0.5f);
    [XmlElement] public int radius                          = 100;
    [XmlElement] public float width                         = 1f;
    [XmlElement] public int segments                        = 64;
    #endregion

    #region /- Constructor ---------------------------------------------------------------------------------------------
    /// <summary>
    /// Creates a new instance of this class.
    /// </summary>
    public TargetRingItem()
    {
    } // TargetRingItem()

    /// <summary>
    /// Creates a new instance of this class.
    /// </summary>
    public TargetRingItem(string name, int id = -1)
    {
        this.name = name;
        this.id = id;
    } // TargetRingItem()
    
    /// <summary>
    /// Creates a duplicate of the provided TargetRingItem source.
    /// </summary>
    /// <param name="source"></param>
    public TargetRingItem(TargetRingItem source)
    {
        string debuginfo = "";
        debuginfo += string.Format("- Source: {0}\n", source.name);

        // Clone via JSON, make a new object and source from that
        TargetRingItem newSource = new TargetRingItem();
        newSource = (TargetRingItem)JsonUtility.FromJson(JsonUtility.ToJson(source), typeof(TargetRingItem));

        // Parse every field on this class and duplicate the values over
        FieldInfo[] fields = this.GetType().GetFields(BindingFlags.Public|BindingFlags.NonPublic|BindingFlags.Instance);
        foreach (FieldInfo field in fields)
        {
            // Note: object references will copy the pointer, not make a new object. Need to deep copy it
            field.SetValue(this, field.GetValue(newSource));
            debuginfo += string.Format("- Field: {0} = {1}, {2}\n", field.Name, field.GetValue(newSource), field.GetType().IsByRef);
        } // loop fields

        // Duplicate 
        //this.lastmodified.datetimestring = source.lastmodified.datetime.ToString("yyyy-MM-dd HH:mm:ss.fff");

        //Debug.LogFormat(gameObject, "[ {0} ] {1}(): Clone\n{2}", GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, debuginfo);
    } // TargetRingItem()
    #endregion

} // TargetRingItem()


/// <summary>
/// TargetBoxItem for managing multiple targets on the HUD
/// </summary>
[System.Serializable, XmlRoot("targetboxitem")]
public class TargetBoxItem
{
    #region /- Variables -----------------------------------------------------------------------------------------------
    [XmlElement] public string name                         = "";
    [XmlElement] public int id                              = -1;
    [XmlElement] public bool enabled                        = true;

    [XmlIgnore] public GameObject gameObject;
    [XmlIgnore] public MeshRenderer meshr;

    [XmlIgnore] public Rect rect;
    [XmlIgnore] public Vector3 center                       = Vector3.zero;
    
    [XmlIgnore] public VectorLine line;

    [XmlElement] public Color color                         = new Color(0f, 1f, 0f, 0.5f);
    [XmlElement] public float width                         = 2f;

    #endregion

    #region /- Constructor ---------------------------------------------------------------------------------------------
    /// <summary>
    /// Creates a new instance of this class.
    /// </summary>
    public TargetBoxItem()
    {
    } // TargetBoxItem()

    /// <summary>
    /// Creates a new instance of this class.
    /// </summary>
    public TargetBoxItem(string name, int id = -1, GameObject gameObject=null)
    {
        this.name = name;
        this.id = id;
        this.gameObject = gameObject;
    } // TargetBoxItem()

    /// <summary>
    /// Creates a duplicate of the provided TargetBoxItem source.
    /// </summary>
    /// <param name="source"></param>
    public TargetBoxItem(TargetBoxItem source)
    {
        string debuginfo = "";
        debuginfo += string.Format("- Source: {0}\n", source.name);

        // Clone via JSON, make a new object and source from that
        TargetBoxItem newSource = new TargetBoxItem();
        newSource = (TargetBoxItem)JsonUtility.FromJson(JsonUtility.ToJson(source), typeof(TargetBoxItem));

        // Parse every field on this class and duplicate the values over
        FieldInfo[] fields = this.GetType().GetFields(BindingFlags.Public|BindingFlags.NonPublic|BindingFlags.Instance);
        foreach (FieldInfo field in fields)
        {
            // Note: object references will copy the pointer, not make a new object. Need to deep copy it
            field.SetValue(this, field.GetValue(newSource));
            debuginfo += string.Format("- Field: {0} = {1}, {2}\n", field.Name, field.GetValue(newSource), field.GetType().IsByRef);
        } // loop fields

        // Duplicate 
        //this.lastmodified.datetimestring = source.lastmodified.datetime.ToString("yyyy-MM-dd HH:mm:ss.fff");

        //Debug.LogFormat(gameObject, "[ {0} ] {1}(): Clone\n{2}", GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, debuginfo);
    } // TargetBoxItem()
    #endregion

} // TargetBoxItem()

