﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;

namespace CameraOrbit
{
    /// <summary>
    /// CameraOrbitItem
    /// </summary>
    [System.Serializable, XmlRoot("cameraorbititem")]
    public struct CameraOrbitItem
    {
        #region /- Variables -----------------------------------------------------------------------------------------------
        [XmlElement] public string name;
        [XmlElement] public int id;


        [XmlElement] public Vector3 position;
        [XmlIgnore] public bool dirtyPosition;
        [XmlElement] public Vector3 panOffset;
        [XmlIgnore] public bool dirtyPanOffset;

        [XmlElement] public Vector3 rotation;
        [XmlIgnore] public bool dirtyRotation;
        [XmlIgnore]
        public float yaw
        {
            set { rotation.y = value; }
            get { return (rotation.y); }
        }
        [XmlIgnore]
        public float pitch
        {
            set { rotation.x = value; }
            get { return (rotation.x); }
        }
        [XmlIgnore]
        public float roll
        {
            set { rotation.z = value; }
            get { return (rotation.z); }
        }

        [XmlElement] public float distance;
        [XmlIgnore] public bool dirtyDistance;
        [XmlElement] public float clippingNear;
        [XmlElement] public float clippingFar;
        [XmlElement] public bool orthographic;
        #endregion

        #region /- Constructor ---------------------------------------------------------------------------------------------
        /// <summary>
        /// 
        /// </summary>
        public CameraOrbitItem(string name = "", int id = 0, Vector3 position = default(Vector3), Vector3 rotation = default(Vector3), Vector3 panOffset = default(Vector3), float distance = 0, float clippingNear = 0.3f, float clippingFar = 1000f, bool orthographic = false)
        {
            this.name = name;
            this.id = id;
            this.position = position;
            this.rotation = rotation;
            this.distance = distance;
            this.panOffset = panOffset;
            this.dirtyPosition = false;
            this.dirtyRotation = false;
            this.dirtyDistance = false;
            this.dirtyPanOffset = false;
            this.clippingNear = clippingNear;
            this.clippingFar = clippingFar;
            this.orthographic = orthographic;
        } // CameraOrbitItem()

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source">Source CameraOrbitItem to duplicate from.</param>
        public CameraOrbitItem(CameraOrbitItem source)
        {
            this.name = source.name;
            this.id = source.id;
            this.position = source.position;
            this.rotation = source.rotation;
            this.distance = source.distance;
            this.panOffset = source.panOffset;
            this.dirtyPosition = source.dirtyPosition;
            this.dirtyRotation = source.dirtyRotation;
            this.dirtyDistance = source.dirtyDistance;
            this.dirtyPanOffset = source.dirtyPanOffset;
            this.clippingNear = source.clippingNear;
            this.clippingFar = source.clippingFar;
            this.orthographic = source.orthographic;
        } // CameraOrbitItem()
        #endregion

    } // CameraOrbitItem()


    [System.Serializable, XmlRoot("cameraclampitem")]
    public struct CameraClampItem
    {
        #region /- Variables -----------------------------------------------------------------------------------------------
        [XmlElement] public bool enabled;
        [XmlElement] public float min;
        [XmlElement] public float max;
        #endregion

        #region /- Constructor ---------------------------------------------------------------------------------------------
        public CameraClampItem(bool enabled = false, float min = 0, float max = 100)
        {
            this.enabled = enabled;
            this.min = min;
            this.max = max;
        } // CameraClampItem()

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source">Source CameraOrbitItem to duplicate from.</param>
        public CameraClampItem(CameraClampItem source)
        {
            this.enabled = source.enabled;
            this.min = source.min;
            this.max = source.max;
        } // CameraClampItem()
        #endregion

    } // CameraClampItem()
} // CameraOrbit