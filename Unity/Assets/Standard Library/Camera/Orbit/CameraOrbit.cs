﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace CameraOrbit
{
    /// <summary>
    /// 
    /// </summary>
    [HelpURL("http://thrikreen.com/unity3d/cameraorbit")]
    public class CameraOrbit : MonoBehaviour
    {
        #region /- Variables -----------------------------------------------------------------------------------------------
        public bool DEBUG                           = true;
        /* 
        public static new bool DEBUG // Use this on an UnitySingleton, so the DEBUG modifies the Instance's
        {
            get { return (Instance != null ? Instance._debug : false); }
            set { if (Instance != null) Instance._debug = value; }
        }
        private bool _debug                         = false;
        */

        // References (Components, Scene GameObjects, etc.)
        public Camera m_cameraObject;
        public GameObject m_targetObject;


        // Smooth Motion
        public bool m_smoothMotion              = true;
        public float m_rotationSpeed            = 4.0f;
        public float m_distanceSpeed            = 3.0f;
        public float m_followSpeed              = 1.0f;
        public float m_panSpeed                 = 1.0f;
        public float m_clipSpeed                = 1.0f;

        // Orth
        public float m_lockZPosition            = 1.0f;
        public Vector2 m_orthSize               = Vector2.zero;

        public CameraOrbitItem m_initial;
        public CameraOrbitItem m_current;
        public CameraOrbitItem m_target;

        // Clamp
        public CameraClampItem m_clampDistance  = new CameraClampItem(true, 1f, 20f);
        public CameraClampItem m_clampYaw       = new CameraClampItem(false, -360f, 360f);
        public CameraClampItem m_clampPitch     = new CameraClampItem(true, -89f, 89f);
        public CameraClampItem m_clampRoll      = new CameraClampItem(false, -180f, 180f);

        // Distance Tresholds
        public float m_finishedRotationThreshold= 0.1f;
        public float m_finishedDistanceThreshold= 0.5f;
        public float m_finishedFollowThreshold  = 0.5f;
        public float m_finishedPanThreshold     = 0.5f;

        // Events
        public Action OnRotationEnd;
        public Action OnDistanceEnd;
        public Action OnPanEnd;
        #endregion

        #region /- Help ------------------------------------------------------------------------------------------------
        [ContextMenu("Help")]
        void Context_Help() { HelpSystem.OpenURL(this.GetType().Name); }
        #endregion

        #region /- Initialize ------------------------------------------------------------------------------------------
        void Reset()
        {
            // Camera, assume it's on the camera itself, at least on component add.
            m_cameraObject = GetComponent<Camera>();
        }

        /// <summary>
        /// Initializes references to other components. Runs after OnEnable().
        /// </summary>
        void Start()
        {
            string debuginfo = "";

            // Component References Check
            if (m_cameraObject == null) m_cameraObject = gameObject.GetComponent<Camera>();
            if (m_cameraObject == null) debuginfo += "- Missing Camera for m_cameraObject.\n";

            // Target
            if (m_targetObject == null) debuginfo += "- Missing Target Object.\n";

            SetInitialProperties();

            if (debuginfo != "") Debug.LogErrorFormat(gameObject, "[ {0} ] {1}():\n{2}", GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, debuginfo);
        } // Start()
        #endregion

        #region /- Update ----------------------------------------------------------------------------------------------
        void LateUpdate()
        {
            UpdateCamera();
        } // Update()

        void UpdateCamera()
        {
            #region /- Clamp Target Values ---------------------------------------------------------------------------------
            if (m_clampDistance.enabled)
            {
                m_target.distance = Mathf.Clamp(m_target.distance, m_clampDistance.min, m_clampDistance.max);
            }

            if (m_clampYaw.enabled)
            {
                m_target.yaw = Mathf.Clamp(m_target.yaw, m_clampYaw.min, m_clampYaw.max);
            }

            if (m_clampPitch.enabled) m_target.pitch = Mathf.Clamp(m_target.pitch, m_clampPitch.min, m_clampPitch.max);

            // Clipping Planes
            if (m_target.clippingNear < 0.01f) m_target.clippingNear = 0.01f;
            if (m_target.clippingNear > m_target.clippingFar) m_target.clippingFar = m_target.clippingNear + 0.01f;
            if (m_target.clippingFar <= m_target.clippingNear) m_target.clippingFar = m_target.clippingNear + 0.01f;
            #endregion

            #region /- Calculate Distance and Rotation -----------------------------------------------------------------
            if (m_smoothMotion) // Dampens motion over time
            {
                // Distance to Target Object (Zoom In/Out or OrthographicSize in 2D mode)
                m_current.distance = Mathf.Lerp(m_current.distance, m_target.distance, Time.deltaTime * m_distanceSpeed);

                // Yaw (left/right)
                m_current.yaw = Mathf.Lerp(m_current.yaw, m_target.yaw, Time.deltaTime * m_rotationSpeed);

                // Pitch (up/dpwn)
                m_current.pitch = Mathf.LerpAngle(m_current.pitch, m_target.pitch, Time.deltaTime * m_rotationSpeed);

                // Position
                m_target.position = m_targetObject.transform.position;
                m_current.position = Vector3.Lerp(m_current.position, m_target.position, Time.deltaTime * m_followSpeed);

                // Clipping Planes
                m_current.clippingNear = Mathf.Lerp(m_current.clippingNear, m_target.clippingNear, Time.deltaTime * m_clipSpeed);
                m_current.clippingFar = Mathf.Lerp(m_current.clippingFar, m_target.clippingFar, Time.deltaTime * m_clipSpeed);
            }
            else // Snap to the new position & rotation
            {
                m_current.distance = m_target.distance;
                m_current.yaw = m_target.yaw;
                m_current.pitch = m_target.pitch;
                m_current.position = m_target.position;

                m_current.clippingNear = m_target.clippingNear;
                m_current.clippingFar = m_target.clippingFar;
            }
            #endregion

            #region /- Apply the motions to the camera -----------------------------------------------------------------
            // Rotation
            Vector3 target_rotation = Vector3.Lerp(m_current.rotation, m_target.rotation, Time.deltaTime * m_rotationSpeed);

            // Apply rotation to camera
            m_cameraObject.transform.rotation = Quaternion.Euler(target_rotation);

            /*
            Quaternion target_rotation = Quaternion.Lerp(m_cameraObject.transform.rotation, Quaternion.Euler(m_target.pitch, m_target.yaw, 0), Time.deltaTime * m_rotationSpeed); 

            // Flatten Z-Axis (roll) from quat conversion
            Vector3 m_vectorRotation = target_rotation.eulerAngles;
            target_rotation = Quaternion.Euler(m_vectorRotation.x, m_vectorRotation.y, 0);

            // Apply rotation to camera
            m_cameraObject.transform.rotation = target_rotation;
            */


            // Position
            if (m_cameraObject.orthographic)
            {
                // Update the orth size
                m_cameraObject.orthographicSize = m_current.distance;
                m_current.position.z = m_lockZPosition;
                m_cameraObject.transform.position = m_current.position;

                // Update the OrthographicSize
                m_orthSize.x = m_cameraObject.orthographicSize * m_cameraObject.aspect;
                m_orthSize.y = m_cameraObject.orthographicSize;
            }
            else
            {
                m_cameraObject.transform.position = m_current.position - (m_current.distance * m_cameraObject.transform.forward);
            }

            // Clip Planes
            m_cameraObject.nearClipPlane = m_current.clippingNear;
            m_cameraObject.farClipPlane = m_current.clippingFar;

            #endregion

            #region /- Events ------------------------------------------------------------------------------------------
            // Distance
            if (m_current.distance.InRange(m_target.distance, m_finishedDistanceThreshold) && m_current.dirtyDistance)
            {
                m_current.dirtyDistance = false;
                if (OnDistanceEnd != null)
                {
                    //Debug.LogErrorFormat(gameObject, "[ {0} ] {1}()", GetType().Name, "OnDistanceEnd");
                    OnDistanceEnd.Invoke();
                }
            }

            // Rotation
            if (m_current.yaw.InRange(m_target.yaw, m_finishedRotationThreshold) && m_current.pitch.InRange(m_target.pitch, m_finishedRotationThreshold) && m_current.dirtyRotation)
            {
                m_current.dirtyRotation = false;
                if (OnRotationEnd != null)
                {
                    //Debug.LogErrorFormat(gameObject, "[ {0} ] {1}()", GetType().Name, "OnRotationEnd");
                    OnRotationEnd.Invoke();
                }
                // Clamp the angles
                if ((m_target.yaw > 360) || (m_target.yaw < -360))
                {
                    m_target.yaw = ClampAngle(m_target.yaw);
                    m_current.yaw = ClampAngle(m_current.yaw);
                }
            }
            #endregion

        } // UpdateCamera()
        #endregion

        #region /- Camera Transform Settings ---------------------------------------------------------------------------
        /// <summary>
        /// Sets the initial properties of the camera.
        /// </summary>
        public void SetInitialProperties()
        {
            // Rotation
            Vector3 initialRotation = m_cameraObject.transform.eulerAngles;
            m_initial.yaw = m_current.yaw = m_target.yaw = initialRotation.y;
            m_initial.pitch = m_current.pitch = m_target.pitch = initialRotation.x;

            // Distance
            if (m_cameraObject.orthographic) m_current.distance = m_target.distance = m_initial.distance = m_cameraObject.orthographicSize;
            else m_current.distance = m_target.distance = m_initial.distance = Vector3.Distance(m_cameraObject.transform.position, m_targetObject.transform.position);

            // Orthographic
            m_current.orthographic = m_initial.orthographic = m_cameraObject.orthographic;

            // Position
            if (m_cameraObject.orthographic)
            {
                m_current.position = m_initial.position = m_targetObject.transform.position;
                m_current.position.z = m_lockZPosition;
            }
            else
            {
                m_current.position = m_initial.position = m_targetObject.transform.position;
            }

            // Clipping
            m_current.clippingNear = m_initial.clippingNear = m_cameraObject.nearClipPlane;
            m_current.clippingFar = m_initial.clippingFar = m_cameraObject.farClipPlane;

        } // SetInitialProperties()

        public void SetCameraRotationToInitial()
        {
            SetCameraRotation(m_initial.pitch, m_initial.yaw, 0);
        }

        public void AlignWithTarget()
        {
            // Position
            if (m_cameraObject.orthographic)
            {
                m_cameraObject.transform.position = m_targetObject.transform.position;
            }
            else
            {
                m_cameraObject.transform.position = m_targetObject.transform.position - (m_initial.distance * m_cameraObject.transform.forward);
            }
        }

        public Camera GetCamera()
        {
            return (m_cameraObject);
        }
        #endregion

        #region /- Get/Set Camera Transforms ---------------------------------------------------------------------------
        // Rotation
        public void SetCameraRotation(float pitch, float yaw, float roll)
        {
            SetCameraRotation(Quaternion.Euler(pitch, yaw, roll));
        }

        public void SetCameraRotation(Vector3 newRotation)
        {
            SetCameraRotation(Quaternion.Euler(newRotation.x, newRotation.y, newRotation.z));
        }

        public void SetCameraRotation(Quaternion newRotation)
        {
            m_cameraObject.transform.rotation = newRotation;
        }

        // Distance
        public void SetCameraDistance(float newDistance)
        {
            m_cameraObject.transform.position = m_targetObject.transform.position - (newDistance * m_cameraObject.transform.forward);
        }

        // Target Position
        public void SetTargetPosition(float x, float y, float z)
        {
            SetTargetPosition(new Vector3(x, y, z));
        }

        public void SetTargetPosition(Vector3 newPosition)
        {
            m_targetObject.transform.position = newPosition;
        }
        #endregion

        #region /- Get/Set CameraOrbitItem -----------------------------------------------------------------------------
        public CameraOrbitItem GetCurrentOrbit()
        {
            if (Application.isPlaying)
            {
                CameraOrbitItem newItem = new CameraOrbitItem(m_current);
                return (newItem);
            }
            else // Not playing, in the editor
            {
                // Grab current camera settings directly
                CameraOrbitItem newItem = new CameraOrbitItem();

                // Rotation
                newItem.rotation = m_cameraObject.transform.eulerAngles;

                // Distance
                if (m_cameraObject.orthographic)
                {
                    newItem.distance = m_cameraObject.orthographicSize;
                }
                else
                {
                    newItem.distance = Vector3.Distance(m_cameraObject.transform.position, m_targetObject.transform.position);
                }

                // Orthographic
                newItem.orthographic = m_cameraObject.orthographic;

                // Position
                if (m_cameraObject.orthographic)
                {
                    newItem.position = m_targetObject.transform.position;
                    newItem.position.z = m_lockZPosition;
                }
                else
                {
                    newItem.position = m_targetObject.transform.position;
                }

                // Clipping
                newItem.clippingNear = m_cameraObject.nearClipPlane;
                newItem.clippingFar = m_cameraObject.farClipPlane;

                return (newItem);
            } // Editor

        } // GetCurrentState()

        public CameraOrbitItem GetTargetOrbit()
        {
            if (Application.isPlaying)
            {
                CameraOrbitItem newItem = new CameraOrbitItem(m_target);
                return (newItem);
            }
            else // Not playing, in the editor
            {
                // Grab current camera settings directly
                CameraOrbitItem newItem = new CameraOrbitItem();

                // Rotation
                newItem.rotation = m_cameraObject.transform.eulerAngles;

                // Distance
                if (m_cameraObject.orthographic)
                {
                    newItem.distance = m_cameraObject.orthographicSize;
                }
                else
                {
                    newItem.distance = Vector3.Distance(m_cameraObject.transform.position, m_targetObject.transform.position);
                }

                // Orthographic
                newItem.orthographic = m_cameraObject.orthographic;

                // Position
                if (m_cameraObject.orthographic)
                {
                    newItem.position = m_targetObject.transform.position;
                    newItem.position.z = m_lockZPosition;
                }
                else
                {
                    newItem.position = m_targetObject.transform.position;
                }

                // Clipping
                newItem.clippingNear = m_cameraObject.nearClipPlane;
                newItem.clippingFar = m_cameraObject.farClipPlane;

                return (newItem);
            } // Editor
        } // GetTargetOrbit()

        public void SetTargetOrbit(CameraOrbitItem newTarget, bool instant = false)
        {
            m_target = newTarget;
            m_current.dirtyPosition = true;
            m_current.dirtyRotation = true;
            m_current.dirtyDistance = true;
            m_current.dirtyPanOffset = true;
            
            if (instant) m_current = m_target;

            if (!Application.isPlaying) // Not playing, in the editor
            {
                SetCameraRotation(newTarget.rotation);
                SetCameraDistance(newTarget.distance);
                m_target = newTarget;
                m_current = newTarget;
            }

            /*
            if (DEBUG)
            {
                string debugOutput = "";
                //debugOutput += string.Format("Yaw, Target  = {0}\n", m_target.yaw);

                debugOutput += string.Format("Yaw, Current = {0}\n", ClampAngle(m_current.yaw));

                debugOutput += "\n\n";
                Debug.LogFormat("{0}", debugOutput);
            }*/
        } // SetTargetOrbit()
        #endregion

        #region /- Methods ---------------------------------------------------------------------------------------------
        /// <summary>
        /// Clamps the angle so it stays within -360 to 360 degrees.
        /// </summary>
        /// <returns>The angle in degrees.</returns>
        /// <param name="angle">Angle to be clamped, in degrees.</param>
        /// <param name="min">Minimum angle value, in degrees. Defaults to -360 degrees.</param>
        /// <param name="max">Maximum angle value, in degrees. Defaults to 360 degrees.</param>
        static float ClampAngle(float angle, float min=-360f, float max=360f)
        {
            int count = Mathf.Abs((int)angle) / 360;
            if (angle < -360)
            {
                angle += 360 * count;
            }
            else if (angle > 360)
            {
                angle -= 360 * count;
            }
            return (Mathf.Clamp(angle, min, max));
        } // ClampAngle()
        #endregion

    } // CameraOrbit()

} // CamereOrbit