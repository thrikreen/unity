﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace CameraOrbit
{
    /// <summary>
    /// 
    /// </summary>
    public class CameraOrbitList : MonoBehaviour
    {
        #region /- Variables -----------------------------------------------------------------------------------------------
        public bool DEBUG                           = true;

        // References (Components, Scene GameObjects, etc.)
        public CameraOrbit m_targetCamera;

        public string m_saveFile                    = "";
        public List<CameraOrbitItem> m_items        = new List<CameraOrbitItem>();
        #endregion

        #region /- Help ----------------------------------------------------------------------------------------------------
        [ContextMenu("Help")]
        void Context_Help() { HelpSystem.OpenURL(this.GetType().Name); }
        #endregion

        #region /- Initialize ----------------------------------------------------------------------------------------------
        /// <summary>
        /// Initializes references to other components. Runs after OnEnable().
        /// </summary>
        void Start()
        {
            string debuginfo = "";

            // Component References Check
            if (m_targetCamera == null) m_targetCamera = gameObject.GetComponent<CameraOrbit>();
            if (m_targetCamera == null) debuginfo += "- Missing CameraOrbit for m_targetCamera.\n";

            // Other start up processes and checks

            if (debuginfo != "") Debug.LogErrorFormat(gameObject, "[ {0} ] {1}():\n{2}", GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, debuginfo);
        } // Start()
        #endregion

        #region /- Main ----------------------------------------------------------------------------------------------------


        public void Save()
        {
            if (m_targetCamera != null)
            {
                // Load current camera properties 
                CameraOrbitItem newItem = m_targetCamera.GetCurrentOrbit();
                m_items.Add(newItem);
            }

        }





        #endregion



        #region /- List Functions ------------------------------------------------------------------------------------------
        /// <summary>
        /// Sets the active item to the supplied Item, and does whatever processing it needs to do.
        /// </summary>
        /// <param name="item">Item to activate.</param>
        public void SetItem(CameraOrbitItem item)
        {
            string debuginfo = "";

            m_targetCamera.SetTargetOrbit(item);

            if (DEBUG) Debug.LogFormat(gameObject, "[ {0} ] {1}( {2} ):\n{3}", GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, item.name, debuginfo);
        } // Play()

        /// <summary>
        /// Adds the item to the list.
        /// </summary>
        /// <param name="item">Item to add.</param>
        public void AddItem(CameraOrbitItem newItem)
        {
            m_items.Add(newItem);
        } // AddItem()

        /// <summary>
        /// Gets the item by name, if found.
        /// </summary>
        /// <param name="name">Name of the item to find.</param>
        /// <returns>The item if found, otherwise null.</returns>
        public Nullable<CameraOrbitItem> GetItem(string name)
        {
            for (int i = 0; i < m_items.Count; i++)
            {
                if (m_items[i].name.ToLower() == name.ToLower()) return (m_items[i]);
            }
            return (null);
        } // GetItem()

        /// <summary>
        /// Gets the index of the item, if found. Searches by item name and returns the first instance encountered.
        /// </summary>
        /// <param name="name">Name of the item to get the index of. </param>
        /// <returns>The index of the found item.</returns>
        public int GetItemIndex(string name)
        {
            return (GetItemIndex(GetItem(name)));
        } // GetItemIndex()

        /// <summary>
        /// Gets the index of the item, if found.
        /// </summary>
        /// <param name="item">The item to get the index of in the main list.</param>
        /// <returns>The index of the found item.</returns>
        public int GetItemIndex(Nullable<CameraOrbitItem> item)
        {
            for (int i = 0; i < m_items.Count; i++)
            {
                //if (m_items[i] == item) return (i);
            }
            return (-1);
        } // GetItemIndex()

        /// <summary>
        /// Deletes the specified Item from the main list.
        /// </summary>
        /// <param name="item">Item to delete.</param>
        public void DeleteItem(CameraOrbitItem item)
        {
            //if (item == null) return;
            m_items.Remove(item);
        } // DeleteItem()

        /// <summary>
        /// Duplicates the item.
        /// </summary>
        /// <param name="item">Item to duplicate.</param>
        public void DuplicateItem(CameraOrbitItem item)
        {
            //if (item == null) return;
            CameraOrbitItem newItem = new CameraOrbitItem(item);
            newItem.name += " Copy";
            m_items.Add(newItem);
        } // DuplicateItem()

        /// <summary>
        /// Moves the specified item one down in the list.
        /// </summary>
        /// <param name="item">Item to move.</param>
        public void MoveItemDown(CameraOrbitItem item)
        {
            //if (item == null) return;
            int index = GetItemIndex(item);
            if (GetItemIndex(item) != (m_items.Count - 1))
            {
                m_items.Remove(item);
                m_items.Insert(index + 1, item);
            }
        } // MoveItemDown()

        /// <summary>
        /// Moves the specified item one up in the list.
        /// </summary>
        /// <param name="item">Item to move.</param>
        public void MoveItemUp(CameraOrbitItem item)
        {
            //if (item == null) return;
            int index = GetItemIndex(item);
            if (index > 0)
            {
                m_items.Remove(item);
                m_items.Insert(index - 1, item);
            }
        } // MoveItemUp()


        /// <summary>
        /// Sort the items.
        /// </summary>
        public void SortItems()
        {
            m_items.Sort(delegate (CameraOrbitItem a, CameraOrbitItem b)
            {
                int result = a.name.CompareTo(b.name);
            // Same name, compare by other criteria
            //if (result == 0)  result = a.OTHERPROPERTY.CompareTo(b.OTHERPROPERTY);
            return (result);
            });
        } // SortItems()

        public void RandomizeItems()
        {
            string debuginfo = "";

            for (int i = 0; i < m_items.Count; i++)
            {
                debuginfo += string.Format("- #{0}: ", i);

                CameraOrbitItem item = m_items[i];
                item.name = item.GetType().Name + " " + i;
                item.id = i;

            } // loop items

            debuginfo += "\n";
            if (DEBUG) Debug.LogFormat(gameObject, "[ {0} ] {1}( {2} ):\n{3}", GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, m_items.Count, debuginfo);
        }
        #endregion

    } // CameraOrbitList()
} // CameraOrbit
