﻿#pragma warning disable
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DigitalRubyShared;

#if FINGERS
namespace CameraOrbit
{
    public class CameraOrbitController : MonoBehaviour
    {
        #region /- Variables -------------------------------------------------------------------------------------------
        public bool DEBUG                       = false;
        public CameraOrbit m_targetOrbit;

        // Tap/Double Tap/Long Press
        public bool m_enableTap                 = true;
        //TapGestureRecognizer tap;
        //LongPressGestureRecognizer longpress;
        public float m_doubleTapDuration        = 0.5f;
        public float m_longPressDuration        = 3.0f;

        // Swipe
        public bool m_enableSwipe               = true;
        SwipeGestureRecognizer m_swipe;
        public Vector2 m_sensitivity            = Vector2.one;
        public float m_MinimumDistanceUnits     = 0.01f;
        public float m_MinimumSpeedUnits        = 0.1f;
        public float m_DirectionThreshold       = 0f;

        // Scale/Pinch
        public bool m_enableScale               = true;
        ScaleGestureRecognizer m_scale;
        public float m_scaleSensitivity         = 1.0f;
        public float m_scaleMultiplier          = 0f;

        // Pan/Drag
        public bool m_enablePan                 = true;
        PanGestureRecognizer m_pan;
        public float m_movementSpeed            = 1.0f;

        // Rotate/Twist
        public bool m_enableRotate              = true;
        //RotateGestureRecognizer rotate;


        // Debug
        public Text m_debugOutput;
        string debugSwipe                       = "";
        string debugPan                         = "";
        string debugScale                       = "";
        string debugTap                         = "";

        /*
        public string m_state                   = "";
        public Vector2 m_delta                  = Vector2.zero;
        public Vector2 m_distance               = Vector2.zero;
        */
        public int touchCount                   = 0;
        #endregion

        #region /- Help ------------------------------------------------------------------------------------------------
        [ContextMenu("Help")]
        void Context_Help() { HelpSystem.OpenURL(this.GetType().Name); }
        #endregion

        #region /- Initialize ------------------------------------------------------------------------------------------
        void Start()
        {
            // Target CameraOrbit
            if (m_targetOrbit == null) m_targetOrbit = gameObject.GetComponent<CameraOrbit>();
            if (m_targetOrbit != null)
            {
                // Order is important!
                //CreateDoubleTapGesture();
                //CreateTapGesture();
                if (m_enableSwipe) CreateSwipeGesture();
                if (m_enablePan) CreatePanGesture();
                if (m_enableScale) CreateScaleGesture();
                //CreateRotateGesture();
                //CreateLongPressGesture();
            }
        } // Start()
        #endregion
        
        #region /- Update ----------------------------------------------------------------------------------------------
        private void Update()
        {
            touchCount = Input.touchCount;
            if (FingersScript.Instance.TreatMousePointerAsFinger && Input.mousePresent)
            {
                touchCount += (Input.GetMouseButton(0) ? 1 : 0);
                touchCount += (Input.GetMouseButton(1) ? 1 : 0);
                touchCount += (Input.GetMouseButton(2) ? 1 : 0);
            }

            UpdateDebug();
        } // Update()
        
        void UpdateDebug()
        {
            string debugOutput = "";
            if (m_debugOutput != null)
            {
                debugOutput += string.Format("Touch Count: {0}\n", touchCount);
                debugOutput += string.Format("{0}\n", debugSwipe);
                debugOutput += string.Format("{0}\n", debugPan);
                debugOutput += string.Format("{0}\n", debugScale);

                m_debugOutput.text = debugOutput;
            }
        }
        #endregion

        #region /- Finger Gesture - Tap/Double Tap ---------------------------------------------------------------------
        /*
        /// <summary>
        /// Register a tap event
        /// </summary>
        private void CreateTapGesture()
        {
            tap = new TapGestureRecognizer();
            tap.Updated += Tap_Updated;
            FingersScript.Instance.AddGesture(tap);
        }
        
        private void Tap_Updated(GestureRecognizer gesture, ICollection<GestureTouch> touches)
        {
            if (gesture.State == GestureRecognizerState.Ended)
            {
                Debug.Log("Tap");
            }
        }
        */
        #endregion

        #region /- Finger Gesture - Swipe ------------------------------------------------------------------------------
        /// <summary>
        /// Register the swipe gesture.
        /// </summary>
        private void CreateSwipeGesture()
        {
            m_swipe = new SwipeGestureRecognizer();
            m_swipe.Updated += Swipe_Updated;
            m_swipe.Direction = SwipeGestureRecognizerDirection.Any;
            m_swipe.DirectionThreshold = m_DirectionThreshold;
            m_swipe.MinimumDistanceUnits = m_MinimumDistanceUnits;
            m_swipe.MinimumSpeedUnits = m_MinimumSpeedUnits;
            FingersScript.Instance.AddGesture(m_swipe);
        } // CreateSwipeGesture()


        private void Swipe_Updated(GestureRecognizer gesture, ICollection<GestureTouch> touches)
        {
            SwipeGestureRecognizer swipe = gesture as SwipeGestureRecognizer;
            debugSwipe = "";

            if (swipe.State == GestureRecognizerState.Executing)
            {
                // Apply the gesture motions to the CameraOrbit
                CameraOrbitItem orbit = m_targetOrbit.GetTargetOrbit();
                orbit.yaw += swipe.DeltaX * m_sensitivity.x;
                orbit.pitch += swipe.DeltaY * m_sensitivity.y;
                m_targetOrbit.SetTargetOrbit(orbit);

                // Debug
                debugSwipe += string.Format(" {0}({1}): Delta: ({2:0.0}, {3:0.0})\n", System.Reflection.MethodBase.GetCurrentMethod().Name, gesture.State, gesture.DeltaX, gesture.DeltaY);
            } // Executing

            //if (DEBUG) Debug.LogFormat("[ {0} ] {1}(): {2}\n{3}", GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, gesture.State, debugOutput);
        } // Swipe_Updated()
        #endregion

        #region /- Finger Gesture - scale ------------------------------------------------------------------------------
        /// <summary>
        /// Register a scale (scale) gesture
        /// </summary>
        private void CreateScaleGesture()
        {
            m_scale = new ScaleGestureRecognizer();
            m_scale.Updated += Scale_Updated;
            FingersScript.Instance.AddGesture(m_scale);
        } // CreateScaleGesture()
        
        private void Scale_Updated(GestureRecognizer gesture, ICollection<GestureTouch> touches)
        {
            ScaleGestureRecognizer scale = gesture as ScaleGestureRecognizer;
            debugScale = "";
            if (Input.touchCount != 2) return; // do nothing if not two touches

            if (scale.State == GestureRecognizerState.Executing)
            {
                // Apply the gesture motions to the CameraOrbit
                CameraOrbitItem orbit = m_targetOrbit.GetTargetOrbit();
                orbit.distance += (1 - scale.ScaleMultiplier) * m_scaleSensitivity;
                m_targetOrbit.SetTargetOrbit(orbit);

                // Debug
                debugScale += string.Format(" {0}({1}): Delta: ({2:0.0}, {3:0.0})\n", System.Reflection.MethodBase.GetCurrentMethod().Name, gesture.State, gesture.DeltaX, gesture.DeltaY);
            } // Executing

            //if (DEBUG) Debug.LogFormat("[ {0} ] {1}(): {2}\n{3}", GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, gesture.State, debugOutput);
        } // scale_Updated()
        #endregion

        #region /- Finger Gesture - Pan/Drag ---------------------------------------------------------------------------
        /// <summary>
        /// Register a Pan/Drag Gesture
        /// </summary>
        void CreatePanGesture()
        {
            m_pan = new PanGestureRecognizer();
            m_pan.Updated += Pan_Updated;
            FingersScript.Instance.AddGesture(m_pan);
        }
        
        private void Pan_Updated(GestureRecognizer gesture, ICollection<GestureTouch> touches)
        {
            PanGestureRecognizer pan = gesture as PanGestureRecognizer;
            debugPan = "";
            if (Input.touchCount != 2) return; // do nothing if not two touches

            if (gesture.State == GestureRecognizerState.Executing)
            {
                Vector2 delta = new Vector2(pan.DeltaX, pan.DeltaY);

                // Camera Direction
                Vector3 forward = m_targetOrbit.GetCamera().transform.forward;
                forward.y = 0; // flatten Y
                forward = forward.normalized;
                Vector3 right = new Vector3(forward.z, 0, -forward.x);

                // Convert to direction relative to the camera orientation
                Vector3 move_direction = ((delta.x * right) + (delta.y * forward)).normalized * m_movementSpeed;

                // Apply to the target object
                m_targetOrbit.m_targetObject.transform.Translate(move_direction);

                // Debug
                debugPan += string.Format(" {0}({1}): Delta: ({2:0.0}, {3:0.0})\n", System.Reflection.MethodBase.GetCurrentMethod().Name, gesture.State, gesture.DeltaX, gesture.DeltaY);
            }

            //if (DEBUG) Debug.LogFormat("[ {0} ] {1}(): {2}\n{3}", GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, gesture.State, debugOutput);
        } // Pan_Updated()
        #endregion

    } // CameraOrbitController()
} // CameraOrbit
#endif
