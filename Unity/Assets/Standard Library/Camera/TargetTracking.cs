﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;
using Vectrosity;

/// <summary>
/// Tracks the screen position of multiple objects and maps their position to the camera as screen coordinates.
/// </summary>
public class TargetTracking : MonoBehaviour
{
	#region /- Variables -----------------------------------------------------------------------------------------------
	public bool DEBUG                           = true;

	// References (Components, Scene GameObjects, etc.)
	public Camera m_targetCamera;
    public Vector2 m_screenCenter               = new Vector2();

    public List<GameObject> m_devTargets        = new List<GameObject>();

    // Vectrosity stuff
    public List<TargetRingItem> m_targetRings   = new List<TargetRingItem>();
    public List<TargetBoxItem> m_targets        = new List<TargetBoxItem>();

    #endregion

    #region /- Help ----------------------------------------------------------------------------------------------------
    [ContextMenu("Help")]
	void Context_Help() { /* HelpSystem.OpenURL(this.GetType().Name); */ }
    #endregion

    #region /- Initialize ----------------------------------------------------------------------------------------------
    private void Reset()
    {
        if (gameObject.GetComponent<Camera>() != null) m_targetCamera = gameObject.GetComponent<Camera>();
        UpdateScreenCenter();
    }

    /// <summary>
    /// Initializes references to other components. Runs after OnEnable().
    /// </summary>
    void Start()
	{
		string debuginfo = "";

		// Component References Check
		if (m_targetCamera == null) m_targetCamera = Camera.main;
		if (m_targetCamera == null) debuginfo += "- Missing Main Camera reference.\n";

        // Other start up processes and checks
        UpdateScreenCenter();

        if (debuginfo != "") Debug.LogErrorFormat(gameObject, "[ {0} ] {1}():\n{2}", GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, debuginfo);
	} // Start()

	/// <summary>
	/// Runs when this component (and by extension, the GameObject it is attached to) has become active.
	/// </summary>
	void OnEnable()
	{
        UpdateScreenCenter();
        CreateRings();

        if (DEBUG)
        {
            for (int i=0; i<m_devTargets.Count; i++) AddTarget(m_devTargets[i]);
        }
    } // OnEnable()

	/// <summary>
	/// Runs when this component (and by extension, the GameObject it is attached to) has become inactive.
	/// </summary>
	void OnDisable()
	{
	} // OnDisable()

    public void UpdateScreenCenter()
    {
        m_screenCenter = new Vector2(Screen.width / 2, Screen.height / 2);
    }
    #endregion

    #region /- Target Rings --------------------------------------------------------------------------------------------
    public void CreateRings()
    {
        for (int i=0; i<m_targetRings.Count; i++)
        {
            CreateRing(m_targetRings[i]);
        }
    }

    public void CreateRing(TargetRingItem item)
    {
        // Destroy the old one if it exists
        if (item.line != null) VectorLine.Destroy(ref item.line);

        // Create ring
        List<Vector2> points = new List<Vector2>(item.segments + 1);
        item.line = new VectorLine(item.name, points, item.width, LineType.Continuous, Joins.Weld);
        item.line.MakeCircle(m_screenCenter, item.radius);
        item.line.SetColor(item.color);

        // Draw Ring
        item.line.Draw();
    } // CreateRing()
    #endregion

    #region /- Target Boxes --------------------------------------------------------------------------------------------
    public void AddTarget(GameObject target)
    {
        // Create a new TargetBox and add to the list
        TargetBoxItem item = new TargetBoxItem(target.name, m_targets.Count, target);
        item.meshr = item.gameObject.GetComponent<MeshRenderer>();
        
        m_targets.Add(item);

        // Create the box
        CreateBox(item);
    }

    public void CreateBox(TargetBoxItem item)
    {
        // Destroy the old one if it exists
        if (item.line != null) VectorLine.Destroy(ref item.line);

        // Create target box
        List<Vector2> points = new List<Vector2>(4 + 1);
        item.line = new VectorLine(item.name, points, item.width, LineType.Continuous, Joins.Weld);
        item.line.SetColor(item.color);

        UpdateTargetBox(item);
    }

    public void UpdateTargetBoxes()
    {
        for (int i = 0; i < m_targets.Count; i++)
        {
            UpdateTargetBox(m_targets[i]);
        }
    } // UpdateTargetBoxes()

    public void UpdateTargetBox(TargetBoxItem item)
    {
        item.rect = GetScreenRect(item);

        item.line.MakeRect(item.rect);
        item.line.Draw();
    } // UpdateTargetBox()

    public Rect GetScreenRect(TargetBoxItem item)
    {
        Vector3 cen = item.meshr.bounds.center;
        Vector3 ext = item.meshr.bounds.extents;
        Vector2[] extentPoints = new Vector2[8]
           {
               WorldToGUIPoint(new Vector3(cen.x-ext.x, cen.y-ext.y, cen.z-ext.z)),
               WorldToGUIPoint(new Vector3(cen.x+ext.x, cen.y-ext.y, cen.z-ext.z)),
               WorldToGUIPoint(new Vector3(cen.x-ext.x, cen.y-ext.y, cen.z+ext.z)),
               WorldToGUIPoint(new Vector3(cen.x+ext.x, cen.y-ext.y, cen.z+ext.z)),
               WorldToGUIPoint(new Vector3(cen.x-ext.x, cen.y+ext.y, cen.z-ext.z)),
               WorldToGUIPoint(new Vector3(cen.x+ext.x, cen.y+ext.y, cen.z-ext.z)),
               WorldToGUIPoint(new Vector3(cen.x-ext.x, cen.y+ext.y, cen.z+ext.z)),
               WorldToGUIPoint(new Vector3(cen.x+ext.x, cen.y+ext.y, cen.z+ext.z))
           };
        Vector2 min = extentPoints[0];
        Vector2 max = extentPoints[0];
        foreach (Vector2 v in extentPoints)
        {
            min = Vector2.Min(min, v);
            max = Vector2.Max(max, v);
        }

        item.center = cen;

        Rect rect = new Rect(min.x, min.y, max.x - min.x, max.y - min.y);
        rect.y = Screen.height - rect.y - rect.height; // fix for screen coordinates

        return (rect);
    } // GetScreenRect()
    
    public Vector2 WorldToGUIPoint(Vector3 world)
    {
        Vector2 screenPoint = m_targetCamera.WorldToScreenPoint(world);
        screenPoint.y = (float)Screen.height - screenPoint.y;
        return screenPoint;
    }
    #endregion
   
    #region /- Update --------------------------------------------------------------------------------------------------
    void LateUpdate()
    {
        UpdateTargetBoxes();
    } // LateUpdate()
    #endregion

} // TargetTracking()
