Collection of general tools and stuff that people might have some use for their projects.

Website: http://www.thrikreen.com/

Trello: https://trello.com/b/B1FWTsj9/unity

Some code might utilize licensed asset packages, which I obviously can't upload. So I will utilize something like preprocessor 
directives so they won't break if you grab the repo and don't have the necessary packages.

i.e. Vectrosity -> #if VECTROSITY ... #endif

To enable it, open the "Other Settings" panel of the "Player Settings" and navigate to the "Scripting Define Symbols" text 
field. Separate the symbols with ";" i.e. "VECTROSITY;SHADERFORGE;FINGERS"

List of packages and the symbols in use:

FINGERS: https://assetstore.unity.com/packages/tools/input-management/fingers-touch-gestures-for-unity-41076

GRIDS: https://assetstore.unity.com/packages/tools/grids-pro-66291

REWIRED: https://assetstore.unity.com/packages/tools/utilities/rewired-21676

SHADERFORGE: https://assetstore.unity.com/packages/tools/visual-scripting/shader-forge-14147

SPLINES: https://assetstore.unity.com/packages/tools/utilities/dreamteck-splines-61926

VECTROSITY: https://assetstore.unity.com/packages/tools/particles-effects/vectrosity-82

(more to be added)

<eof>
